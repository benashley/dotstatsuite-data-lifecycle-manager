import { pathOr, zipObj, keys, pipe, assocPath, pick, path, mergeDeepRight } from 'ramda';
import htmlescape from 'htmlescape';
import debug from 'debug';
import { getLanguage } from '../utils/language';

const loginfo = debug('webapp');

const renderHtml = (config, assets, i18n, settings, stylesheetUrl) => {
  return `
    <!doctype html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="/favicon.ico">
        <link rel="stylesheet" href="/css/preloader.css">
        <link rel="stylesheet" href="/static/css/vendors~main.chunk.css">
        <style id="insertion-point-jss"></style>
        <link rel="stylesheet" href="${stylesheetUrl}">
        <title>Data Lifecycle Manager</title>
        <script> CONFIG = ${htmlescape(config)} </script>
        <script> SETTINGS = ${htmlescape(settings)} </script>
        <script> I18N = ${htmlescape(i18n)} </script>
      </head>
      <body>
        <noscript>
          You need to enable JavaScript to run this app.
        </noscript>
        <div id="root">
          <div class="preloader">
            <div class="lds-roller">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
            </div>
          </div>
        </div>
        <script type="text/javascript" src="${assets.vendors}"></script>
        <script type="text/javascript" src="${assets.main}"></script>
      </body>
    </html>
  `;
};

const ssr = ({ config, assets, configProvider }) => async (req, res) => {
  const { tenant } = req;
  const settings = await configProvider.getSettings(tenant);

  const datasources = await configProvider.getDataSources(tenant);
  const localDatasources = path(['sdmx', 'datasources'], settings);
  const tenantDatasources = pick(keys(localDatasources), datasources);

  const locales = pipe(
    pathOr([], ['i18n', 'locales']),
    keys,
  )(settings);
  const i18n = await configProvider.getI18n(tenant, locales);
  const stylesheetUrl = settings.styles;
  const html = renderHtml(
    {
      tenant,
      env: config.env,
      backendServerUrl: config.backendServerUrl,
      keycloak: { ...tenant.keycloak, url: `${config.authServerUrl}/auth` },
      transferServerUrl: config.transferServerUrl,
    },
    assets,
    zipObj(locales, i18n),
    assocPath(
      ['sdmx', 'datasources'],
      mergeDeepRight(tenantDatasources, localDatasources),
      settings,
    ),
    stylesheetUrl,
  );
  res.send(html);
  loginfo(`render site for tenant '${tenant.name}'`);
};

export default ssr;
