const server = {
  host: process.env.SERVER_HOST || '0.0.0.0',
  port: Number(process.env.SERVER_PORT) || 7002,
};

const config = {
  appId: 'data-lifecycle-manager',
  env: process.env.NODE_ENV || 'development',
  isProduction: process.env.NODE_ENV === 'production',
  gitHash: process.env.GIT_HASH,
  backendServerUrl: process.env.BACKEND_URL || 'http://localhost:7000',
  server,
  configUrl: process.env.CONFIG_URL || 'http://localhost:5007',
  authServerUrl: process.env.AUTH_SERVER_URL || 'http://localhost:8080',
  transferServerUrl: process.env.TRANSFER_SERVER_URL,
};

module.exports = config;
