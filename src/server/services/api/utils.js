import debug from 'debug';
import Keycloak from 'keycloak-verify';
import { slice, assocPath } from 'ramda';
import { HTTPError } from '../../utils/errors';

const loginfo = debug('webapp:api:keycloak');

const handleError = error => {
  loginfo('Authentification error: ', error.message);
  throw new HTTPError(403);
};

const getToken = slice(7, Infinity);

const verify = method => async ctx => {
  try {
    const realm = ctx.locals.req.tenant.keycloak.realm;
    const authServerUrl = ctx.globals.config.authServerUrl;
    const keycloak = Keycloak({ realm, authServerUrl });
    const auth = ctx.locals.req.headers.authorization;
    if (!auth) throw Error('No token provided.');
    const token = getToken(auth);
    const user = await keycloak[method](token);
    return assocPath(['locals', 'req', 'user'], user)(ctx);
  } catch (error) {
    handleError(error);
  }
};

export const getUserOnline = () => ctx => verify('verifyOnline')(ctx);
export const getUserOffline = () => ctx => verify('verifyOffline')(ctx);
