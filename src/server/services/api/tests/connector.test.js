import { getMessage } from '../connector';

describe('api services connector', () => {
  describe('getMessage', () => {
    it('should return the service, the method and the input', () => {
      const service = 'api';
      const req = {
        body: { foo: 'bar' },
        query: { bar: 'foo' },
        path: '/api',
        method: 'fakeMethod',
      };
      expect(getMessage(service, req)).toEqual({
        input: { ...req.body, ...req.query },
        service,
        method: 'api',
      });
    });
  });
});
