const debug = require('debug');
const evtX = require('evtx').default;
const initHealthcheck = require('./healthcheck');
const { initServices } = require('../../utils');

const loginfo = debug('webapp:evtx');

const services = [initHealthcheck];

const init = ctx => {
  const {
    config: { gitHash },
    configProvider,
  } = ctx;
  const globals = { configProvider, startTime: new Date(), gitHash };
  const healthcheck = evtX(globals).configure(initServices(services));
  loginfo('monitoring service up.');
  return Promise.resolve({
    ...ctx,
    services: { ...ctx.services, healthcheck },
  });
};

module.exports = init;
