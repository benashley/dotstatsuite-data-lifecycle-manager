import { reduce, pipeP } from 'ramda';

export const initResources = resources => ctx => pipeP(...resources)(ctx);

export const initServices = services => evtx =>
  reduce((acc, service) => acc.configure(service), evtx, services);
