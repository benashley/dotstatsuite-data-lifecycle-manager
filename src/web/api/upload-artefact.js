import { SUCCESS, WARNING, ERROR } from '../modules/upload';
import { postStructureRequest } from '../sdmx-lib';
import { get } from 'lodash';

const fileReaderPromise = file => {
  const reader = new FileReader();
  reader.readAsText(file);

  return new Promise(
    resolve => (reader.onload = () => resolve(reader.result)),
    reject => (reader.onerror = error => reject(error)),
  );
};

export const uploadFromFile = (options = {}) => {
  return fileReaderPromise(options.file)
    .then(raw => postStructureRequest({ ...options, data: raw }))
    .then(log => {
      if (get(log, 'isError', false)) {
        return { type: ERROR, message: log.data };
      }
      if (get(log, 'isWarning', false)) {
        return { type: WARNING, message: log.data };
      }
      return { type: SUCCESS };
    })
    .catch(error => ({ type: ERROR, message: error.message }));
};
