import { getData as sdmxLibGetData } from '../sdmx-lib';
import fileSaver from 'file-saver';

export const download = (file, options, format) => {
  const { agency, code, version } = options;
  let blob;
  if (format === 'xml') blob = new Blob([file], { type: 'text/html' });
  if (format === 'csv') blob = new Blob([file], { type: 'text/csv' });
  const filename = `${agency}-${code}-${version}-data.${format}`;
  fileSaver.saveAs(blob, filename);
};

export const dataflowExportData = (options = {}, format) => {
  return sdmxLibGetData({ ...options, format }).then(data => download(data, options, format));
};
