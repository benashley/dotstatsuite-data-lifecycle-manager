import fileSaver from 'file-saver';
import { getArtefactStructure } from '../sdmx-lib';

export const download = (xml, options) => {
  const { agency, code, version, withReferences } = options;
  let blob = new Blob([xml], { type: 'text/html' });
  const filename = `${agency}-${code}-${version}${withReferences === 'all' ? '-all' : ''}.xml`;
  fileSaver.saveAs(blob, filename);
};

export const artefactExportStructure = (options = {}, withReferences) => {
  return getArtefactStructure({ ...options, withReferences }).then(structure =>
    download(structure, { ...options, withReferences }),
  );
};
