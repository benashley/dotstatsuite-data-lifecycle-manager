import { SUCCESS, ERROR } from '../modules/transfer';
import * as R from 'ramda';

export const transfer = (options = {}) => {
  const {
    endpoint,
    sourceSpace,
    sourceDataflow,
    destinationSpace,
    destinationDataflow,
    dataQuery,
  } = options;

  const url = `${endpoint}/transfer/dataflow`;

  const formData = new FormData();
  formData.append('sourceDataspace', sourceSpace.id);
  formData.append('sourceDataflow', sourceDataflow.sdmxId);
  formData.append('destinationDataspace', destinationSpace.id);
  formData.append('destinationDataflow', destinationDataflow.sdmxId);

  if (!R.isNil(dataQuery)) {
    formData.append('sourceQuery', dataQuery);
  }

  const fetchOptions = {
    method: 'POST',
    mode: 'cors',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${R.path(['TOKENS', 'idToken'], window)}`,
    },
    body: formData,
  };

  return fetch(url, fetchOptions)
    .then(res => res.json())
    .then(json => {
      if (json.success) return { type: SUCCESS, message: json.message };
      throw Error(json.message);
    })
    .catch(error => ({ type: ERROR, error }));
};
