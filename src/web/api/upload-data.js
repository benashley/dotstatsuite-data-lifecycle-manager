import { find, get, head, includes } from 'lodash';
import { path } from 'ramda';
import {
  SUCCESS,
  ERROR,
  MIME_TYPE_XML,
  MIME_TYPE_XLSX,
  EXCEL_EDD,
  SDMX_CSV,
} from '../modules/upload';

export const upload = (options = {}) => {
  const { dataflow, endpoint, files, mode, space, byPath, filepath } = options;

  let url = null;
  if (mode === EXCEL_EDD) {
    url = `${endpoint}/import/excel`;
  }
  if (mode === SDMX_CSV) {
    url = `${endpoint}/import/sdmxFile`;
  }

  // API limitation: one of a kind only
  const eddFile = find(files, file => includes(MIME_TYPE_XML, file.type));
  const excelFile = find(files, file => includes(MIME_TYPE_XLSX, file.type));
  const sdmxFile = head(files);
  const dataflowId = get(dataflow, 'sdmxId', null);

  const formData = new FormData();
  formData.append('dataspace', space.id);
  if (mode === EXCEL_EDD) {
    formData.append('eddFile', eddFile);
    formData.append('excelFile', excelFile);
  }
  if (mode === SDMX_CSV) {
    if (byPath) {
      formData.append('filepath', filepath);
    } else {
      formData.append('file', sdmxFile);
    }
    formData.append('dataflow', dataflowId);
  }

  const fetchOptions = {
    method: 'POST',
    mode: 'cors',
    headers: {
      Accept: 'application/json',
      Authorization: `Bearer ${path(['TOKENS', 'idToken'], window)}`,
    },
    body: formData,
  };

  return fetch(url, fetchOptions)
    .then(res => {
      return res.json();
    })
    .then(json => {
      if (json.success) return { type: SUCCESS, message: json.message };
      throw Error(json.message);
    })
    .catch(error => ({ type: ERROR, error }));
};
