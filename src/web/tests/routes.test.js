import routes from '../routes';

describe('App | routes', () => {
  it('should get default route', () => {
    expect(routes.getDefault()).toEqual(routes.getRouteByName('manageStructure'));
  });
});
