import { compose, reduce, toPairs, find, filter, prop } from 'ramda';
import ManageStructure from './pages/manage-structure';
import UploadArtefact from './pages/upload-artefact';
import UploadData from './pages/upload-data';
import Dump from './pages/dump';

const routes = {
  manageStructure: {
    path: '/',
    component: ManageStructure,
    exact: true,
    auth: true,
    default: true,
    name: 'home',
    icon: 'home',
  },
  uploadArtefact: {
    path: '/upload-artefact',
    component: UploadArtefact,
    exact: true,
    auth: true,
    name: 'uploadArtefact',
    icon: 'folder-shared',
    translationKey: 'route.upload.structure',
  },
  uploadData: {
    path: '/upload-data',
    component: UploadData,
    exact: true,
    auth: true,
    name: 'uploadData',
    icon: 'document-share',
    translationKey: 'route.upload.data',
  },
  dumpDownload: {
    path: '/dump',
    component: Dump,
    exact: true,
    auth: true,
    name: 'dumpDownload',
    icon: 'cloud-download',
    translationKey: 'route.dump',
  },
};

const isAuthRequired = route => route && route.auth;
export const getDefault = () => find(route => route.default, exportedRoutes);

const exportedRoutes = compose(
  reduce((acc, [name, r]) => [...acc, { ...r, name }], []),
  toPairs,
  filter(prop('path')),
)(routes);

export const getRouteByName = name => routes[name] && { ...routes[name], name };

export const getPathByName = (name, param) => {
  const path = prop('path', getRouteByName(name));
  return param ? `${path.replace(':id', param)}` : path;
};

export default {
  getRoutes: () => exportedRoutes,
  isAuthRequired,
  getDefault,
  getRouteByName,
  getPathByName,
};
