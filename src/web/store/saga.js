import { all } from 'redux-saga/effects';
import { agenciesSaga } from '../modules/agencies';
import { artefactsSaga } from '../modules/artefacts';
import { uploadArtefactSaga } from '../modules/upload-artefact';
import { uploadDataSaga, fetchDataflowsSaga } from '../modules/upload-data';
import { transferArtefactSaga } from '../modules/transfer-artefact';
import { transferDataSaga } from '../modules/transfer-data';
import { exportDataSaga } from '../modules/export-data';
import { selectionSaga } from '../modules/selection';
import { transferControlSaga } from '../modules/transfer-control';
import { dataflowDetailsSaga } from '../modules/dataflow-details';
import { categoriesSaga } from '../modules/categories';
import { dumpSaga } from '../modules/dump';

export const createSaga = () => {
  return function* saga() {
    yield all([
      agenciesSaga(),
      artefactsSaga(),
      uploadArtefactSaga(),
      uploadDataSaga(),
      transferArtefactSaga(),
      transferDataSaga(),
      exportDataSaga(),
      selectionSaga(),
      fetchDataflowsSaga(),
      transferControlSaga(),
      dataflowDetailsSaga(),
      categoriesSaga(),
      dumpSaga(),
    ]);
  };
};
