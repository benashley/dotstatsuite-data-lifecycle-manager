import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';
import { createReducer } from './reducer';
import { createSaga } from './saga';

const sagaMiddleware = createSagaMiddleware();

const configureStore = (initialState = {}) => {
  const logger = createLogger({ duration: true, timestamp: false, collapsed: true, diff: true });

  const middlewares = [sagaMiddleware, routerMiddleware(history), logger];

  const store = createStore(
    createReducer(),
    initialState,
    composeWithDevTools(applyMiddleware(...middlewares)),
  );

  sagaMiddleware.run(createSaga());

  return store;
};

export default configureStore;
