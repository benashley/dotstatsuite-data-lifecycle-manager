import * as R from 'ramda';
import { agenciesParser } from './parsers';
import { ALL } from './constants';
import { cartesian } from './utils';

import { getParsedAgenciesStructures } from './requests';
export {
  deleteStructureRequest,
  getArtefactStructure,
  getData,
  getDataflowCategories,
  getDataflowObservationsCount,
  getParsedArtefactStructures,
  getParsedDataflowStructures,
  postStructureRequest,
  transferStructure,
} from './requests';

export const ROOT_AGENCIES = 'SDMX';

export const getAgencies = ({ locale, spaces }) =>
  Promise.all(
    R.pipe(
      R.values,
      R.map(space => getParsedAgenciesStructures({ locale, space })),
    )(spaces),
  ).then(agenciesParser);

const agencyCodeParser = agency =>
  R.startsWith(ROOT_AGENCIES, R.prop('hierarchicalCode', agency))
    ? R.prop('code', agency)
    : R.prop('hierarchicalCode', agency);

const filterAndParseAgenciesBySpace = (agencies, space) =>
  R.reduce(
    (acc, agency) =>
      !R.has('spaces', agency) || R.hasPath(['spaces', space.id], agency)
        ? [...acc, agencyCodeParser(agency)]
        : acc,
    [],
    R.values(agencies),
  );

export const getArtefactsOptionsBySpace = R.pipe(
  ({ agencies, spaces, types, versions }) =>
    R.mapObjIndexed(
      space => ({
        space,
        types: R.isEmpty(types) ? [ALL] : types,
        versions,
        agencies: R.isEmpty(agencies) ? [ALL] : filterAndParseAgenciesBySpace(agencies, space),
      }),
      spaces,
    ),
  R.values,
  R.map(({ agencies, space, types, versions }) =>
    R.map(
      ([agency, type, version]) => ({
        agency,
        code: ALL,
        space,
        type,
        version,
      }),
      cartesian(agencies, types, versions),
    ),
  ),
  R.unnest,
);
