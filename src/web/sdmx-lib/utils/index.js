import * as R from 'ramda';

export const cartesian = (...rest) =>
  R.reduce((acc, array) => R.unnest(R.map(x => R.map(y => [...x, y], array), acc)), [[]], rest);
