import { agenciesParser } from '../parsers';

describe('agencies parser test', () => {
  it('empty result', () => {
    expect(agenciesParser([])).toEqual({ data: {}, log: {} });
  });

  it('irrelevant', () => {
    expect(agenciesParser([{ toto: 'toto' }, { tata: 'tata' }, { tutu: 'tutu' }])).toEqual({
      data: {},
      log: {},
    });
  });

  it('basic logs concat', () => {
    expect(
      agenciesParser([
        { log: { toto: 'toto' } },
        { log: { tata: 'tata' } },
        { log: { tutu: 'tutu' } },
      ]),
    ).toEqual({ data: {}, log: { toto: 'toto', tata: 'tata', tutu: 'tutu' } });
  });

  it('uniq agencies', () => {
    expect(
      agenciesParser([
        {
          data: {
            a: {
              code: 'artefact',
              hierarchicalCode: 'test-artefact',
              label: 'artefact',
              parentId: 'parentA',
            },
            parentA: { agencyId: 'test', type: 'agencyscheme' },
          },
        },
        {
          data: {
            b: {
              code: 'artefact',
              hierarchicalCode: 'test-artefact',
              label: 'artefact',
              parentId: 'parentB',
            },
            parentB: { agencyId: 'test', type: 'agencyscheme' },
          },
        },
        {
          data: {
            c: {
              code: 'artefact',
              hierarchicalCode: 'test-artefact',
              label: 'artefact',
              parentId: 'parentC',
            },
            parentC: { agencyId: 'test', type: 'agencyscheme' },
          },
        },
      ]),
    ).toEqual({
      data: {
        'test:artefact': {
          code: 'artefact',
          hierarchicalCode: 'test-artefact',
          id: 'test:artefact',
          label: 'artefact',
          parentId: 'test',
        },
      },
      log: {},
    });
  });

  it('reject non existing scheme', () => {
    expect(
      agenciesParser([
        {
          data: {
            a: {
              code: 'artefact',
              hierarchicalCode: 'test-artefact',
              label: 'artefact',
              parentId: 'incorrect',
            },
            parentA: { agencyId: 'test', type: 'agencyscheme' },
          },
        },
      ]),
    ).toEqual({ data: {}, log: {} });
  });

  it('multi agencies from same space', () => {
    expect(
      agenciesParser([
        {
          data: {
            a: {
              code: 'artefact',
              hierarchicalCode: 'test-artefact',
              label: 'artefact',
              parentId: 'parentA',
            },
            b: {
              code: 'artefact2',
              hierarchicalCode: 'test-artefact2',
              label: 'artefact2',
              parentId: 'parentA',
            },
            c: {
              code: 'artefact3',
              hierarchicalCode: 'test-artefact3',
              label: 'artefact3',
              parentId: 'parentA',
            },
            parentA: { agencyId: 'test', type: 'agencyscheme' },
          },
        },
        {
          data: {
            b: {
              code: 'artefact',
              hierarchicalCode: 'test-artefact',
              label: 'artefact',
              parentId: 'parentB',
            },
            parentB: { agencyId: 'test', type: 'agencyscheme' },
          },
        },
      ]),
    ).toEqual({
      data: {
        'test:artefact': {
          code: 'artefact',
          hierarchicalCode: 'test-artefact',
          id: 'test:artefact',
          label: 'artefact',
          parentId: 'test',
        },
        'test:artefact2': {
          code: 'artefact2',
          hierarchicalCode: 'test-artefact2',
          id: 'test:artefact2',
          label: 'artefact2',
          parentId: 'test',
        },
        'test:artefact3': {
          code: 'artefact3',
          hierarchicalCode: 'test-artefact3',
          id: 'test:artefact3',
          label: 'artefact3',
          parentId: 'test',
        },
      },
      log: {},
    });
  });
});
