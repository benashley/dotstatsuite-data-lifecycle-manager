import { getArtefactsOptionsBySpace } from '../';
import { ALL } from '../constants';

const spaces = { space1: { id: 'space1' }, space2: { id: 'space2' } };
const agencies = {
  agency1: { hierarchicalCode: 'agency1' },
  agency2: { hierarchicalCode: 'agency2' },
};
const spacedAgencies = {
  agency1: { hierarchicalCode: 'agency1', spaces: { space1: 'space1', space2: 'space2' } },
  agency2: { hierarchicalCode: 'agency2', spaces: { space1: 'space1' } },
};
const agenciesCodes = {
  agency1: { code: 'agency1', hierarchicalCode: 'SDMX.agency1' },
  agency2: { hierarchicalCode: 'agency2' },
};
const types = ['type1', 'type2'];
const versions = ['version1', 'version2'];

describe('getArtefactsOptionsBySpace tests', () => {
  it('basic cartesian test', () => {
    expect(getArtefactsOptionsBySpace({ agencies, spaces, types, versions })).toEqual([
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space1' },
        type: 'type1',
        version: 'version1',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space1' },
        type: 'type1',
        version: 'version2',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space1' },
        type: 'type2',
        version: 'version1',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space1' },
        type: 'type2',
        version: 'version2',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space1' },
        type: 'type1',
        version: 'version1',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space1' },
        type: 'type1',
        version: 'version2',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space1' },
        type: 'type2',
        version: 'version1',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space1' },
        type: 'type2',
        version: 'version2',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space2' },
        type: 'type1',
        version: 'version1',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space2' },
        type: 'type1',
        version: 'version2',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space2' },
        type: 'type2',
        version: 'version1',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space2' },
        type: 'type2',
        version: 'version2',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space2' },
        type: 'type1',
        version: 'version1',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space2' },
        type: 'type1',
        version: 'version2',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space2' },
        type: 'type2',
        version: 'version1',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space2' },
        type: 'type2',
        version: 'version2',
      },
    ]);
  });

  it('restrained agencies spaces combinations', () => {
    expect(
      getArtefactsOptionsBySpace({ agencies: spacedAgencies, spaces, types, versions }),
    ).toEqual([
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space1' },
        type: 'type1',
        version: 'version1',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space1' },
        type: 'type1',
        version: 'version2',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space1' },
        type: 'type2',
        version: 'version1',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space1' },
        type: 'type2',
        version: 'version2',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space1' },
        type: 'type1',
        version: 'version1',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space1' },
        type: 'type1',
        version: 'version2',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space1' },
        type: 'type2',
        version: 'version1',
      },
      {
        agency: 'agency2',
        code: 'all',
        space: { id: 'space1' },
        type: 'type2',
        version: 'version2',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space2' },
        type: 'type1',
        version: 'version1',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space2' },
        type: 'type1',
        version: 'version2',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space2' },
        type: 'type2',
        version: 'version1',
      },
      {
        agency: 'agency1',
        code: 'all',
        space: { id: 'space2' },
        type: 'type2',
        version: 'version2',
      },
    ]);
  });

  it('parse agencies codes', () => {
    expect(
      getArtefactsOptionsBySpace({
        agencies: agenciesCodes,
        spaces: { space: { id: 'space' } },
        types: ['type'],
        versions: ['version'],
      }),
    ).toEqual([
      { agency: 'agency1', code: 'all', space: { id: 'space' }, type: 'type', version: 'version' },
      { agency: 'agency2', code: 'all', space: { id: 'space' }, type: 'type', version: 'version' },
    ]);
  });
});
