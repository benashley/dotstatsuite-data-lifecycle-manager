import * as R from 'ramda';

/*
  rejected for now: each parsed agency referencing spaces where it can be found.
  put aside due to nsi web service limitations regarding agency schemes
*/
const agenciesParser = R.pipe(
  R.map(artefacts =>
    R.reduce(
      (acc, artefact) => {
        if (artefact.type === 'agencyscheme') {
          return acc;
        }
        const scheme = R.propOr({}, artefact.parentId, artefacts);
        const code = artefact.code;
        const id = `${scheme.agencyId}:${artefact.code}`;
        return {
          ...acc,
          [id]: {
            ...R.pick(['label', 'hierarchicalCode'], artefact),
            id,
            code,
            parentId: scheme.agencyId,
            space: R.path(['space', 'id'], artefact),
          },
        };
      },
      {},
      R.values(artefacts),
    ),
  ),
  R.reduce(
    (acc, agencies) => ({
      ...acc,
      ...R.mapObjIndexed((agency, id) => {
        const spaces = R.pathOr({}, [id, 'spaces'], acc);
        const space = R.prop('space', agency);
        return {
          ...R.pick(['id', 'code', 'hierarchicalCode', 'label', 'parentId'], agency),
          spaces: {
            ...spaces,
            [space]: space,
          },
        };
      }, agencies),
    }),
    {},
  ),
);

/*
 structure json parsing proposal
*/

const artefactsResultsKeys = {
  [AGENCY_SCHEME]: 'agencySchemes',
  [CATEGORISATION]: 'categorisations',
  [CATEGORY_SCHEME]: 'categorySchemes',
  [CODELIST]: 'codelists',
  [CONCEPT_SCHEME]: 'conceptSchemes',
  [CONTENT_CONSTRAINT]: 'contentConstraints',
  [DATAFLOW]: 'dataflows',
  [DATA_STRUCTURE]: 'dataStructures',
  [HIERARCHICAL_CODELIST]: 'hierarchicalCodelists',
};

const getLabel = locale => R.path(['name', locale]);

const getArtefactsResponseParser = (locale, space, type) =>
  R.pipe(
    R.path(['data', artefactsResultsKeys[type]]),
    R.reduce((acc, artefact) => {
      const parsed = {
        agencyId: artefact.agencyID,
        code: artefact.id,
        id: `${space.id}:${type}:${artefact.agencyID}:${artefact.id}:${artefact.version}`,
        isFinal: artefact.isFinal,
        label: getLabel(locale)(artefact),
        sdmxId: `${artefact.agencyID}:${artefact.id}(${artefact.version})`,
        space,
        type,
        version: artefact.version,
      };
      return { ...acc, [parsed.id]: parsed };
    }, {}),
  );
