import * as R from 'ramda';

const parseTargetUrls = R.map(categorisation => {
  const targetUrl = categorisation.target;
  const [, schemeAgencyId, schemeId, schemeVersion, path] = targetUrl.match(
    /=([\w_]+):([\w_]+)\(([\d.]+)\).([\w_.]+)$/,
  );
  return { schemeAgencyId, schemeId, schemeVersion, path };
});

const indexCatgeorySchemes = R.reduce((acc, scheme) => {
  const { agencyID, id, version } = R.pick(['agencyID', 'id', 'version'], scheme);
  const index = `${agencyID}:${id}:${version}`;
  return {
    ...acc,
    [index]: scheme,
  };
}, {});

const indexedCategorySchemes = R.pipe(
  R.pathOr([], ['data', 'categorySchemes']),
  indexCatgeorySchemes,
);

const getCategories = (categories, path, locale) => {
  const res = R.reduce(
    ({ result, categories }, id) => {
      if (R.isNil(categories)) {
        return { result };
      }
      const category = R.find(cat => cat.id === id, categories);
      if (R.isNil(category)) {
        return { result };
      }
      const res = [...result, { id, label: R.path(['name', locale], category) }];
      return { result: res, categories: category.categories };
    },
    { result: [], categories },
    R.split('.', path),
  );
  return res.result;
};

const targetsToCategories = (schemes, locale) =>
  R.reduce((acc, { schemeAgencyId, schemeId, schemeVersion, path }) => {
    const index = `${schemeAgencyId}:${schemeId}:${schemeVersion}`;
    const scheme = schemes[index];
    if (R.isNil(scheme)) {
      return acc;
    }
    const categories = getCategories(scheme.categories, path, locale);
    if (R.isNil(categories) || R.isEmpty(categories)) {
      return acc;
    }
    return [...acc, categories];
  }, []);

export default (json, locale) =>
  R.pipe(
    R.pathOr([], ['data', 'categorisations']),
    parseTargetUrls,
    targetsToCategories(indexedCategorySchemes(json), locale),
  )(json);
