import * as R from 'ramda';
import { AGENCY_SCHEME } from '../constants';

export default R.reduce(
  (acc, { data, log }) => {
    let agencies = {};
    R.forEachObjIndexed(artefact => {
      if (artefact.type === AGENCY_SCHEME) {
        return;
      }
      const scheme = R.prop(artefact.parentId, data);
      if (R.isNil(scheme)) {
        return;
      }
      const code = artefact.code;
      const id = `${scheme.agencyId}:${artefact.code}`;
      agencies = {
        ...agencies,
        [id]: {
          ...R.pick(['label', 'hierarchicalCode'], artefact),
          id,
          code,
          parentId: scheme.agencyId,
        },
      };
    }, data);
    return {
      data: {
        ...acc.data,
        ...agencies,
      },
      log: {
        ...acc.log,
        ...log,
      },
    };
  },
  { data: {}, log: {} },
);
