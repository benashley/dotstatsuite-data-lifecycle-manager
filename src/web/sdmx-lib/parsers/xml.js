import sax from 'sax';
import { compact, isArray, isEmpty, isNil, lt, size, some, toLower, trim } from 'lodash';

/*
  No interest to rewrite an xml parser in ramda, better keep lodash until drop xml parser for json
*/

const strictIncludes = (array, value) =>
  some(isArray(array) ? array : [array], item => item === value);

const typeParser = type => {
  if (isEmpty(type)) return;
  const m = /^(common|structure|str|com|mes)?:?(.*)/g.exec(type);
  if (lt(size(m), 2)) return;
  return toLower(m[2]);
};

const artefactParser = (space, type, parentId, parentAgencyId, node) => ({
  space,
  type,
  parentId,
  label: null,
  code: node.attributes.id,
  hierarchicalCode: compact([parentAgencyId, node.attributes.id]).join('.'),
  sdmxId: `${node.attributes.agencyID}:${node.attributes.id}(${node.attributes.version})`,
  id: compact([
    space.id,
    type,
    node.attributes.id,
    node.attributes.agencyID,
    node.attributes.version,
  ]).join(':'),
  version: node.attributes.version,
  isFinal: node.attributes.isFinal === 'true',
  agencyId: node.attributes.agencyID,
});

export const sdmxXmlParser = (parentTypes, types, locale, space) => xml => {
  const constants = { NAME: 'name' };
  const strict = true;
  const options = { lowercase: true };
  const parser = sax.parser(strict, options);
  const sdmx = {};
  let cursor = {}; // current path, xml look-through flatten nodes

  return new Promise((resolve, reject) => {
    parser.onerror = reject;
    parser.onopentag = node => {
      const type = typeParser(node.name);
      if (strictIncludes(types, type)) {
        const artefact = artefactParser(
          space,
          type,
          strictIncludes(parentTypes, type) ? null : cursor.parentId,
          strictIncludes(parentTypes, type) ? null : cursor.agencyId,
          node,
        );
        const parentId = strictIncludes(parentTypes, type) ? artefact.id : cursor.parentId;
        const agencyId = strictIncludes(parentTypes, type) ? artefact.agencyId : cursor.agencyId;
        cursor = { id: artefact.id, parentId, agencyId };
        sdmx[artefact.id] = artefact; // not pure, don't know how to do it differently
      }
      if (type === constants.NAME && !isEmpty(cursor)) {
        cursor['locale'] = node.attributes['xml:lang'];
      }
    };
    parser.ontext = text => {
      if (!isEmpty(cursor) && cursor.locale === locale) {
        sdmx[cursor.id] = { ...sdmx[cursor.id], label: text };
        cursor['locale'] = null; // reset to avoid overwrite
      }
    };
    parser.onend = () => resolve(sdmx);
    parser.write(xml).close();
  });
};

export const sdmxXmlErrorParser = xml => {
  const strict = false;
  const options = { lowercase: true };
  const parser = sax.parser(strict, options);
  let errors = [];

  return new Promise((resolve, reject) => {
    parser.onerror = reject;
    parser.ontext = text => {
      const message = trim(text);
      if (!isEmpty(message) && !isNil(message)) {
        errors.push(message);
      }
    };
    parser.onend = () => resolve(errors);
    parser.write(xml).close();
  });
};
