import axios from 'axios';
import * as R from 'ramda';
import { ALL, AGENCY, AGENCY_SCHEME, DATAFLOW } from './constants';
import { categoriesParser, sdmxXmlParser, sdmxXmlErrorParser } from './parsers';

export const XML_STRUCTURE_FORMAT = 'application/vnd.sdmx.structure+xml;version=2.1;urn=true';
export const JSON_STRUCTURE_FORMAT = 'application/vnd.sdmx.structure+json;version=1.0;urn=true';

export const XML_DATA_FORMAT = 'application/vnd.sdmx.genericdata+xml;version=2.1';
export const JSON_DATA_FORMAT = 'application/vnd.sdmx.data+json;version=1.0.0-wd';
export const CSV_DATA_FORMAT = 'application/vnd.sdmx.data+csv';

export const ALL_STUBS_PARAM = '?detail=allstubs';
export const ALL_REFS_PARAM = '?references=all';
export const PARENT_REFS_PARAM = '?references=parentsandsiblings';

export const SUCCESS_DELETE_CODES = [204];
export const WARNING_DELETE_CODES = [202];

export const SUCCESS_POST_CODES = [201];
export const WARNING_POST_CODES = [202, 207];

//---------------------------------------------------------------------------------options

const rangeHeader = range => (R.isNil(range) ? {} : { Range: `values=0-${range}` });

export const getRequestOptions = (withCredentials, format, locale, cancelToken, range) => ({
  mode: 'cors',
  cancelToken,
  withCredentials,
  headers: {
    Accept: format,
    'Accept-Language': locale,
    ...rangeHeader(range),
  },
});

export const deleteRequestOptions = () => ({
  mode: 'cors',
  withCredentials: true,
});

export const postRequestOptions = (data, locale) => ({
  mode: 'cors',
  withCredentials: true,
  headers: {
    'Accept-Language': locale,
  },
});

export const getDataFormat = format => {
  switch (format) {
    case 'csv':
      return CSV_DATA_FORMAT;
    case 'xml':
      return XML_DATA_FORMAT;
    default:
      return null;
  }
};

//----------------------------------------------------------------------structure requests

export const withLog = id => request =>
  request.catch(error => {
    const log = error.response ? error.response.data : error.request.data;
    return { data: null, log: { [id]: log } };
  });

export const getStructureRequest = (options = {}) => {
  const { agency, code, format, queryParam, space, type, version } = options;
  const url = `${space.endpoint}/${type}/${agency}/${code}/${version}/${
    queryParam ? queryParam : ''
  }`;
  return axios.get(url, getRequestOptions(R.propOr(false, 'credentials', space), format));
  //.then(res => res.data)
};

export const withParsedLog = (successCodes, warningCodes) => request =>
  request
    .catch(error => {
      if (error.response) {
        return sdmxXmlErrorParser(error.response.data).then(data => ({ isError: true, data }));
      }
      throw Error(error.response.status);
    })
    .then(res => {
      if (res.isError) {
        return res;
      }
      if (R.contains(res.status, successCodes)) {
        return { isSuccess: true };
      }
      if (R.contains(res.status, warningCodes)) {
        return sdmxXmlErrorParser(res.data).then(data => ({ isWarning: true, data }));
      }
      throw Error(`unknown status: ${res.status}`);
    });

export const postStructureRequest = (options = {}) => {
  const { space, data, locale } = options;
  const url = `${space.endpoint}/structure/`;
  return withParsedLog(SUCCESS_POST_CODES, WARNING_POST_CODES)(
    axios.post(url, data, postRequestOptions(locale)),
  );
};

export const deleteStructureRequest = (options = {}) => {
  const { agency, code, space, type, version } = options;
  const url = `${space.endpoint}/${type}/${agency}/${code}/${version}/`;
  return withParsedLog(SUCCESS_DELETE_CODES, WARNING_DELETE_CODES)(
    axios.delete(url, deleteRequestOptions()),
  );
};

//---------------------------------------------------------------------------data requests
export const getDataRequest = (options = {}) => {
  const { space, agency, code, version, format } = options;
  const url = `${space.endpoint}/data/${agency},${code},${version}/`;
  return axios
    .get(url, getRequestOptions(R.propOr(false, 'credentials', space), format))
    .then(res => res.data);
};

export const getDataflowObservationsCountRequest = (options = {}) => {
  const { space, agency, code, version, format, cancelToken, range, locale } = options;
  const url = `${space.endpoint}/data/${agency},${code},${version}/`;
  return axios
    .get(
      url,
      getRequestOptions(R.propOr(false, 'credentials', space), format, locale, cancelToken, range),
    )
    .then(res => {
      const range = R.path(['headers', 'content-range'], res);
      if (!R.isNil(range)) {
        const observations = parseInt(R.last(R.split('/', range)));
        return { data: observations };
      }
      throw Error('could not retrieve information');
    })
    .catch(error => {
      if (error.response && error.response.status === 404) {
        return { data: 0 };
      }
      throw Error(error.message);
    });
};

//----------------------------------------------------------------------------with parsing
export const getParsedStructures = ({ locale, parentTypes, parsedTypes, space, ...rest }) =>
  withLog(space.id)(getStructureRequest({ space, ...rest })).then(({ data, log }) =>
    sdmxXmlParser(parentTypes, parsedTypes, locale, space)(data).then(parsed => ({
      data: parsed,
      log,
    })),
  );

export const getParsedAgenciesStructures = options =>
  getParsedStructures({
    ...options, // { locale, space }
    agency: ALL,
    code: ALL,
    format: XML_STRUCTURE_FORMAT,
    parentTypes: [AGENCY_SCHEME],
    parsedTypes: [AGENCY, AGENCY_SCHEME],
    queryParam: null,
    type: AGENCY_SCHEME,
    version: '1.0',
  });

export const getParsedArtefactStructures = ({ type, ...rest }) =>
  getParsedStructures({
    ...rest, // { agency, space, version, code, locale }
    format: XML_STRUCTURE_FORMAT,
    parentTypes: null,
    parsedTypes: [type],
    queryParam: ALL_STUBS_PARAM,
    type,
  });

export const getParsedDataflowStructures = options =>
  getParsedArtefactStructures({
    ...options, // { locale, space }
    agency: ALL,
    code: ALL,
    type: DATAFLOW,
    version: ALL,
  });

export const getArtefactStructure = ({ withReferences, ...rest }) =>
  getStructureRequest({
    ...rest, // { agency, code, space, type, version }
    format: XML_STRUCTURE_FORMAT,
    queryParam: withReferences ? ALL_REFS_PARAM : null,
  }).then(res => res.data);

export const transferStructure = (artefactOptions, destinationSpace) =>
  getArtefactStructure(artefactOptions).then(structure =>
    postStructureRequest({
      locale: R.prop('locale', artefactOptions),
      space: destinationSpace,
      data: structure,
    }),
  );

export const getDataflowCategories = options =>
  getStructureRequest({
    ...options, // { agency, code, locale, space, version }
    format: JSON_STRUCTURE_FORMAT,
    queryParam: PARENT_REFS_PARAM,
    type: DATAFLOW,
  }).then(res => ({ data: categoriesParser(res.data, R.prop('locale', options)) }));

export const getData = ({ format, ...rest }) =>
  getDataRequest({
    ...rest, // { agency, code, locale, space, version }
    format: getDataFormat(format),
  });

export const getDataflowObservationsCount = options =>
  getDataflowObservationsCountRequest({
    ...options, // { agency, cancelToken, code, space, version }
    format: JSON_DATA_FORMAT,
    locale: 'en',
    range: 1,
  });
