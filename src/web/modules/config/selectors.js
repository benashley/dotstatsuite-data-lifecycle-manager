import { createSelector } from 'reselect';
import { pipe, path, map, indexBy, prop, toPairs } from 'ramda';
import { keyBy, pickBy } from 'lodash';
import { withSort } from '../common/utils';

const getState = state => state.config;

export const getUploadSizeLimit = createSelector(
  getState,
  path(['sdmx', 'uploadSizeLimit']),
);

export const getSpaces = () =>
  createSelector(
    getState,
    pipe(
      path(['sdmx', 'datasources']),
      toPairs,
      map(([id, { url, ...rest }]) => ({ id, endpoint: url, ...rest })),
      indexBy(prop('id')),
    ),
  );

export const getTypes = () =>
  createSelector(
    getState,
    pipe(
      path(['sdmx', 'typeIds']),
      map(id => ({ id, label: id })),
      indexBy(prop('id')),
    ),
  );

export const getInternalSpaces = () =>
  createSelector(
    [getSpaces()],
    spaces => pickBy(spaces, space => !space.isExternal),
  );

export const getExternalSpaces = () =>
  createSelector(
    [getSpaces()],
    spaces => pickBy(spaces, space => !!space.isExternal),
  );

export const getSpacesForApi = () =>
  createSelector(
    [getSpaces()],
    spaces => keyBy(spaces, 'endpoint'),
  );

export const getSortedSpaces = (fields = []) =>
  createSelector(
    [getSpaces()],
    withSort(fields),
  );

export const getSortedInternalSpaces = (fields = []) =>
  createSelector(
    [getInternalSpaces()],
    withSort(fields),
  );

export const getSortedExternalSpaces = (fields = []) =>
  createSelector(
    [getExternalSpaces()],
    withSort(fields),
  );
