import * as R from 'ramda';
export default R.when(R.isNil, R.always({}));

export { default as withConfig } from './with-config';
export {
  getSpaces,
  getTypes,
  getSpacesForApi,
  getUploadSizeLimit,
  getInternalSpaces,
  getExternalSpaces,
} from './selectors';
