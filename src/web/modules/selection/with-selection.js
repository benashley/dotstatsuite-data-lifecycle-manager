import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  exportArtefacts,
  exportDatas,
  deleteArtefacts,
  resetSelection,
  toggleArtefact,
  transferArtefacts,
  transferDatas,
} from './action-creators';
import {
  getDestinationSpaces,
  getHasSelection,
  getIsSelectable,
  getIsSelected,
  getIsSelectionDeletable,
  getIsSelectionHasData,
  getSelectedSpace,
  getSelection,
} from './selectors';

export const withSelection = Component => {
  const mapDispatchToProps = {
    exportArtefacts,
    exportDatas,
    deleteArtefacts,
    resetSelection,
  };

  const mapStateToProps = createStructuredSelector({
    hasSelection: getHasSelection(),
    isSelectionDeletable: getIsSelectionDeletable(),
    isSelectionHasData: getIsSelectionHasData(),
    selection: getSelection(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};

//doesn't take directly Component to have same signature as withTransfer for item-transfers generation
export const withSelectionArtefactsTransfer = () => Component => {
  const mapDispatchToProps = {
    transfer: transferArtefacts,
  };

  const mapStateToProps = createStructuredSelector({
    destinationSpaces: getDestinationSpaces(),
    sourceSpace: getSelectedSpace(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};

export const withSelectionDatasTransfer = () => Component => {
  const mapDispatchToProps = {
    transfer: transferDatas,
  };

  const mapStateToProps = createStructuredSelector({
    destinationSpaces: getDestinationSpaces(),
    sourceSpace: getSelectedSpace(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};

export const withSelectedState = id => Component => {
  const mapDispatchToProps = {
    toggleArtefact,
  };

  const mapStateToProps = createStructuredSelector({
    isSelectable: getIsSelectable(id),
    isSelected: getIsSelected(id),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
