import { all, takeEvery, takeLatest, put, select } from 'redux-saga/effects';
import { map } from 'lodash';
import { getSelection, getSpace, getWithReferences } from './selectors';
import {
  EXPORT_ARTEFACTS,
  EXPORT_DATAS,
  DELETE_ARTEFACTS,
  deselectArtefact,
  resetSelection,
  TRANSFER_ARTEFACTS,
  TRANSFER_DATAS,
} from './action-creators';
import {
  ARTEFACT_DELETE,
  ARTEFACT_DELETE_SUCCESS,
  ARTEFACT_EXPORT_STRUCTURE,
  ARTEFACT_EXPORT_STRUCTURE_SUCCESS,
  ARTEFACTS_FETCH_SUCCESS,
} from '../artefacts';
import { EXPORT_DATA, EXPORT_DATA_SUCCESS } from '../export-data';
import { TRANSFER_ARTEFACT, TRANSFER_ARTEFACT_SUCCESS } from '../transfer-artefact';
import { TRANSFER_DATA, TRANSFER_DATA_SUCCESS } from '../transfer-data';

export const workerFactory = baseActionType => {
  return function* worker() {
    const ids = yield select(getSelection());
    const space = yield select(getSpace());
    const withReferences = yield select(getWithReferences());

    yield all(
      map(ids, ({ id }) => put({ type: baseActionType, payload: { id, space, withReferences } })),
    );
  };
};

export function* deselectArtefactWorker(action) {
  const id = action.payload.id;

  yield put(deselectArtefact(id));
}

export function* resetSelectionWorker() {
  yield put(resetSelection());
}

export default function* saga() {
  yield takeLatest(DELETE_ARTEFACTS, workerFactory(ARTEFACT_DELETE));
  yield takeLatest(EXPORT_ARTEFACTS, workerFactory(ARTEFACT_EXPORT_STRUCTURE));
  yield takeLatest(EXPORT_DATAS, workerFactory(EXPORT_DATA));
  yield takeLatest(TRANSFER_ARTEFACTS, workerFactory(TRANSFER_ARTEFACT));
  yield takeLatest(TRANSFER_DATAS, workerFactory(TRANSFER_DATA));
  yield takeLatest(ARTEFACTS_FETCH_SUCCESS, resetSelectionWorker);
  yield takeEvery(
    [
      ARTEFACT_DELETE_SUCCESS,
      ARTEFACT_EXPORT_STRUCTURE_SUCCESS,
      EXPORT_DATA_SUCCESS,
      TRANSFER_ARTEFACT_SUCCESS,
      TRANSFER_DATA_SUCCESS,
    ],
    deselectArtefactWorker,
  );
}
