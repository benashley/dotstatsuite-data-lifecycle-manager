import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const selectionReducer = reducerFactory(model(), handlers);
export { default as selectionSaga } from './saga';
export {
  withSelectedState,
  withSelection,
  withSelectionArtefactsTransfer,
  withSelectionDatasTransfer,
} from './with-selection';
