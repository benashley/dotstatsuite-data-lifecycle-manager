import { createSelector } from 'reselect';
import { find, get, has, isEmpty, every, includes, reject } from 'lodash';
import { NAMESPACE } from './constants';
import { getArtefact, getArtefacts, ARTEFACT_TYPES_WITH_DATA } from '../artefacts';
import { getInternalSpaces } from '../config';

const getState = state => state[NAMESPACE];

export const getSelection = () =>
  createSelector(
    getState,
    state => get(state, 'selection', {}),
  );

export const getHasSelection = () =>
  createSelector(
    [getSelection()],
    selection => !isEmpty(selection),
  );

export const getIsSelectionHasData = () =>
  createSelector(
    [getArtefacts(), getSelection()],
    (artefacts, selection) =>
      every(selection, ({ id }) => {
        const artefact = get(artefacts, id);
        return includes(ARTEFACT_TYPES_WITH_DATA, get(artefact, 'type'));
      }),
  );

export const getSelectedSpace = () =>
  createSelector(
    [getArtefacts(), getSelection()],
    (artefacts, selection) => {
      const selectedArtefactId = get(find(selection), 'id');
      const artefact = get(artefacts, selectedArtefactId);
      return get(artefact, 'space', null);
    },
  );

export const getIsSelectionDeletable = () =>
  createSelector(
    [getSelectedSpace(), getInternalSpaces()],
    (space, internalSpaces) => has(internalSpaces, get(space, 'id')),
  );

export const getDestinationSpaces = () =>
  createSelector(
    [getSelectedSpace(), getInternalSpaces()],
    (space, internalSpaces) => reject(internalSpaces, { id: get(space, 'id') }),
  );

//--------------------------------------------------------------------------------------------------

export const getIsSelected = id =>
  createSelector(
    [getSelection()],
    selection => has(selection, id),
  );

export const getIsSelectable = id =>
  createSelector(
    [getArtefact(id), getSelection(), getSelectedSpace()],
    (artefact, selection, space) =>
      isEmpty(selection) || get(artefact, 'space.id', null) === get(space, 'id', null),
  );

//--------------------------------------------------------------------------------------------------
//destination space, used in saga worker
export const getSpace = () =>
  createSelector(
    getState,
    state => get(state, 'space', null),
  );

export const getWithReferences = () =>
  createSelector(
    getState,
    state => get(state, 'withReferences', null),
  );
