import { get, omit } from 'lodash';
import { MODULE } from './constants';

export const EXPORT_ARTEFACTS = `${MODULE}_EXPORT_ARTEFACTS`;
//ids just here, to make it have the same signature as exportArtefact in actions component
export const exportArtefacts = (ids, withReferences) => ({
  type: EXPORT_ARTEFACTS,
  payload: { withReferences },
});
export const exportArtefactsHandler = (state, action) => ({
  ...state,
  withReferences: action.payload.withReferences,
});

export const EXPORT_DATAS = `${MODULE}_EXPORT_DATAS`;
export const exportDatas = () => ({
  type: EXPORT_DATAS,
  payload: {},
});

export const DELETE_ARTEFACTS = `${MODULE}_DELETE_ARTEFACTS`;
export const deleteArtefacts = () => ({
  type: DELETE_ARTEFACTS,
  payload: {},
});

export const TRANSFER_ARTEFACTS = `${MODULE}_TRANSFER_ARTEFACTS`;
//same as exportArtefacts for transfers component
export const transferArtefacts = (ids, space, withReferences) => ({
  type: TRANSFER_ARTEFACTS,
  payload: { space, withReferences },
});
export const transferArtefactsHandler = (state, action) => ({
  ...state,
  space: action.payload.space,
  withReferences: action.payload.withReferences,
});

export const TRANSFER_DATAS = `${MODULE}_TRANSFER_DATAS`;
//same as transferArtefacts
export const transferDatas = (ids, space) => ({
  type: TRANSFER_DATAS,
  payload: { space },
});
export const transferDatasHandler = (state, action) => ({
  ...state,
  space: action.payload.space,
});

//--------------------------------------------------------------------------------------------------

export const TOGGLE_ARTEFACT = `${MODULE}_TOGGLE_ARTEFACT`;
export const toggleArtefact = id => ({
  type: TOGGLE_ARTEFACT,
  payload: { id },
});
export const toggleArtefactHandler = (state, action) => ({
  ...state,
  selection: get(state.selection, action.payload.id)
    ? omit(state.selection, action.payload.id)
    : { ...state.selection, [action.payload.id]: { id: action.payload.id } },
});

export const DESELECT_ARTEFACT = `${MODULE}_DESELECT_ARTEFACT`;
export const deselectArtefact = id => ({
  type: DESELECT_ARTEFACT,
  payload: { id },
});
export const deselectArtefactHandler = (state, action) => ({
  ...state,
  selection: omit(state.selection, action.payload.id),
});

export const RESET_SELECTION = `${MODULE}_RESET_SELECTION`;
export const resetSelection = () => ({
  type: RESET_SELECTION,
  payload: {},
});
export const resetSelectionHandler = state => ({
  ...state,
  selection: {},
});

export default {
  [DESELECT_ARTEFACT]: deselectArtefactHandler,
  [EXPORT_ARTEFACTS]: exportArtefactsHandler,
  [RESET_SELECTION]: resetSelectionHandler,
  [TOGGLE_ARTEFACT]: toggleArtefactHandler,
  [TRANSFER_ARTEFACTS]: transferArtefactsHandler,
  [TRANSFER_DATAS]: transferDatasHandler,
};
