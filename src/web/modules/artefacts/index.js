import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const artefactsReducer = reducerFactory(model(), handlers);

export { ARTEFACT_TYPES_WITH_DATA, SUCCESS, WARNING } from './constants';
export { default as artefactsSaga } from './saga';
export { default as withArtefacts } from './with-artefacts';
export { getArtefact, getArtefacts } from './selectors';
export {
  ARTEFACT_DELETE,
  ARTEFACT_DELETE_SUCCESS,
  ARTEFACT_EXPORT_STRUCTURE,
  ARTEFACT_EXPORT_STRUCTURE_SUCCESS,
  ARTEFACTS_FETCH_SUCCESS,
} from './action-creators';
