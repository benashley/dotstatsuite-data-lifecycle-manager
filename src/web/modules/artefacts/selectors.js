import { createSelector } from 'reselect';
import { get, pickBy } from 'lodash';
import { getIsFinal } from '../filters';

const getState = state => state.artefacts;

export const getArtefacts = () =>
  createSelector(
    getState,
    state => state.artefacts,
  );
export const getArtefact = id =>
  createSelector(
    [getArtefacts()],
    artefacts => get(artefacts, id),
  );
export const getIsFetching = () =>
  createSelector(
    getState,
    state => state.isFetching,
  );

export const getFilteredArtefacts = () =>
  createSelector(
    [getArtefacts(), getIsFinal()],
    (artefacts, isFinal) => pickBy(artefacts, artefact => !isFinal || artefact.isFinal),
  );
