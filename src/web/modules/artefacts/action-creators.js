import { MODULE } from './constants';
import { pickBy } from 'lodash';

//--------------------------------------------------------------------------------------------------
export const ARTEFACT_FETCH = `${MODULE}_ARTEFACT_FETCH`;
export const artefactFetch = id => ({
  type: ARTEFACT_FETCH,
  payload: { id },
});
export const artefactFetchHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isUpdating: true,
    },
  },
});

export const ARTEFACT_FETCH_SUCCESS = `${MODULE}_ARTEFACT_FETCH_SUCCESS`;
export const artefactFetchSuccess = (id, artefact) => ({
  type: ARTEFACT_FETCH_SUCCESS,
  payload: { id, artefact },
});
export const artefactFetchSuccessHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isUpdating: false,
    },
    [action.payload.artefact.id]: {
      ...action.payload.artefact,
      isNew: true,
    },
  },
});

export const ARTEFACT_FETCH_ERROR = `${MODULE}_ARTEFACT_FETCH_ERROR`;
export const artefactFetchError = (id, error) => ({
  type: ARTEFACT_FETCH_ERROR,
  payload: { id, error },
});
export const artefactFetchErrorHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      error: { type: 'fetch', ...action.payload.error },
      isUpdating: false,
    },
  },
});

//--------------------------------------------------------------------------------------------------
export const ARTEFACTS_CLEAR = 'ARTEFACTS_CLEAR';
export const artefactsClear = () => ({ type: ARTEFACTS_CLEAR });
export const artefactsClearHandler = state => ({
  ...state,
  artefacts: {},
});

export const ARTEFACTS_FETCH = 'ARTEFACTS_FETCH';
export const artefactsFetch = () => ({ type: ARTEFACTS_FETCH });
export const artefactsFetchHandler = state => ({
  ...state,
  isFetching: true,
});

export const ARTEFACTS_FETCH_SUCCESS = 'ARTEFACTS_FETCH_SUCCESS';
export const artefactsFetchSuccess = artefacts => ({
  type: ARTEFACTS_FETCH_SUCCESS,
  payload: { artefacts },
});
export const artefactsFetchSuccessHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    ...action.payload.artefacts,
  },
});

export const ARTEFACTS_FETCH_ERROR = 'ARTEFACTS_FETCH_ERROR';
export const artefactsFetchError = error => ({
  type: ARTEFACTS_FETCH_ERROR,
  payload: { error },
});
export const artefactsFetchErrorHandler = (state, action) => ({
  ...state,
  error: action.payload.error,
});

export const ARTEFACTS_FETCH_DONE = 'ARTEFACTS_FETCH_DONE';
export const artefactsFetchDone = () => ({
  type: ARTEFACTS_FETCH_DONE,
});
export const artefactsFetchDoneHandler = state => ({
  ...state,
  isFetching: false,
});

//--------------------------------------------------------------------------------------------------
export const ARTEFACT_DELETE = `${MODULE}_ARTEFACT_DELETE`;
export const artefactDelete = id => ({ type: ARTEFACT_DELETE, payload: { id } });
export const artefactDeleteHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isDeleting: true,
    },
  },
});

export const ARTEFACT_DELETE_SUCCESS = `${MODULE}_ARTEFACT_DELETE_SUCCESS`;
export const artefactDeleteSuccess = id => ({
  type: ARTEFACT_DELETE_SUCCESS,
  payload: { id },
});
export const artefactDeleteSuccessHandler = (state, action) => ({
  ...state,
  artefacts: pickBy(state.artefacts, artefact => artefact.id !== action.payload.id),
});

export const ARTEFACT_DELETE_ERROR = `${MODULE}_ARTEFACT_DELETE_ERROR`;
export const artefactDeleteError = (id, error) => ({
  type: ARTEFACT_DELETE_ERROR,
  payload: { id, error },
});
export const artefactDeleteErrorHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      error: { type: 'delete', ...action.payload.error },
      isDeleting: false,
    },
  },
});

//--------------------------------------------------------------------------------------------------
export const ARTEFACT_EXPORT_STRUCTURE = `${MODULE}_ARTEFACT_EXPORT_STRUCTURE`;
export const artefactExportStructure = (id, withReferences) => ({
  type: ARTEFACT_EXPORT_STRUCTURE,
  payload: { id, withReferences },
});
export const artefactExportStructureHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isExporting: true,
    },
  },
});

export const ARTEFACT_EXPORT_STRUCTURE_SUCCESS = `${MODULE}_ARTEFACT_EXPORT_STRUCTURE_SUCCESS`;
export const artefactExportStructureSuccess = id => ({
  type: ARTEFACT_EXPORT_STRUCTURE_SUCCESS,
  payload: { id },
});
export const artefactExportStructureSuccessHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      isExporting: false,
    },
  },
});

export const ARTEFACT_EXPORT_STRUCTURE_ERROR = `${MODULE}_ARTEFACT_EXPORT_STRUCTURE_ERROR`;
export const artefactExportStructureError = (id, error) => ({
  type: ARTEFACT_EXPORT_STRUCTURE_ERROR,
  payload: { id, error },
});
export const artefactExportStructureErrorHandler = (state, action) => ({
  ...state,
  artefacts: {
    ...state.artefacts,
    [action.payload.id]: {
      ...state.artefacts[action.payload.id],
      error: { type: 'export', ...action.payload.error },
      isExporting: false,
    },
  },
});

//--------------------------------------------------------------------------------------------------
export default {
  [ARTEFACTS_FETCH]: artefactsFetchHandler,
  [ARTEFACTS_FETCH_SUCCESS]: artefactsFetchSuccessHandler,
  [ARTEFACTS_FETCH_ERROR]: artefactsFetchErrorHandler,
  [ARTEFACTS_FETCH_DONE]: artefactsFetchDoneHandler,
  [ARTEFACTS_CLEAR]: artefactsClearHandler,
  [ARTEFACT_FETCH]: artefactFetchHandler,
  [ARTEFACT_FETCH_SUCCESS]: artefactFetchSuccessHandler,
  [ARTEFACT_FETCH_ERROR]: artefactFetchErrorHandler,
  [ARTEFACT_DELETE]: artefactDeleteHandler,
  [ARTEFACT_DELETE_SUCCESS]: artefactDeleteSuccessHandler,
  [ARTEFACT_DELETE_ERROR]: artefactDeleteErrorHandler,
  [ARTEFACT_EXPORT_STRUCTURE]: artefactExportStructureHandler,
  [ARTEFACT_EXPORT_STRUCTURE_SUCCESS]: artefactExportStructureSuccessHandler,
  [ARTEFACT_EXPORT_STRUCTURE_ERROR]: artefactExportStructureErrorHandler,
};
