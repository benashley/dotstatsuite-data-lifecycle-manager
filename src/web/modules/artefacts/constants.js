export const MODULE = 'ARTEFACTS';
export const SUCCESS = 'SUCCESS';
export const WARNING = 'WARNING';
export const ARTEFACT_TYPES_WITH_DATA = ['dataflow'];
