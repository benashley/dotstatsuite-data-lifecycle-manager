import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getFilteredArtefacts, getIsFetching } from './selectors';
import { artefactDelete, artefactExportStructure } from './action-creators';

export default Component => {
  const mapDispatchToProps = {
    artefactDelete,
    artefactExportStructure,
  };

  const mapStateToProps = createStructuredSelector({
    artefacts: getFilteredArtefacts(),
    isFetching: getIsFetching(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
