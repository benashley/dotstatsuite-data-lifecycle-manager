// export function: model can be configurable and won't be modified if recalled
export default () => ({
  // available artefacts => { artefactId: { id, label, type } }
  artefacts: {},

  // error about artefacts
  error: null,

  // fetch in progress about artefacts
  isFetching: false,
});
