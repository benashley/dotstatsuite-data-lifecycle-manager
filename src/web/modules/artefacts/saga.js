import { all, takeEvery, takeLatest, call, put, select } from 'redux-saga/effects';
import axios from 'axios';
import { head, isEmpty, values } from 'lodash';
import { getLocale, CHANGE_LOCALE } from '../i18n';
import {
  artefactFetch,
  artefactFetchSuccess,
  artefactFetchError,
  artefactsFetchDone,
  artefactsFetch,
  artefactsFetchSuccess,
  artefactsFetchError,
  artefactsClear,
  ARTEFACT_DELETE,
  artefactDeleteSuccess,
  artefactDeleteError,
  ARTEFACT_EXPORT_STRUCTURE,
  artefactExportStructureSuccess,
  artefactExportStructureError,
} from './action-creators';
import { artefactExportStructure } from '../../api/artefacts';
import { getSelection, FILTERS_TOGGLE_IS_FINAL } from '../filters';
import { getArtefactOptions, getTransferArtefactOptions } from '../common/selectors';
import { TRANSFER_ARTEFACT_SUCCESS } from '../transfer-artefact';
import { deleteStructureRequest, getParsedArtefactStructures } from '../../sdmx-lib';
import { getArtefactsListsRequestsArgs, parseStubLists } from '@sis-cc/dotstatsuite-sdmxjs';
import { AGENCIES_FETCH_SUCCESS } from '../agencies';
import * as R from 'ramda';

export function* fetchArtefactWorker(action) {
  const id = action.payload.id;
  yield put(artefactFetch(id));

  try {
    const locale = yield select(getLocale());
    const artefactOptions = yield select(getTransferArtefactOptions(id));
    const { data } = yield call(getParsedArtefactStructures, { ...artefactOptions, locale });
    yield put(artefactFetchSuccess(id, head(values(data))));
  } catch (error) {
    yield put(artefactFetchError(id, error));
  }
}

const request = ({ url, headers, params, parsing }) => {
  return axios
    .get(url, { headers, params })
    .then(res => parseStubLists(res.data))
    .then(
      R.pipe(
        R.when(R.always(parsing.fromCategory), R.omit('categoryscheme')),
        R.values,
        R.unnest,
        R.reduce((acc, artefact) => {
          const agencyIds = R.propOr([], 'agencyIds', parsing);
          if (!R.isEmpty(agencyIds) && !R.includes(artefact.agencyId, agencyIds)) {
            return acc;
          }
          const space = R.propOr({}, 'space', parsing);
          const id = `${space.id}:${artefact.type}:${artefact.sdmxId}`;
          return R.assoc(id, { id, space, ...artefact }, acc);
        }, {}),
      ),
    );
};

export function* fetchArtefactsListWorker(requestArgs) {
  try {
    const artefacts = yield call(request, requestArgs);
    yield put(artefactsFetchSuccess(artefacts, requestArgs.parsing.space));
  } catch (error) {
    yield put(artefactsFetchError(error, requestArgs.parsing.space));
  }
}

export function* fetchAllArtefactsWorker() {
  yield put(artefactsClear());
  const selection = yield select(getSelection());
  if (isEmpty(selection.spaces) || isEmpty(selection.types)) {
    return;
  }
  const locale = yield select(getLocale());
  const requestsArgs = getArtefactsListsRequestsArgs({ ...selection, locale });
  yield put(artefactsFetch());
  yield all(R.map(args => call(fetchArtefactsListWorker, args), requestsArgs));
  yield put(artefactsFetchDone());
}

export function* artefactExportStructureWorker(action) {
  const id = action.payload.id;
  const withReferences = action.payload.withReferences;

  try {
    const artefactOptions = yield select(getArtefactOptions(id));
    yield call(artefactExportStructure, artefactOptions, withReferences);
    yield put(artefactExportStructureSuccess(id));
  } catch (error) {
    yield put(artefactExportStructureError(id, error));
  }
}

export function* deleteArtefactWorker(action) {
  const id = action.payload.id;

  try {
    const options = yield select(getArtefactOptions(id));
    const log = yield call(deleteStructureRequest, options);
    if (log.isError) {
      yield put(artefactDeleteError(id, log.data));
    } else {
      yield put(artefactDeleteSuccess(id));
    }
  } catch (error) {
    yield put(artefactDeleteError(id, error));
  }
}

export default function* saga() {
  yield takeLatest(action => {
    return (
      action.type === AGENCIES_FETCH_SUCCESS ||
      action.type === CHANGE_LOCALE ||
      (/^FILTERS_*/.test(action.type) && action.type !== FILTERS_TOGGLE_IS_FINAL)
    );
  }, fetchAllArtefactsWorker);
  yield takeEvery(TRANSFER_ARTEFACT_SUCCESS, fetchArtefactWorker);
  yield takeEvery(ARTEFACT_DELETE, deleteArtefactWorker);
  yield takeEvery(ARTEFACT_EXPORT_STRUCTURE, artefactExportStructureWorker);
}
