import { all, takeLatest, call, put, select } from 'redux-saga/effects';
import axios from 'axios';
import fileSaver from 'file-saver';
import * as R from 'ramda';
import {
  getArtefactsListRequestArgs,
  getRequestArgs,
  parseOverviewStructureList,
} from '@sis-cc/dotstatsuite-sdmxjs';
import {
  dumpDone,
  dumpDownloadFileSuccess,
  dumpDownloadFileError,
  dumpDownloadDataflowsError,
  DUMP_REQUEST,
} from './action-creators';
import { getTypes } from '../config';
import { getSelectedSpace } from './selectors';
import { getArtefactsFilename, getDataflowFilename, MIME_TYPES } from './utils';

export const download = (data, filename, format) => {
  let blob = new Blob([data], MIME_TYPES[format]);
  fileSaver.saveAs(blob, filename);
};

export function* downloadArtefactsList({ type, space }) {
  const format = 'xml';
  const { url, headers } = getRequestArgs({
    type,
    datasource: { url: space.endpoint },
    identifiers: { agencyId: 'all', code: 'all', version: 'all' },
    format,
  });
  const filename = getArtefactsFilename({ type, format });
  try {
    const response = yield call(axios.get, url, { headers });
    yield call(download, response.data, filename, format);
    yield put(dumpDownloadFileSuccess(filename));
  } catch (error) {
    yield put(dumpDownloadFileError(filename, error));
  }
}

export function* downloadDataflowStructure({ dataflow, space }) {
  const format = 'xml';
  const { agencyId, code, version } = R.pick(['agencyId', 'code', 'version'], dataflow);
  const { url, headers, params } = getRequestArgs({
    datasource: { url: space.endpoint },
    identifiers: { agencyId, code, version },
    type: 'dataflow',
    format,
  });
  const filename = getDataflowFilename({ ...dataflow, type: 'structure', format });
  try {
    const response = yield call(axios.get, url, { headers, params });
    yield call(download, response.data, filename, format);
    yield put(dumpDownloadFileSuccess(filename));
  } catch (error) {
    yield put(dumpDownloadFileError(filename, error));
  }
}

export function* downloadDataflowData({ dataflow, space }) {
  const format = 'csv';
  const { agencyId, code, version } = R.pick(['agencyId', 'code', 'version'], dataflow);
  const { url, headers, params } = getRequestArgs({
    datasource: { url: space.endpoint },
    identifiers: { agencyId, code, version },
    type: 'data',
    format,
  });
  const filename = getDataflowFilename({ ...dataflow, type: 'data', format });
  try {
    const response = yield call(axios.get, url, { headers, params });
    yield call(download, response.data, filename, format);
    yield put(dumpDownloadFileSuccess(filename));
  } catch (error) {
    yield put(dumpDownloadFileError(filename, error));
  }
}

export function* downloadDataflow({ dataflow, space }) {
  yield call(downloadDataflowStructure, { dataflow, space });
  yield call(downloadDataflowData, { dataflow, space });
}

const request = ({ url, headers, params, parsing }) => {
  return axios
    .get(url, { headers, params })
    .then(res => parseOverviewStructureList(parsing)(res.data));
};

export function* downloadDataflows(space) {
  const args = getArtefactsListRequestArgs({
    agencyId: 'all',
    code: 'all',
    datasource: { url: space.endpoint },
    type: 'dataflow',
    version: 'all',
  });
  try {
    const dataflows = yield call(request, args);
    yield all(R.mapObjIndexed(dataflow => call(downloadDataflow, { dataflow, space }), dataflows));
  } catch (error) {
    yield put(dumpDownloadDataflowsError(error));
  }
}

export function* dumpDownloadWorker() {
  const types = yield select(getTypes());
  const space = yield select(getSelectedSpace());
  yield all(
    R.map(
      type => call(downloadArtefactsList, { space, type }),
      R.pipe(
        R.omit(['dataflow']),
        R.keys,
      )(types),
    ),
  );
  yield call(downloadDataflows, space);
  yield put(dumpDone());
}

export default function* saga() {
  yield takeLatest(DUMP_REQUEST, dumpDownloadWorker);
}
