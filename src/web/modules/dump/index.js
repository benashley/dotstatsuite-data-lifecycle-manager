import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const dumpReducer = reducerFactory(model(), handlers);
export { default as withDump } from './with-dump';
export { default as dumpSaga } from './saga';
