import { createSelector } from 'reselect';
import { get, isEmpty, size } from 'lodash';
import { getUploadSizeLimit } from '../config';

export default getState => {
  const selectorHelper = key => () =>
    createSelector(
      getState,
      state => get(state, key, {}),
    );
  const getFiles = selectorHelper('files');
  const getSpaces = selectorHelper('spaces');
  const getAcceptedFiles = getFiles;
  const getDropzoneConfig = selectorHelper('dropzoneConfig');
  const getLog = selectorHelper('log');

  return {
    getState,
    getDropzoneConfig,
    getIsUploading: selectorHelper('isUploading'),
    getIsFlushable: selectorHelper('flushable'),
    getLog,
    getSpaces,
    getFiles,
    getAcceptedFiles, // default: all files are accepted
    getRejectedFiles: () => () => [], // default: no rejection
    getHasNoFile: () =>
      createSelector(
        [getFiles()],
        files => isEmpty(files),
      ),
    hasUploadInAllSpace: () =>
      createSelector(
        [getLog(), getSpaces()],
        (log, spaces) => size(log) === size(spaces),
      ),
    getHasDataflowSelection: selectorHelper('hasDataflowSelection'),
    getHasPathOption: () => () => false,
    getMaxSize: () =>
      createSelector(
        getUploadSizeLimit,
        sizeLimit => sizeLimit,
      ),
  };
};
