import { pick } from 'lodash';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

export default (actions, selectors) => Component => {
  const mapDispatchToProps = pick(actions, [
    'filesChange',
    'fileRemove',
    'upload',
    'formReset',
    'selectSpace',
    'removeSpace',
  ]);

  const mapStateToProps = createStructuredSelector({
    dropzoneConfig: selectors.getDropzoneConfig(),
    isUploading: selectors.getIsUploading(),
    log: selectors.getLog(),
    acceptedFiles: selectors.getKeyedAcceptedFiles(),
    rejectedFiles: selectors.getKeyedRejectedFiles(),
    maxSize: selectors.getMaxSize(),
    files: selectors.getFiles(),
    hasNoFile: selectors.getHasNoFile(),
    spaces: selectors.getSpaces(),
    formIsValid: selectors.getFormIsValid(),
    hasDataflowSelection: selectors.getHasDataflowSelection(),
    hasPathOption: selectors.getHasPathOption(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
