export const SUCCESS = 'SUCCESS';
export const WARNING = 'WARNING';
export const ERROR = 'ERROR';
export const MIME_TYPE_XML = 'application/xml, text/xml';
export const MIME_TYPE_XLSX = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
export const MIME_TYPE_CSV = 'application/vnd.ms-excel, application/csv, text/csv, .csv';
export const MIME_TYPE_ZIP =
  'application/zip, application/x-7z-compressed, application/x-zip-compressed';
export const EXCEL_EDD = 'EXCEL_EDD';
export const SDMX_CSV = 'SDMX_CSV';
