import { createSelector } from 'reselect';
import { get, map, reduce, groupBy } from 'lodash';
import { getAgencies as getAgenciesFromFilters } from '../filters';
import { css } from 'glamor';

const getState = state => state.agencies;

export const getIsFetching = () =>
  createSelector(
    getState,
    state => state.isFetching,
  );
export const getAgencies = () =>
  createSelector(
    getState,
    state => state.agencies,
  );
export const getExpanded = () =>
  createSelector(
    getState,
    state => state.expanded,
  );

export const ROOT_AGENCY_SCHEME_ID = 'SDMX';

const CodeLabelStyle = css({ color: 'grey', fontStyle: 'italic' });

export const getAgenciesFlatFilterItems = () =>
  createSelector(
    [getAgencies(), getExpanded()],
    (agencies, expanded) =>
      reduce(
        agencies,
        (memo, agency) => {
          const isExpanded = !!get(expanded, agency.id, false);
          let _agency = {};
          if (!get(agency, 'label', null)) {
            _agency = { label: agency.code, className: `${CodeLabelStyle}` };
          }
          return {
            ...memo,
            [agency.id]: {
              ...agency,
              ..._agency,
              isExpanded,
            },
          };
        },
        {},
      ),
  );

const recurse = (agencies, agencyId, codeSelector = 'hierarchicalCode') =>
  map(agencies[agencyId], agency => {
    if (
      get(agencies, agency[codeSelector], null) &&
      agency[codeSelector] !== ROOT_AGENCY_SCHEME_ID
    ) {
      return {
        ...agency,
        childNodes: recurse(agencies, agency[codeSelector]),
      };
    }
    return agency;
  });

export const getAgenciesTree = () =>
  createSelector(
    [getAgenciesFromFilters()],
    agencies => {
      const groupedItems = groupBy(agencies, item => item.parentId);
      return recurse(groupedItems, ROOT_AGENCY_SCHEME_ID, 'code');
    },
  );
