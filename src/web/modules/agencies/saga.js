import { call, put, select, takeLatest } from 'redux-saga/effects';
import { agenciesFetch, agenciesFetchSuccess, agenciesFetchError } from './action-creators';
import { getSpacesForApi } from '../config';
import { getLocale, CHANGE_LOCALE } from '../i18n';
import { getAgencies as sdmxLibGetAgencies } from '../../sdmx-lib';
import { USER_SIGNED_IN, getIsInitialLogin } from '../users';

export function* fetchAgenciesWorker(action) {
  const isInitialLogin = yield select(getIsInitialLogin());
  if (action.type === USER_SIGNED_IN && !isInitialLogin) {
    return;
  }
  const spaces = yield select(getSpacesForApi());
  const locale = yield select(getLocale());
  yield put(agenciesFetch());

  try {
    const { data } = yield call(sdmxLibGetAgencies, { locale, spaces });
    yield put(agenciesFetchSuccess(data));
  } catch (error) {
    yield put(agenciesFetchError(error));
  }
}

export default function* saga() {
  yield takeLatest([USER_SIGNED_IN, CHANGE_LOCALE], fetchAgenciesWorker);
}
