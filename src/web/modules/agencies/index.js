import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const agenciesReducer = reducerFactory(model(), handlers);
export { default as agenciesSaga } from './saga';
export { default as withAgencies } from './with-agencies';
export { AGENCIES_FETCH_SUCCESS } from './action-creators';
export { getAgencies, getAgenciesTree, getAgenciesFlatFilterItems } from './selectors';
export { getHierarchicalCodeHelper } from './utils';
