export const AGENCIES_FETCH = 'AGENCIES_FETCH';
export const agenciesFetch = () => ({ type: AGENCIES_FETCH });
export const agenciesFetchHandler = state => ({
  ...state,
  isFetching: true,
});

export const AGENCIES_FETCH_SUCCESS = 'AGENCIES_FETCH_SUCCESS';
export const agenciesFetchSuccess = agencies => ({
  type: AGENCIES_FETCH_SUCCESS,
  payload: { agencies },
});
export const agenciesFetchSuccessHandler = (state, action) => {
  return {
    ...state,
    agencies: action.payload.agencies,
    isFetching: false,
  };
};

export const AGENCIES_FETCH_ERROR = 'AGENCIES_FETCH_ERROR';
export const agenciesFetchError = error => ({ type: AGENCIES_FETCH_ERROR, payload: { error } });
export const agenciesFetchErrorHandler = (state, action) => ({
  ...state,
  error: action.payload.error,
  isFetching: false,
});

export const AGENCY_TOGGLE_NODE = 'AGENCY_TOGGLE_NODE';
export const agencyToggleNode = isExpanded => agency => ({
  type: AGENCY_TOGGLE_NODE,
  payload: { agency, isExpanded },
});
export const agencyToggleNodeHandler = (state, action) => {
  const { agency, isExpanded } = action.payload;
  return {
    ...state,
    expanded: {
      ...state.expanded,
      [agency.id]: isExpanded,
    },
  };
};

export default {
  [AGENCIES_FETCH]: agenciesFetchHandler,
  [AGENCIES_FETCH_SUCCESS]: agenciesFetchSuccessHandler,
  [AGENCIES_FETCH_ERROR]: agenciesFetchErrorHandler,
  [AGENCY_TOGGLE_NODE]: agencyToggleNodeHandler,
};
