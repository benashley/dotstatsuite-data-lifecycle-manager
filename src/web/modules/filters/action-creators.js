import { omit, get } from 'lodash';

export const filterToggle = type => id => ({ type, payload: { id } });
export const filterToggleHandler = filterKey => (state, action) => ({
  ...state,
  isMyArtefacts: filterKey === 'agencyIds' ? false : state.isMyArtefacts,
  [filterKey]: get(state[filterKey], action.payload.id)
    ? omit(state[filterKey], action.payload.id)
    : { ...state[filterKey], [action.payload.id]: { id: action.payload.id } },
});

export const FILTERS_SPACE_TOGGLE = 'FILTERS_SPACE_TOGGLE';
export const spaceToggle = filterToggle(FILTERS_SPACE_TOGGLE);

export const FILTERS_TYPE_TOGGLE = 'FILTERS_TYPE_TOGGLE';
export const typeToggle = filterToggle(FILTERS_TYPE_TOGGLE);

export const FILTERS_AGENCY_TOGGLE = 'FILTERS_AGENCY_TOGGLE';
export const agencyToggle = agency => ({ type: FILTERS_AGENCY_TOGGLE, payload: { id: agency.id } });

export const FILTERS_CATEGORY_TOGGLE = 'FILTERS_CATEGORY_TOGGLE';
export const categoryToggle = category => ({
  type: FILTERS_CATEGORY_TOGGLE,
  payload: { id: category.id },
});

export const FILTERS_CHANGE_VERSION = 'FILTERS_CHANGE_VERSION';
export const changeVersion = version => ({ type: FILTERS_CHANGE_VERSION, payload: { version } });
export const changeVersionHandler = (state, action) => ({
  ...state,
  version: get(state, `versions.${action.payload.version}`),
});

export const FILTERS_TOGGLE_IS_FINAL = 'FILTERS_TOGGLE_IS_FINAL';
export const toggleIsFinal = () => ({ type: FILTERS_TOGGLE_IS_FINAL });
export const toggleIsFinalHandler = state => ({
  ...state,
  isFinal: !get(state, 'isFinal'),
});

export const FILTERS_TOGGLE_IS_MY_ARTEFACTS = 'FILTERS_TOGGLE_IS_MY_ARTEFACTS';
export const toggleIsMyArtefacts = () => ({ type: FILTERS_TOGGLE_IS_MY_ARTEFACTS });
export const toggleIsMyArtefactsHandler = state => ({
  ...state,
  isMyArtefacts: !get(state, 'isMyArtefacts'),
  agencyIds: {},
});

export const filterClear = type => () => ({ type });
export const filterClearHandler = filterKey => state => ({
  ...state,
  isMyArtefacts: filterKey === 'agencyIds' ? false : state.isMyArtefacts,
  [filterKey]: {},
});

export const FILTERS_SPACE_CLEAR = 'FILTERS_SPACE_CLEAR';
export const spaceClear = filterClear(FILTERS_SPACE_CLEAR);

export const FILTERS_TYPE_CLEAR = 'FILTERS_TYPE_CLEAR';
export const typeClear = filterClear(FILTERS_TYPE_CLEAR);

export const FILTERS_AGENCY_CLEAR = 'FILTERS_AGENCY_CLEAR';
export const agencyClear = filterClear(FILTERS_AGENCY_CLEAR);

export const FILTERS_CATEGORY_CLEAR = 'FILTERS_CATEGORY_CLEAR';
export const categoryClear = filterClear(FILTERS_CATEGORY_CLEAR);

export default {
  [FILTERS_SPACE_TOGGLE]: filterToggleHandler('spaceIds'),
  [FILTERS_TYPE_TOGGLE]: filterToggleHandler('typeIds'),
  [FILTERS_AGENCY_TOGGLE]: filterToggleHandler('agencyIds'),
  [FILTERS_CATEGORY_TOGGLE]: filterToggleHandler('categoryIds'),
  [FILTERS_CHANGE_VERSION]: changeVersionHandler,
  [FILTERS_TOGGLE_IS_FINAL]: toggleIsFinalHandler,
  [FILTERS_TOGGLE_IS_MY_ARTEFACTS]: toggleIsMyArtefactsHandler,
  [FILTERS_SPACE_CLEAR]: filterClearHandler('spaceIds'),
  [FILTERS_TYPE_CLEAR]: filterClearHandler('typeIds'),
  [FILTERS_AGENCY_CLEAR]: filterClearHandler('agencyIds'),
  [FILTERS_CATEGORY_CLEAR]: filterClearHandler('categoryIds'),
};
