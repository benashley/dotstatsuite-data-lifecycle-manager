import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const filtersReducer = reducerFactory(model(), handlers);

export { withFilters, withSelection } from './with-filters';
export { getSelection, getIsFinal, getAgencies, getCategories } from './selectors';
export { FILTERS_TOGGLE_IS_FINAL, FILTERS_SPACE_TOGGLE } from './action-creators';
