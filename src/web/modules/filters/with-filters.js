import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  getAgencies,
  getCategories,
  getIsFinal,
  getIsMyArtefacts,
  getSelectedSpaces,
  getSelection,
  getSortedExternalSpaces,
  getSortedInternalSpaces,
  getSortedTypes,
  getVersion,
  getVersions,
} from './selectors';
import {
  agencyClear,
  agencyToggle,
  categoryClear,
  categoryToggle,
  changeVersion,
  spaceClear,
  spaceToggle,
  toggleIsFinal,
  toggleIsMyArtefacts,
  typeClear,
  typeToggle,
} from './action-creators';

export const withFilters = Component => {
  const mapDispatchToProps = {
    agencyClear,
    agencyToggle,
    categoryClear,
    categoryToggle,
    changeVersion,
    spaceClear,
    spaceToggle,
    toggleIsFinal,
    toggleIsMyArtefacts,
    typeClear,
    typeToggle,
  };

  const mapStateToProps = createStructuredSelector({
    agencies: getAgencies(),
    categories: getCategories(),
    externalSpaces: getSortedExternalSpaces(),
    internalSpaces: getSortedInternalSpaces(),
    isFinal: getIsFinal(),
    isMyArtefacts: getIsMyArtefacts(),
    selectedSpaces: getSelectedSpaces(),
    types: getSortedTypes(),
    version: getVersion(),
    versions: getVersions(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};

export const withSelection = Component =>
  connect(
    createStructuredSelector({
      selectedFiltersValues: getSelection(),
    }),
  )(Component);
