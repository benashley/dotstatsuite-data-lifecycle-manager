import { createSelector } from 'reselect';
import { get, reduce, keys, pickBy, sortBy, keyBy } from 'lodash';
import * as R from 'ramda';
import {
  getExternalSpaces as getExternalSpacesFromConfig,
  getInternalSpaces as getInternalSpacesFromConfig,
  getSpaces as getSpacesFromConfig,
  getTypes as getTypesFromConfig,
} from '../config';
import { getAgenciesFlatFilterItems as getAgenciesFromAgencies } from '../agencies';
import { getCategories as getCategoriesFromCategories } from '../categories';
import { getMyAgencies } from '../users';
import { withSort } from '../common/utils';

const getState = state => state.filters;

export const getSelectionIds = key =>
  createSelector(
    getState,
    state => get(state, key),
  );

export const getVersion = () =>
  createSelector(
    getState,
    state => get(state, 'version'),
  );
export const getVersions = () =>
  createSelector(
    getState,
    state => get(state, 'versions'),
  );
export const getIsFinal = () =>
  createSelector(
    getState,
    state => get(state, 'isFinal'),
  );
export const getIsMyArtefacts = () =>
  createSelector(
    getState,
    state => get(state, 'isMyArtefacts'),
  );

const withSelection = (filters, ids) =>
  reduce(
    ids,
    (memo, { id }) => {
      const filter = get(filters, id);
      return filter ? { ...memo, [id]: { ...filter, isSelected: true } } : { ...memo };
    },
    { ...filters },
  );

const isSelected = filters => pickBy(filters, filter => filter.isSelected);

export const getInternalSpaces = () =>
  createSelector(
    [getInternalSpacesFromConfig(), getSelectionIds('spaceIds')],
    withSelection,
  );

export const getExternalSpaces = () =>
  createSelector(
    [getExternalSpacesFromConfig(), getSelectionIds('spaceIds')],
    withSelection,
  );

export const getCategories = () =>
  createSelector(
    [getCategoriesFromCategories(), getSelectionIds('categoryIds')],
    withSelection,
  );

export const getSpaces = () =>
  createSelector(
    [getSpacesFromConfig(), getSelectionIds('spaceIds')],
    withSelection,
  );

export const getSortedInternalSpaces = (fields = []) =>
  createSelector(
    [getInternalSpaces()],
    withSort(fields),
  );

export const getSortedExternalSpaces = (fields = []) =>
  createSelector(
    [getExternalSpaces()],
    withSort(fields),
  );

export const getSortedSpaces = (fields = []) =>
  createSelector(
    [getSpaces()],
    withSort(fields),
  );

export const getTypes = () =>
  createSelector(
    [getTypesFromConfig(), getSelectionIds('typeIds')],
    withSelection,
  );

export const getSortedTypes = () =>
  createSelector(
    [getTypes()],
    types => sortBy(types, ['label']),
  );

export const getAgencies = () =>
  createSelector(
    [getAgenciesFromAgencies(), getSelectionIds('agencyIds')],
    withSelection,
  );

export const getSelectedSpaces = () =>
  createSelector(
    [getSpaces()],
    isSelected,
  );
export const getSelectedTypes = () =>
  createSelector(
    [getTypes()],
    isSelected,
  );
export const getSelectedAgencies = () =>
  createSelector(
    [getAgencies()],
    isSelected,
  );

export const getRelevantAgencies = () =>
  createSelector(
    [getIsMyArtefacts(), getSelectedAgencies(), getMyAgencies()],
    (isMyArtefacts, selectedAgencies, myAgencies) =>
      isMyArtefacts ? myAgencies : selectedAgencies,
  );

export const getSelectionTypes = () =>
  createSelector(
    [getSelectedTypes()],
    selectedTypes => keys(selectedTypes),
  );

export const getSelectionSpaces = () =>
  createSelector(
    [getSelectedSpaces()],
    selectedSpaces => keyBy(selectedSpaces, 'endpoint'),
  );

export const getSelectedCategories = () =>
  createSelector(
    getCategories(),
    categories => {
      const selected = isSelected(categories);

      return R.mapObjIndexed(category => {
        const topParentId = category.topParentId;

        if (R.has(topParentId, categories)) {
          const scheme = categories[topParentId];
          return { scheme, ...R.pick(['code', 'hierarchicalCode', 'spaceId'], category) };
        }

        return { scheme: category, spaceId: category.spaceId };
      }, selected);
    },
  );

export const getSelection = () =>
  createSelector(
    [
      getSelectionTypes(),
      getSelectionSpaces(),
      getRelevantAgencies(),
      getVersion(),
      getSelectedCategories(),
    ],
    (types, spaces, agencies, version, categories) => {
      return {
        types,
        spaces,
        agencies,
        versions: [version],
        categories,
      };
    },
  );
