import { createSelector } from 'reselect';
import { compact, difference, find, head, isEmpty, keyBy, map } from 'lodash';
import { getLocale } from '../i18n';
import { MIME_TYPE_XML } from '../upload';

export default selectors => {
  const getAcceptedFiles = () =>
    createSelector(
      [selectors.getFiles(), selectors.getMaxSize()],
      (files, maxSize) =>
        compact(
          map([MIME_TYPE_XML], mimeType =>
            find(files, file => mimeType.includes(file.type) && file.size <= maxSize),
          ),
        ),
    );

  const getRejectedFiles = () =>
    createSelector(
      [selectors.getFiles(), getAcceptedFiles()],
      (files, acceptedFiles) => difference(files, acceptedFiles),
    );

  return {
    ...selectors,
    getAcceptedFiles,
    getRejectedFiles,
    getUploadOptions: () =>
      createSelector(
        [getAcceptedFiles(), getLocale()],
        (acceptedFiles, locale) => ({
          file: head(acceptedFiles),
          locale,
        }),
      ),
    getKeyedAcceptedFiles: () =>
      createSelector(
        [getAcceptedFiles()],
        files => keyBy(files, file => file.name),
      ),
    getKeyedRejectedFiles: () =>
      createSelector(
        [getRejectedFiles()],
        files => keyBy(files, file => file.name),
      ),
    getFormIsValid: () =>
      createSelector(
        [getAcceptedFiles(), selectors.getSpaces()],
        (files, spaces) => !!(!isEmpty(spaces) && !isEmpty(files)),
      ),
  };
};
