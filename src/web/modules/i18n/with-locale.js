import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getAvailableLocales, getLocale } from './selectors';
import { changeLocale } from './action-creators';

export default Component => {
  const mapDispatchToProps = {
    changeLocale,
  };

  const mapStateToProps = createStructuredSelector({
    availableLocales: getAvailableLocales(),
    locale: getLocale(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
