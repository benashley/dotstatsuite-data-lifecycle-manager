import React from 'react';
import PropTypes from 'prop-types';
import { assoc } from 'ramda';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { IntlProvider } from 'react-intl';
import { getLocale } from './selectors';
import { none, NONE } from './utils';

const Provider = ({ localeId, messages, children }) => (
  <IntlProvider locale={localeId} key={localeId} messages={assoc(NONE, none, messages[localeId])}>
    {React.Children.only(children)}
  </IntlProvider>
);

Provider.propTypes = {
  localeId: PropTypes.string,
  messages: PropTypes.object,
  children: PropTypes.element.isRequired,
};

export default connect(createStructuredSelector({ localeId: getLocale() }))(Provider);
