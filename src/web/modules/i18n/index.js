import * as R from 'ramda';
import numeral from 'numeral';
import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

/*
// https://fr.wiktionary.org/wiki/Wiktionnaire:BCP_47/language-2

// For IE11: yarn add @formatjs/intl-pluralrules
if (!Intl.PluralRules) {
  import '@formatjs/intl-pluralrules/polyfill';
  import '@formatjs/intl-pluralrules/dist/locale-data/ar';
  import '@formatjs/intl-pluralrules/dist/locale-data/en';
  import '@formatjs/intl-pluralrules/dist/locale-data/fr';
  import '@formatjs/intl-pluralrules/dist/locale-data/es';
  import '@formatjs/intl-pluralrules/dist/locale-data/it';
  import '@formatjs/intl-pluralrules/dist/locale-data/km';
  import '@formatjs/intl-pluralrules/dist/locale-data/nl';
}

// For IE11: yarn add @formatjs/intl-relativetimeformat
if (!Intl.RelativeTimeFormat) {
  import '@formatjs/intl-relativetimeformat/polyfill';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/ar';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/en';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/fr';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/es';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/it';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/km';
  import '@formatjs/intl-relativetimeformat/dist/locale-data/nl';
}
*/

export const i18nReducer = reducerFactory(model(), handlers);

export { default as I18nProvider } from './provider';
export { default as withLocale } from './with-locale';
export { getLocale } from './selectors';
export { CHANGE_LOCALE } from './action-creators';

const modelLocale = locale => `${locale}/${locale}`;

export const setLocale = locale => numeral.locale(modelLocale(locale));

export default ({ locales = [], localeId = 'en' }) => {
  setLocale(localeId);

  R.forEach(locale => {
    const delimiters = R.prop('delimiters')(locale);
    if (R.isNil(delimiters)) return;
    numeral.register('locale', modelLocale(R.prop('id')(locale)), { delimiters });
  }, R.values(locales));
};
