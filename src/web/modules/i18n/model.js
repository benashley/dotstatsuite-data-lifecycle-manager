// export function: model can be configurable and won't be modified if recalled
export default () => ({
  // current locale
  locale: 'en',

  // timezone, useful for dates
  timezone: null,

  // currency, useful for formats
  currency: null,

  // font direction, useful for languages such as arabic
  direction: 'rtl',
});
