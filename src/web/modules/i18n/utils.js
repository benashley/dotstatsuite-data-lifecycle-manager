import { assoc, indexBy, prop, ifElse, has, __, replace, pipe } from 'ramda';
import { defineMessages } from 'react-intl';

export const NONE = 'i18n.translation.none';
export const none = {
  id: NONE,
  defaultMessage: 'unknown id: {id}',
  description: 'fallback when a translation id is not defined',
};

export const getTranslations = translations => {
  const stripPoints = replace('.', '');
  const stripedNone = stripPoints(NONE);

  const translationsWithFallback = defineMessages(
    assoc(
      stripedNone,
      none,
      indexBy(
        pipe(
          prop('id'),
          stripPoints,
        ),
        translations,
      ),
    ),
  );

  const translationsProp = pipe(
    stripPoints,
    prop(__, translationsWithFallback),
  );

  return ifElse(
    pipe(
      stripPoints,
      has(__, translationsWithFallback),
    ),
    id => [translationsProp(id)],
    id => [translationsProp(NONE), { id }],
  );
};

// usage:
//
// import { getTranslations } from '...';
// const myTranslations = [{ id: 'myId1'}];
// const getTranslationsWithFallback = getTranslations(myTranslations);
// ... intl.formatMessage(...getTranslations('myId1')) ...
// note: splat operator is useful to have the unknown id in the UI
