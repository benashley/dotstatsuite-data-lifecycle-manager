import * as R from 'ramda';
import { createSelector } from 'reselect';

const getState = state => state.i18n;

export const getLocale = () =>
  createSelector(
    getState,
    state => state.locale,
  );
export const getAvailableLocales = () =>
  createSelector(
    getState,
    R.always(
      R.pipe(
        R.pathOr({}, ['i18n', 'locales']),
        R.values,
      )(window.SETTINGS),
    ),
  );
