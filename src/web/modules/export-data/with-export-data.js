import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { exportData } from './action-creators';
import { getIsExportingData, getError } from './selectors';

export default id => Component => {
  const mapDispatchToProps = {
    exportData,
  };

  const mapStateToProps = createStructuredSelector({
    isExportingData: getIsExportingData(id),
    error: getError(id),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
