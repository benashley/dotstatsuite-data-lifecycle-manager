import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const exportDataReducer = reducerFactory(model(), handlers);
export { default as exportDataSaga } from './saga';
export { default as withExportData } from './with-export-data';
export { EXPORT_DATA, EXPORT_DATA_SUCCESS } from './action-creators';
