import { createSelector } from 'reselect';
import { get } from 'lodash';

const getState = state => state.exportData;

export const getExportData = id =>
  createSelector(
    [getState],
    state => get(state, id, {}),
  );

export const getIsExportingData = id =>
  createSelector(
    [getExportData(id)],
    exportState => get(exportState, 'isExporting', false),
  );

export const getError = id =>
  createSelector(
    [getExportData(id)],
    exportState => get(exportState, 'error', null),
  );
