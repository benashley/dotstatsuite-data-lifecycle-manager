import { get } from 'lodash';
import { MODULE } from './constants';

export const EXPORT_DATA = `${MODULE}_EXPORT_DATA`;
export const exportData = (id, format) => ({
  type: EXPORT_DATA,
  payload: { id, format },
});
export const exportDataHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...get(state, action.payload.id, {}),
    id: action.payload.id,
    isExporting: true,
    error: null,
  },
});

export const EXPORT_DATA_SUCCESS = `${MODULE}_EXPORT_DATA_SUCCESS`;
export const exportDataSuccess = id => ({
  type: EXPORT_DATA_SUCCESS,
  payload: { id },
});
export const exportDataSuccessHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...get(state, action.payload.id, {}),
    isExporting: false,
    error: null,
  },
});

export const EXPORT_DATA_ERROR = `${MODULE}_EXPORT_DATA_ERROR`;
export const exportDataError = (id, error) => ({
  type: EXPORT_DATA_ERROR,
  payload: { id, error },
});

export const exportDataErrorHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...get(state, action.payload.id, {}),
    isExporting: false,
    error: { type: 'export', ...action.payload.error },
  },
});

export default {
  [EXPORT_DATA]: exportDataHandler,
  [EXPORT_DATA_ERROR]: exportDataErrorHandler,
  [EXPORT_DATA_SUCCESS]: exportDataSuccessHandler,
};
