import { takeEvery, call, put, select } from 'redux-saga/effects';
import { dataflowExportData } from '../../api/export-data';
import { getArtefactOptions } from '../common/selectors';
import { EXPORT_DATA, exportDataError, exportDataSuccess } from './action-creators';

export function* exportDataWorker(action) {
  const id = action.payload.id;
  const format = action.payload.format;

  try {
    const dataflowOptions = yield select(getArtefactOptions(id));
    yield call(dataflowExportData, dataflowOptions, format);
    yield put(exportDataSuccess(id));
  } catch (error) {
    yield put(exportDataError(id, error));
  }
}

export default function* saga() {
  yield takeEvery(EXPORT_DATA, exportDataWorker);
}
