import { createSelector } from 'reselect';
import { get } from 'lodash';
import { getArtefact, getArtefactOptions } from '../common/selectors';

export default selectors => {
  return {
    ...selectors,
    getArtefactOptions,
    getIsUpdating: id =>
      createSelector(
        [getArtefact(id)],
        artefact => get(artefact, 'isUpdating'),
      ),
  };
};
