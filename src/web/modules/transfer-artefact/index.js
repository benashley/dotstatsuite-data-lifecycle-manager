import { get } from 'lodash';
import reducerFactory from '../../store/reducer-factory';
import {
  model as modelFactory,
  actions as actionsFactory,
  selectors as transferSelectorsFactory,
  withTransfer,
} from '../transfer';
import selectorsFactory from './selectors';
import sagaFactory from './saga';

const MODULE = 'TRANSFER_ARTEFACT';
const NAMESPACE = 'transferArtefact';
const model = modelFactory();
const actions = actionsFactory(MODULE, modelFactory);
const getState = state => get(state, NAMESPACE);
const selectors = selectorsFactory(transferSelectorsFactory(getState));

export const transferArtefactReducer = reducerFactory(model, actions.handlers);
export const transferArtefactSaga = sagaFactory(actions, selectors);
export const withTransferArtefact = withTransfer(actions, selectors);
export const getTransfer = selectors.getTransfer;
export const TRANSFER_ARTEFACT = actions.TRANSFER;
export const TRANSFER_ARTEFACT_SUCCESS = actions.TRANSFER_SUCCESS;
