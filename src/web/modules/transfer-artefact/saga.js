import { takeEvery, call, put, select } from 'redux-saga/effects';
import { transferStructure } from '../../sdmx-lib';
import { ERROR, SUCCESS, WARNING } from '../transfer';
import { getLocale } from '../i18n';

export default (actions, selectors) => {
  function* transferWorker(action) {
    const id = action.payload.id;
    const withReferences = action.payload.withReferences;

    try {
      const artefactOptions = yield select(selectors.getArtefactOptions(id));
      const locale = yield select(getLocale());

      const transferOptions = yield select(selectors.getTransferOptions(id));
      const log = yield call(
        transferStructure,
        { ...artefactOptions, locale, withReferences },
        transferOptions.space,
      );
      if (log.isError) {
        yield put(actions.transferError(id, { type: ERROR, message: log.data }));
      } else if (log.isWarning) {
        yield put(actions.transferWarning(id, { type: WARNING, message: log.data }));
      } else {
        yield put(actions.transferSuccess(id, { type: SUCCESS }));
      }
    } catch (error) {
      yield put(actions.transferError(id, { type: ERROR, message: error.message }));
    }
  }

  return function* saga() {
    yield takeEvery(actions.TRANSFER, transferWorker);
  };
};
