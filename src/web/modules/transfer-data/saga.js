import * as R from 'ramda';
import { takeEvery, call, put, select } from 'redux-saga/effects';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { upload as uploadData } from '../../api/upload-data';
import { transfer as transferData } from '../../api/transfer-data';
import { ERROR, WARNING } from '../transfer';
import { SDMX_CSV } from '../upload';

export default (actions, selectors) => {
  function* externalTransferWorker({ dataflow, dataquery, space, endpoint }) {
    const filepath = R.pipe(
      getRequestArgs,
      R.prop('url'),
    )({
      datasource: { url: R.path(['space', 'endpoint'], dataflow) },
      identifiers: dataflow,
      dataquery,
      type: 'data',
    });
    return yield call(uploadData, {
      endpoint,
      space,
      dataflow,
      byPath: true,
      mode: SDMX_CSV,
      filepath,
    });
  }

  function* transferWorker(action) {
    const id = action.payload.id;
    const dataquery = action.payload.dataQuery;

    try {
      const { isExternal, ...options } = yield select(selectors.getTransferOptions(id));
      const log = isExternal
        ? yield call(externalTransferWorker, { dataquery, ...options })
        : yield call(transferData, { dataQuery: dataquery, ...options });

      if (log.type === ERROR) yield put(actions.transferError(id, log));
      else if (log.type === WARNING) yield put(actions.transferWarning(id, log));
      else yield put(actions.transferSuccess(id, log));
    } catch (error) {
      yield put(actions.transferError(id, { type: ERROR, error }));
    }
  }

  return function* saga() {
    yield takeEvery(actions.TRANSFER, transferWorker);
  };
};
