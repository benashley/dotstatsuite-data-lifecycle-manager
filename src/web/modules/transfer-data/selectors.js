import { createSelector } from 'reselect';
import * as R from 'ramda';
import { getArtefact } from '../common/selectors';

export default selectors => {
  return {
    ...selectors,
    getTransferOptions: id =>
      createSelector(
        [getArtefact(id), selectors.getTransfer(id)],
        (dataflow, transfer) => {
          const isExternal = R.pathOr(false, ['space', 'isExternal'], dataflow);
          if (isExternal) {
            return {
              isExternal,
              endpoint: window.CONFIG.transferServerUrl,
              space: R.prop('space', transfer),
              dataflow,
            };
          }
          return {
            isExternal,
            endpoint: window.CONFIG.transferServerUrl,
            sourceSpace: R.prop('space', dataflow),
            sourceDataflow: dataflow,
            destinationSpace: R.prop('space', transfer),
            destinationDataflow: dataflow,
          };
        },
      ),
  };
};
