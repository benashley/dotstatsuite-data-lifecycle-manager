import { createSelector } from 'reselect';
import {
  compact,
  difference,
  every,
  find,
  get,
  groupBy,
  gte,
  has,
  head,
  isEmpty,
  isNil,
  keys,
  keyBy,
  map,
  reduce,
  size,
} from 'lodash';
import {
  MIME_TYPE_XML,
  MIME_TYPE_XLSX,
  MIME_TYPE_CSV,
  MIME_TYPE_ZIP,
  EXCEL_EDD,
  SDMX_CSV,
} from '../upload';
import { getInternalSpaces } from '../config';
import { getArtefact } from '../common/selectors';

export default selectors => {
  const getMode = () =>
    createSelector(
      [selectors.getState],
      state => state.mode,
    );

  const getAcceptFormats = () =>
    createSelector(
      [getMode()],
      mode => {
        if (mode === EXCEL_EDD) {
          return [MIME_TYPE_XLSX, MIME_TYPE_XML];
        } else if (mode === SDMX_CSV) {
          return [MIME_TYPE_XML, MIME_TYPE_CSV, MIME_TYPE_ZIP];
        }
        return [];
      },
    );

  const getAllowMultiple = () =>
    createSelector(
      [getMode()],
      mode => {
        if (mode === EXCEL_EDD) {
          return true;
        } else if (mode === SDMX_CSV) {
          return false;
        }
        return false;
      },
    );

  const getAcceptedFiles = () =>
    createSelector(
      [selectors.getFiles(), getAcceptFormats(), selectors.getMaxSize(), getMode()],
      (files, formats, maxSize, mode) => {
        if (mode === EXCEL_EDD) {
          return compact(
            map(formats, mimeType =>
              find(files, file => mimeType.includes(file.type) && file.size <= maxSize),
            ),
          );
        } else {
          const joinedFormats = formats.join(', ');
          const result = find(
            files,
            file => joinedFormats.includes(file.type) && file.size <= maxSize,
          );
          return result ? [result] : [];
        }
      },
    );

  const getRejectedFiles = () =>
    createSelector(
      [selectors.getFiles(), getAcceptedFiles()],
      (files, acceptedFiles) => difference(files, acceptedFiles),
    );

  const canFetchDataflows = () =>
    createSelector(
      [getMode(), selectors.getSpaces()],
      (mode, spaces) => !isEmpty(spaces) && mode === SDMX_CSV,
    );

  const getDataflows = () =>
    createSelector(
      [selectors.getState],
      state => get(state, 'dataflows', {}),
    );

  const getDataflow = () =>
    createSelector(
      [selectors.getState],
      state => get(state, 'dataflow', null),
    );

  const getFormIsValid = () =>
    createSelector(
      [
        getMode(),
        selectors.getSpaces(),
        getAcceptedFiles(),
        getDataflow(),
        getByPath(),
        getFilePath(),
      ],
      (mode, spaces, files, dataflow, byPath, filepath) => {
        if (mode === EXCEL_EDD) {
          return !!(!isEmpty(spaces) && gte(size(files), 2));
        } else if (mode === SDMX_CSV) {
          const hasSource = byPath ? !isNil(filepath) && !isEmpty(filepath) : !isEmpty(files);
          return !!(!isEmpty(spaces) && dataflow && hasSource);
        }
        return false;
      },
    );

  const getDataflowsError = () =>
    createSelector(
      [selectors.getState],
      state => get(state, 'dataflowsError', {}),
    );

  const getByPath = () =>
    createSelector(
      selectors.getState,
      state => state.byPath,
    );
  const getFilePath = () =>
    createSelector(
      selectors.getState,
      state => state.filepath,
    );

  return {
    ...selectors,
    getModes: () =>
      createSelector(
        [selectors.getState],
        state => state.modes,
      ),
    getMode,
    getAcceptFormats,
    getDropzoneConfig: () =>
      createSelector(
        [selectors.getDropzoneConfig(), getAcceptFormats(), getAllowMultiple()],
        (config, formats, multiple) => ({
          ...config,
          multiple,
          accept: formats.join(', '),
        }),
      ),
    getDataflow,
    getDataflows,
    getAcceptedFiles,
    getRejectedFiles,
    getFormIsValid,
    getHasPathOption: () =>
      createSelector(
        getMode(),
        mode => mode === SDMX_CSV,
      ),
    getByPath,
    getFilePath,
    getUploadOptions: () =>
      createSelector(
        [getAcceptedFiles(), getDataflow(), getMode(), getByPath(), getFilePath()],
        (acceptedFiles, dataflow, mode, byPath, filepath) => ({
          dataflow,
          endpoint: window.CONFIG.transferServerUrl,
          files: acceptedFiles,
          mode,
          byPath,
          filepath,
        }),
      ),
    getHasDataflowSelection: () =>
      createSelector(
        [getMode(), selectors.getSpaces()],
        (mode, spaces) => !!(mode === SDMX_CSV && !isEmpty(spaces)),
      ),
    getIsFetchingDataflows: () =>
      createSelector(
        [selectors.getState],
        state => get(state, 'isFetchingDataflows', false),
      ),
    getDataflowsError,
    getRefinedDataflows: () =>
      createSelector(
        [getDataflows(), getDataflow(), selectors.getSpaces()],
        (dataflows, selected, spaces) => {
          const flatDataflows = reduce(dataflows, (memo, values) => ({ ...memo, ...values }), {});
          const nbSpaces = size(spaces);
          const groupedDataflows = groupBy(flatDataflows, ({ sdmxId }) => sdmxId);
          const res = reduce(
            groupedDataflows,
            (memo, dataflows, key) => {
              const nb = size(dataflows);
              if (nb !== nbSpaces) return memo;
              const { agencyId, code, isFinal, label, sdmxId, type, version } = head(dataflows);
              return [
                ...memo,
                {
                  agencyId,
                  code,
                  id: key,
                  isFinal,
                  isSelected: !!(selected && sdmxId === get(selected, 'sdmxId')),
                  label,
                  sdmxId,
                  type,
                  version,
                },
              ];
            },
            [],
          );
          return res;
        },
      ),
    hasFetchDataflowsFromAllSpaces: () =>
      createSelector(
        [selectors.getSpaces(), getDataflows(), getDataflowsError()],
        (spaces, dataflows, dataflowsError) => {
          const nbSpaces = size(spaces);
          const nbSpacesInDataflows = size(keys(dataflows));
          const nbSpacesInDataflowsError = size(keys(dataflowsError));
          return nbSpaces === nbSpacesInDataflows + nbSpacesInDataflowsError;
        },
      ),
    getHelpKey: () =>
      createSelector(
        [getMode()],
        mode => {
          if (mode === EXCEL_EDD) {
            return 'upload.data.help.excel';
          }
          if (mode === SDMX_CSV) {
            return 'upload.data.help.sdmx';
          }
          return null;
        },
      ),
    getCanUpload: id =>
      createSelector(
        [getArtefact(id), getInternalSpaces()],
        (artefact, spaces) => {
          return has(spaces, get(artefact, 'space.id'));
        },
      ),
    isSelectedDataflowStillRelevant: () =>
      createSelector(
        [getDataflows(), getDataflow()],
        (dataflows, dataflow) => {
          if (!dataflow) return true;
          return every(
            dataflows,
            spaceDataflows =>
              find(spaceDataflows, df => df.sdmxId === dataflow.sdmxId) !== undefined,
          );
        },
      ),
    canFetchDataflows,
    getKeyedAcceptedFiles: () =>
      createSelector(
        [getAcceptedFiles()],
        files => keyBy(files, file => file.name),
      ),
    getKeyedRejectedFiles: () =>
      createSelector(
        [getRejectedFiles()],
        files => keyBy(files, file => file.name),
      ),
  };
};
