import { SDMX_CSV } from '../upload';
import { get, isEmpty, omit } from 'lodash';
const MODULE = 'UPLOAD_DATA';
const REMOVE_SPACE = `${MODULE}_REMOVE_SPACE`;

export const CHANGE_MODE = `${MODULE}_CHANGE_MODE`;
export const changeMode = mode => ({
  type: CHANGE_MODE,
  payload: { mode },
});
export const changeModeHandler = (state, action) => ({
  ...state,
  flushable: true,
  mode: action.payload.mode,
});

export const UPLOAD_FROM_DATAFLOW = `${MODULE}_UPLOAD_FROM_DATAFLOW`;
export const uploadFromDataflow = dataflow => ({
  type: UPLOAD_FROM_DATAFLOW,
  payload: { dataflow, space: dataflow.space },
});
export const uploadFromDataflowHandler = (state, action) => {
  const space = action.payload.space;
  return {
    ...state,
    dataflow: action.payload.dataflow,
    flushable: false,
    mode: SDMX_CSV,
    spaces: { [space.id]: space },
  };
};

export const TOGGLE_DATAFLOW = `${MODULE}_TOGGLE_DATAFLOW`;
export const toggleDataflow = dataflow => ({
  type: TOGGLE_DATAFLOW,
  payload: { dataflow },
});
export const toggleDataflowHandler = (state, action) => {
  const payloadId = get(action, 'payload.dataflow.id', null);
  const stateId = get(state, 'dataflow.id', null);
  return {
    ...state,
    dataflow: !!stateId && stateId === payloadId ? null : action.payload.dataflow,
    flushable: true,
  };
};

export const FETCH_DATAFLOWS_FROM_ONE_SPACE = `${MODULE}_FETCH_DATAFLOWS_FROM_ONE_SPACE`;
export const fetchDataflowsFromOneSpace = () => ({
  type: FETCH_DATAFLOWS_FROM_ONE_SPACE,
});
export const fetchDataflowsFromOneSpaceHandler = state => ({
  ...state,
  isFetchingDataflows: true,
  flushable: false,
});

export const FETCH_DATAFLOWS_FROM_ALL_SPACES = `${MODULE}_FETCH_DATAFLOWS_FROM_ALL_SPACES`;
export const fetchDataflowsFromAllSpaces = () => ({
  type: FETCH_DATAFLOWS_FROM_ALL_SPACES,
});
export const fetchDataflowsFromAllSpacesHandler = state => ({
  ...state,
  dataflowsError: {},
  dataflows: {},
});

export const FETCH_DATAFLOWS_ERROR = `${MODULE}_FETCH_DATAFLOWS_ERROR`;
export const fetchDataflowsError = (error, space) => ({
  type: FETCH_DATAFLOWS_ERROR,
  payload: { error, space },
});
export const fetchDataflowsErrorHandler = (state, action) => ({
  ...state,
  dataflowsError: {
    ...state.dataflowsError,
    [action.payload.space.id]: {
      error: action.payload.error,
      space: action.payload.space,
    },
  },
  spaces: omit(state.spaces, action.payload.space.id),
});

export const FETCH_DATAFLOWS_SUCCESS = `${MODULE}_FETCH_DATAFLOWS_SUCCESS`;
export const fetchDataflowsSuccess = (dataflows, space) => ({
  type: FETCH_DATAFLOWS_SUCCESS,
  payload: { dataflows, space },
});
export const fetchDataflowsSuccessHandler = (state, action) => ({
  ...state,
  dataflows: {
    ...state.dataflows,
    [action.payload.space.id]: action.payload.dataflows,
  },
});

export const HAS_FETCH_DATAFLOWS_FROM_ALL_SPACES = `${MODULE}_HAS_FETCH_DATAFLOWS_FROM_ALL_SPACES`;
export const hasFetchDataflowsFromAllSpaces = () => ({
  type: HAS_FETCH_DATAFLOWS_FROM_ALL_SPACES,
});
export const hasFetchDataflowsFromAllSpacesHandler = state => ({
  ...state,
  isFetchingDataflows: false,
  flushable: isEmpty(state.dataflowsError),
});

export const RESET_DATAFLOW = `${MODULE}_RESET_DATAFLOW`;
export const resetDataflow = () => ({
  type: RESET_DATAFLOW,
});
export const resetDataflowHandler = state => ({
  ...state,
  dataflow: null,
  flushable: true,
});

export const removeSpaceHandler = (state, action) => ({
  ...state,
  dataflows: omit(state.dataflows, action.payload.space.id),
  spaces: omit(state.spaces, action.payload.space.id),
  flushable: true,
});

export const REMOVE_DATAFLOWS_ERROR_LOG = `${MODULE}_REMOVE_DATAFLOWS_ERROR_LOG`;
export const removeDataflowsErrorLog = id => ({
  type: REMOVE_DATAFLOWS_ERROR_LOG,
  payload: { id },
});
export const removeDataflowsErrorLogHandler = (state, action) => ({
  ...state,
  dataflowsError: omit(state.dataflowsError, action.payload.id),
});

export const TOGGLE_BY_PATH = `${MODULE}_TOGGLE_BY_PATH`;
export const toggleByPath = () => ({
  type: TOGGLE_BY_PATH,
});
export const toggleByPathHandler = state => ({
  ...state,
  byPath: !state.byPath,
  filepath: undefined,
  flushable: true,
});

export const CHANGE_FILEPATH = `${MODULE}_CHANGE_FILEPATH`;
export const changeFilePath = filepath => ({
  type: CHANGE_FILEPATH,
  payload: { filepath },
});
export const changeFilePathHandler = (state, action) => ({
  ...state,
  filepath: action.payload.filepath,
  flushable: true,
});

export default {
  [CHANGE_MODE]: changeModeHandler,
  [TOGGLE_DATAFLOW]: toggleDataflowHandler,
  [UPLOAD_FROM_DATAFLOW]: uploadFromDataflowHandler,
  [FETCH_DATAFLOWS_FROM_ONE_SPACE]: fetchDataflowsFromOneSpaceHandler,
  [FETCH_DATAFLOWS_FROM_ALL_SPACES]: fetchDataflowsFromAllSpacesHandler,
  [FETCH_DATAFLOWS_ERROR]: fetchDataflowsErrorHandler,
  [FETCH_DATAFLOWS_SUCCESS]: fetchDataflowsSuccessHandler,
  [HAS_FETCH_DATAFLOWS_FROM_ALL_SPACES]: hasFetchDataflowsFromAllSpacesHandler,
  [RESET_DATAFLOW]: resetDataflowHandler,
  [REMOVE_SPACE]: removeSpaceHandler,
  [REMOVE_DATAFLOWS_ERROR_LOG]: removeDataflowsErrorLogHandler,
  [TOGGLE_BY_PATH]: toggleByPathHandler,
  [CHANGE_FILEPATH]: changeFilePathHandler,
};
