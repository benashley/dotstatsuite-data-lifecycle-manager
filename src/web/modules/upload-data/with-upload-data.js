import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  changeFilePath,
  changeMode,
  removeDataflowsErrorLog,
  toggleByPath,
  toggleDataflow,
} from './action-creators';

export const withUploadModeFactory = selectors => Component => {
  const mapDispatchToProps = {
    changeMode,
  };

  const mapStateToProps = createStructuredSelector({
    helpKey: selectors.getHelpKey(),
    selected: selectors.getMode(),
    modes: selectors.getModes(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};

export const withDataflowsFactory = selectors => Component => {
  const mapDispatchToProps = {
    removeDataflowsErrorLog,
    toggleDataflow,
  };

  const mapStateToProps = createStructuredSelector({
    dataflows: selectors.getRefinedDataflows(),
    isFetching: selectors.getIsFetchingDataflows(),
    dataflowsError: selectors.getDataflowsError(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};

export const withUploadPathFactory = selectors => Component => {
  const mapDispatchToProps = {
    changeFilePath,
    toggleByPath,
  };

  const mapStateToProps = createStructuredSelector({
    byPath: selectors.getByPath(),
    filepath: selectors.getFilePath(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
