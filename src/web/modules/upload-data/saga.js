import { all, takeEvery, takeLatest, call, put, select } from 'redux-saga/effects';
import {
  CHANGE_MODE,
  fetchDataflowsFromOneSpace,
  fetchDataflowsFromAllSpaces,
  fetchDataflowsError,
  FETCH_DATAFLOWS_ERROR,
  fetchDataflowsSuccess,
  FETCH_DATAFLOWS_SUCCESS,
  hasFetchDataflowsFromAllSpaces,
  resetDataflow,
  UPLOAD_FROM_DATAFLOW,
} from './action-creators';
import { getFetchDataflowsOptions } from './utils';
import { getParsedDataflowStructures } from '../../sdmx-lib';
import { getLocale, CHANGE_LOCALE } from '../i18n';
//import history from '../../core/history'; // AAAH
import { map } from 'lodash';

export default (actions, selectors) => {
  function* uploadFromDataflowWorker() {
    //yield history.push('upload-data'); // AAAH
  }

  function* fetchDataflowsFromOneSpaceWorker(action) {
    const isValid = yield select(selectors.canFetchDataflows());
    if (!isValid) {
      return;
    }
    try {
      const locale = yield select(getLocale());
      const space = action.payload.space;
      const options = getFetchDataflowsOptions(space);
      yield put(fetchDataflowsFromOneSpace());
      const { data } = yield call(getParsedDataflowStructures, { ...options, locale });
      yield put(fetchDataflowsSuccess(data, space));
    } catch (error) {
      yield put(fetchDataflowsError(error, action.payload.space));
    }
  }

  function* fetchDataflowsFromAllSpacesWorker() {
    const isValid = yield select(selectors.canFetchDataflows());
    if (!isValid) {
      return;
    }
    const spaces = yield select(selectors.getSpaces());
    yield put(fetchDataflowsFromAllSpaces());
    yield all(map(spaces, space => call(fetchDataflowsFromOneSpaceWorker, { payload: { space } })));
  }

  function* hasFetchDataflowsFromAllSpacesWorker() {
    const isValid = yield select(selectors.hasFetchDataflowsFromAllSpaces());
    if (isValid) {
      yield put(hasFetchDataflowsFromAllSpaces());
    }
  }

  function* isSelectedDataflowStillRelevantWorker() {
    const isValid = yield select(selectors.isSelectedDataflowStillRelevant());
    if (!isValid) {
      yield put(resetDataflow());
    }
  }

  return function* saga() {
    yield takeLatest(UPLOAD_FROM_DATAFLOW, uploadFromDataflowWorker);
    yield takeEvery([CHANGE_LOCALE, CHANGE_MODE], fetchDataflowsFromAllSpacesWorker);
    yield takeEvery([UPLOAD_FROM_DATAFLOW, actions.SELECT_SPACE], fetchDataflowsFromOneSpaceWorker);
    yield takeEvery(
      [FETCH_DATAFLOWS_SUCCESS, FETCH_DATAFLOWS_ERROR],
      hasFetchDataflowsFromAllSpacesWorker,
    );
    yield takeLatest(FETCH_DATAFLOWS_SUCCESS, isSelectedDataflowStillRelevantWorker);
  };
};
