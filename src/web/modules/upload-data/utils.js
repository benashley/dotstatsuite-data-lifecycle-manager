export const getFetchDataflowsOptions = space => ({
  agency: 'all',
  space,
  type: 'dataflow',
  version: 'all',
});
