import { get } from 'lodash';
import reducerFactory from '../../store/reducer-factory';
import {
  model as modelFactory,
  actions as actionsFactory,
  saga as sagaFactory,
  selectors as uploadSelectorsFactory,
  withUpload,
} from '../upload';
import selectorsFactory from './selectors';
import { upload } from '../../api/upload-data';
import { EXCEL_EDD, SDMX_CSV } from '../upload';
import handlers from './action-creators';
import {
  withDataflowsFactory,
  withUploadModeFactory,
  withUploadPathFactory,
} from './with-upload-data';
import fetchDataflowsSagaFactory from './saga';

const MODULE = 'UPLOAD_DATA';
const NAMESPACE = 'uploadData';
const uploadDataModel = {
  dropzoneConfig: {
    disableClick: true,
  },
  mode: EXCEL_EDD,
  modes: [EXCEL_EDD, SDMX_CSV],
  dataflow: null,
  dataflows: {},
  dataflowsError: {},
  isFetchingDataflows: false,
  byPath: false,
  filePath: undefined,
};
const model = modelFactory(uploadDataModel);
const actions = actionsFactory(MODULE, model);
const getState = state => get(state, NAMESPACE);
const selectors = selectorsFactory(uploadSelectorsFactory(getState));
const apiCall = upload;

export const uploadDataReducer = reducerFactory(model, { ...actions.handlers, ...handlers });
export const uploadDataSaga = sagaFactory(actions, selectors, apiCall);
export const withUploadMode = withUploadModeFactory(selectors);
export const withDataflows = withDataflowsFactory(selectors);
export const fetchDataflowsSaga = fetchDataflowsSagaFactory(actions, selectors);
export const withUploadData = withUpload(actions, selectors);
export const withUploadPath = withUploadPathFactory(selectors);
