import { get } from 'lodash';
import { MODULE } from './constants';
import model from './model';

export const ITEM_TRANSFER_OPEN = `${MODULE}_ITEM_TRANSFER_OPEN`;
export const openItemTransfer = type => id => ({ type: ITEM_TRANSFER_OPEN, payload: { id, type } });
export const openItemTransferHandler = (state, action) => ({
  ...state,
  artefactId: action.payload.id,
  isOpenMenu: true,
  transferType: action.payload.type,
});

export const SELECTION_TRANSFER_OPEN = `${MODULE}_SELECTION_TRANSFER_OPEN`;
export const openSelectionTransfer = type => () => ({
  type: SELECTION_TRANSFER_OPEN,
  payload: { type },
});
export const openSelectionTransferHandler = (state, action) => ({
  ...state,
  isOpenMenu: true,
  isSelection: true,
  transferType: action.payload.type,
});

export const TRANSFER_CLOSE = `${MODULE}_TRANSFER_CLOSE`;
export const closeTransfer = () => ({ type: TRANSFER_CLOSE });
export const closeTransferHandler = () => model();

export const TOGGLE_HAS_DATA_QUERY = `${MODULE}_TOGGLE_HAS_DATA_QUERY`;
export const toggleHasDataQuery = () => ({ type: TOGGLE_HAS_DATA_QUERY });
export const toggleHasDataQueryHandler = state => {
  const hasDataQuery = state.hasDataQuery;
  const dataQuery = state.dataQuery;
  return {
    ...state,
    dataQuery: hasDataQuery ? undefined : dataQuery,
    hasDataQuery: !hasDataQuery,
  };
};

export const UPDATE_DATA_QUERY = `${MODULE}_UPDATE_DATA_QUERY`;
export const updateDataQuery = dataQuery => ({ type: UPDATE_DATA_QUERY, payload: { dataQuery } });
export const updateDataQueryHandler = (state, action) => ({
  ...state,
  dataQuery: action.payload.dataQuery,
});

export const TOGGLE_DESTINATION_SPACE = `${MODULE}_TOGGLE_DESTINATION_SPACE`;
export const toggleDestinationSpace = space => ({
  type: TOGGLE_DESTINATION_SPACE,
  payload: { space },
});
export const toggleDestinationSpaceHandler = (state, action) => {
  const spaceId = action.payload.space.id;
  const stateSpaceId = get(state, 'destinationSpace.id');
  return {
    ...state,
    destinationSpace: spaceId === stateSpaceId ? undefined : action.payload.space,
  };
};

export default {
  [ITEM_TRANSFER_OPEN]: openItemTransferHandler,
  [SELECTION_TRANSFER_OPEN]: openSelectionTransferHandler,
  [TRANSFER_CLOSE]: closeTransferHandler,
  [TOGGLE_HAS_DATA_QUERY]: toggleHasDataQueryHandler,
  [UPDATE_DATA_QUERY]: updateDataQueryHandler,
  [TOGGLE_DESTINATION_SPACE]: toggleDestinationSpaceHandler,
};
