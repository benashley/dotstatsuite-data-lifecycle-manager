import { createSelector } from 'reselect';
import { get } from 'lodash';

const getState = state => get(state, 'transferControl', {});

export const getIsOpenMenu = () =>
  createSelector(
    getState,
    state => get(state, 'isOpenMenu', false),
  );

export const getArtefactId = () =>
  createSelector(
    getState,
    state => get(state, 'artefactId'),
  );

export const getTransferType = () =>
  createSelector(
    getState,
    state => get(state, 'transferType'),
  );

export const getIsSelection = () =>
  createSelector(
    getState,
    state => get(state, 'isSelection', false),
  );

export const getHasDataQuery = () =>
  createSelector(
    getState,
    state => get(state, 'hasDataQuery', false),
  );

export const getDataQuery = () =>
  createSelector(
    getState,
    state => get(state, 'dataQuery', undefined),
  );

export const getDestinationSpace = () =>
  createSelector(
    getState,
    state => get(state, 'destinationSpace', undefined),
  );
