import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {
  closeTransfer,
  toggleDestinationSpace,
  toggleHasDataQuery,
  updateDataQuery,
} from './action-creators';
import {
  getArtefactId,
  getDataQuery,
  getDestinationSpace,
  getHasDataQuery,
  getIsOpenMenu,
  getIsSelection,
  getTransferType,
} from './selectors';
import { getSelection } from '../selection/selectors';
import { transferDatas } from '../selection/action-creators';
import { transferData } from '../transfer-data';
import { getInternalSpaces } from '../config';
import { getArtefacts } from '../artefacts';

export default Component => {
  const mapDispatchToProps = {
    closeTransfer,
    itemDataTransfer: transferData,
    selectionDataTransfer: transferDatas,
    toggleDestinationSpace,
    toggleHasDataQuery,
    updateDataQuery,
  };

  const mapStateToProps = createStructuredSelector({
    artefactId: getArtefactId(),
    artefacts: getArtefacts(),
    dataQuery: getDataQuery(),
    destinationSpace: getDestinationSpace(),
    hasDataQuery: getHasDataQuery(),
    isOpenMenu: getIsOpenMenu(),
    isSelection: getIsSelection(),
    selection: getSelection(),
    spaces: getInternalSpaces(),
    transferType: getTransferType(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
