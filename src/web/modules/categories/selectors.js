import { createSelector } from 'reselect';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { getCategories as getCategoriesFromFilters } from '../filters';
import { getLocale } from '../i18n';
import * as R from 'ramda';

const getState = state => state.categories;

export const getIsFetching = () =>
  createSelector(
    getState,
    R.prop('isFetching'),
  );

export const getCategories = () =>
  createSelector(
    getState,
    R.prop('categories'),
  );

const recurse = parentId => categories =>
  R.pipe(
    R.propOr([], parentId),
    R.map(cat => {
      const childNodes = recurse(cat.id)(categories);
      return R.pipe(
        cat => {
          if (!R.isNil(cat.version)) {
            return { ...cat, label: `${cat.label} - ${cat.agencyId}:${cat.code}(${cat.version})` };
          }
          return cat;
        },
        R.when(R.always(!R.isEmpty(childNodes)), R.assoc('childNodes', childNodes)),
      )(cat);
    }),
  )(categories);

export const getCategoriesRequestArgs = space =>
  createSelector(
    getLocale(),
    R.pipe(
      locale => ({
        datasource: {
          id: R.prop('id', space),
          url: R.prop('endpoint', space),
        },
        locale,
        type: 'categoryscheme',
        identifiers: { agencyId: 'all', code: 'all', version: 'all' },
      }),
      getRequestArgs,
    ),
  );

export const getCategoriesTreeBySpace = spaceId =>
  createSelector(
    getCategoriesFromFilters(),
    R.pipe(
      R.values,
      R.groupBy(R.prop('spaceId')),
      R.propOr([], spaceId),
      R.groupBy(
        R.pipe(
          R.prop('parentId'),
          R.when(R.isNil, R.always('#root')),
        ),
      ),
      recurse('#root'),
    ),
  );
