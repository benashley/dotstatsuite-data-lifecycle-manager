import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const categoriesReducer = reducerFactory(model(), handlers);
export { getCategories } from './selectors';

export { default as categoriesSaga } from './saga';
export { withCategories, withSpacedCategories } from './with-categories';
