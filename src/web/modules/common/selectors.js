import { createSelector } from 'reselect';
import { get } from 'lodash';

// temporary avoiding circular dependency issue
const getArtefactsState = state => state.artefacts;
const getArtefacts = () =>
  createSelector(
    getArtefactsState,
    state => state.artefacts,
  );
export const getArtefact = id =>
  createSelector(
    [getArtefacts()],
    artefacts => get(artefacts, id),
  );
export const getArtefactOptions = id =>
  createSelector(
    [getArtefact(id)],
    artefact => ({
      agency: artefact.agencyId,
      type: artefact.type,
      version: artefact.version,
      code: artefact.code,
      space: artefact.space,
    }),
  );
const getTransfer = id => state => state.transferArtefact[id];
export const getTransferArtefactOptions = id =>
  createSelector(
    [getArtefact(id), getTransfer(id)],
    (artefact, transfer) => {
      return {
        agency: artefact.agencyId,
        type: artefact.type,
        version: artefact.version,
        code: artefact.code,
        space: transfer.space,
      };
    },
  );
// temporary avoiding circular dependency issue
