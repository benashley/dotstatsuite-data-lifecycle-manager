import { isEmpty, sortBy } from 'lodash';

export const withSort = fields => spaces =>
  sortBy(spaces, isEmpty(fields) ? ['order', 'label'] : fields);
