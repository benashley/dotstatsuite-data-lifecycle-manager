import { get } from 'lodash';

const transfered = type => (id, log) => ({ type, payload: { id, log } });
const transferedHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...get(state, action.payload.id, {}),
    log: action.payload.log,
    isTransfering: false,
  },
});

export default module => {
  const TRANSFER = `${module}_TRANSFER`;
  const transfer = (id, space, withReferences, dataQuery) => ({
    type: TRANSFER,
    payload: { id, space, withReferences, dataQuery },
  });
  const transferHandler = (state, action) => ({
    ...state,
    [action.payload.id]: {
      ...get(state, action.payload.id, {}),
      id: action.payload.id,
      space: action.payload.space,
      isTransfering: true,
      log: null,
    },
  });

  const TRANSFER_SUCCESS = `${module}_TRANSFER_SUCCESS`;
  const transferSuccess = transfered(TRANSFER_SUCCESS);

  const TRANSFER_WARNING = `${module}_TRANSFER_WARNING`;
  const transferWarning = transfered(TRANSFER_WARNING);

  const TRANSFER_ERROR = `${module}_TRANSFER_ERROR`;
  const transferError = transfered(TRANSFER_ERROR);

  return {
    TRANSFER,
    transfer,
    TRANSFER_SUCCESS,
    transferSuccess,
    TRANSFER_WARNING,
    transferWarning,
    TRANSFER_ERROR,
    transferError,
    handlers: {
      [TRANSFER]: transferHandler,
      [TRANSFER_SUCCESS]: transferedHandler,
      [TRANSFER_WARNING]: transferedHandler,
      [TRANSFER_ERROR]: transferedHandler,
    },
  };
};
