import { createSelector } from 'reselect';
import { get, keys, pick, reject } from 'lodash';
import { getArtefact } from '../common/selectors';
import { getInternalSpaces } from '../config';

export default getState => {
  const selectorHelper = (key, getSubState) => id =>
    createSelector(
      [getSubState(id)],
      state => get(state, key),
    );

  const getTransfer = id =>
    createSelector(
      getState,
      state => get(state, id),
    );

  return {
    getIds: () =>
      createSelector(
        getState,
        state => keys(state),
      ),
    getTransfer,
    getIsUpdating: () => () => false,
    getIsTransfering: selectorHelper('isTransfering', getTransfer),
    getLog: selectorHelper('log', getTransfer),
    getSpace: selectorHelper('space', getTransfer),
    getSourceSpace: selectorHelper('space', getArtefact),
    getTransferOptions: id =>
      createSelector(
        [getTransfer(id)],
        transfer => pick(transfer, ['space']),
      ),
    getDestinationSpaces: id =>
      createSelector(
        [getArtefact(id), getInternalSpaces()],
        (artefact, spaces) => reject(spaces, { id: get(artefact, 'space.id') }),
      ),
  };
};
