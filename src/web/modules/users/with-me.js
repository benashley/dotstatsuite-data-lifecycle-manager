import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { getMe, getMyAgenciesWithSpaces } from './selectors';

export default Component => {
  const mapDispatchToProps = {};

  const mapStateToProps = createStructuredSelector({
    me: getMe(),
    agencies: getMyAgenciesWithSpaces(),
  });

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};
