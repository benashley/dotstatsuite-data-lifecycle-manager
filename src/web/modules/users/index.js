import { always, when } from 'ramda';

export const USER_SIGNED_IN = 'USER_SIGNED_IN';
const USER_SIGNED_OUT = 'USER_SIGNED_OUT';

const model = () => ({
  user: undefined,
  isLogged: false,
  isInitialLogin: false,
});

export default (state = model(), action = {}) => {
  switch (action.type) {
    case USER_SIGNED_IN:
      return {
        ...state,
        user: action.user,
        isLogged: true,
        isInitialLogin: when(always(state.isLogged), always(false))(true),
      };
    case USER_SIGNED_OUT:
      return model();
    default:
      return state;
  }
};

export const userSignIn = ({ name, email, sub: id }) => ({
  type: USER_SIGNED_IN,
  user: { name, email, id },
});

export const userSignOut = () => ({ type: USER_SIGNED_OUT });

export { default as withMe } from './with-me';
export { getMyAgencies, getIsInitialLogin } from './selectors';
