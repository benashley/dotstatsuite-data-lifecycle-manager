import { createSelector } from 'reselect';
import { find, get, reduce } from 'lodash';
import { getAgencies as getAgenciesFromAgencies } from '../agencies';
import { getSpaces } from '../config';
import getAgenciesEngine from './utils';

const getState = state => state.users;

export const getMe = () =>
  createSelector(
    getState,
    state => state.user,
  );

export const getMeAgencies = () =>
  createSelector(
    [getMe()],
    me => get(me, 'agencies'),
  );

export const getMyAgencies = () =>
  createSelector(
    [getSpaces(), getMeAgencies(), getAgenciesFromAgencies()],
    getAgenciesEngine,
  );

export const getMyAgenciesWithSpaces = () =>
  createSelector(
    [getMyAgencies(), getSpaces()],
    (agencies, spaces) =>
      reduce(
        agencies,
        (acc, agency) => ({
          ...acc,
          [agency.id]: { ...agency, space: get(spaces, find(agency.spaces)) },
        }),
        {},
      ),
  );

export const getIsInitialLogin = () =>
  createSelector(
    getState,
    state => state.isInitialLogin,
  );
