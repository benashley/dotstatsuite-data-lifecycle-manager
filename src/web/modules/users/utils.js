import { get, head, includes, pickBy, reduce } from 'lodash';
import { getHierarchicalCodeHelper } from '../agencies';

const parsing = (codes, agencies, spaceId) => {
  if (head(codes) === '*') {
    const id = `${spaceId}:all`;
    return { [id]: { code: 'all', hierarchicalCode: 'all', id, spaces: { [spaceId]: spaceId } } };
  }
  const _agencies = reduce(
    codes,
    (acc, code) => {
      if (includes(code, '.**')) {
        return {
          ...acc,
          ...pickBy(
            agencies,
            // eslint-disable-next-line
            agency =>
              getHierarchicalCodeHelper(agency).match(new RegExp(code.replace('.**', '..*$'), 'g')),
          ),
        };
      }
      if (includes(code, '.*')) {
        return {
          ...acc,
          ...pickBy(
            agencies,
            // eslint-disable-next-line
            agency =>
              getHierarchicalCodeHelper(agency).match(
                new RegExp(code.replace('.*', '.[^.]+$'), 'g'),
              ),
          ),
        };
      }
      return {
        ...acc,
        ...pickBy(agencies, agency => getHierarchicalCodeHelper(agency) === code),
      };
    },
    {},
  );
  return reduce(
    _agencies,
    (acc, agency) => {
      const id = `${spaceId}:${agency.id}`;
      return { ...acc, [id]: { ...agency, id, spaces: { [spaceId]: spaceId } } };
    },
    {},
  );
};

export default (spaces, myAgencies, agencies) =>
  reduce(
    spaces,
    (acc, space) => {
      if (get(myAgencies, '*')) {
        return { ...acc, ...parsing(get(myAgencies, '*'), agencies, space.id) };
      }
      if (!get(myAgencies, space.id)) {
        return acc;
      }
      return { ...acc, ...parsing(get(myAgencies, space.id), agencies, space.id) };
    },
    {},
  );
