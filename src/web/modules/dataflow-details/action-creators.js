import { get, omit } from 'lodash';

export const REQUEST_DETAILS = 'REQUEST_DETAILS';
export const requestDetails = id => ({
  type: REQUEST_DETAILS,
  payload: { id },
});
export const requestDetailsHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    isFetchingCategories: true,
    isFetchingObservations: true,
  },
});

export const REQUEST_CATEGORISATIONS_SUCCESS = 'REQUEST_CATEGORISATIONS_SUCCESS';
export const requestCategorisationsSuccess = (id, categories) => ({
  type: REQUEST_CATEGORISATIONS_SUCCESS,
  payload: { id, categories },
});
export const requestCategorisationsSuccessHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...get(state, action.payload.id, {}),
    categories: action.payload.categories,
    isFetchingCategories: false,
  },
});

export const REQUEST_CATEGORISATIONS_ERROR = 'REQUEST_CATEGORISATIONS_ERROR';
export const requestCategorisationsError = (id, error) => ({
  type: REQUEST_CATEGORISATIONS_ERROR,
  payload: { id, error },
});
export const requestCategorisationsErrorHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...get(state, action.payload.id, {}),
    categoriesError: action.payload.error,
    isFetchingCategories: false,
  },
});

export const REQUEST_OBSERVATIONS_SUCCESS = 'REQUEST_OBSERVATIONS_SUCCESS';
export const requestObservationsSuccess = (id, observations) => ({
  type: REQUEST_OBSERVATIONS_SUCCESS,
  payload: { id, observations },
});
export const requestObservationsSuccessHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...get(state, action.payload.id, {}),
    observations: action.payload.observations,
    isFetchingObservations: false,
  },
});

export const REQUEST_OBSERVATIONS_ERROR = 'REQUEST_OBSERVATIONS_ERROR';
export const requestObservationsError = (id, error) => ({
  type: REQUEST_OBSERVATIONS_ERROR,
  payload: { id, error },
});
export const requestObservationsErrorHandler = (state, action) => ({
  ...state,
  [action.payload.id]: {
    ...get(state, action.payload.id, {}),
    observations: action.payload.observations,
    isFetchingObservations: false,
  },
});

export const DELETE_DETAILS = 'DELETE_DETAILS';
export const deleteDetails = id => ({
  type: DELETE_DETAILS,
  payload: { id },
});
export const deleteDetailsHandler = (state, action) => omit(state, action.payload.id);

export const DELETE_ALL_DETAILS = 'DELETE_ALL_DETAILS';
export const deleteAllDetails = () => ({
  type: DELETE_ALL_DETAILS,
});
export const deleteAllDetailsHandler = () => ({});

export default {
  [REQUEST_DETAILS]: requestDetailsHandler,
  [REQUEST_CATEGORISATIONS_SUCCESS]: requestCategorisationsSuccessHandler,
  [REQUEST_CATEGORISATIONS_ERROR]: requestCategorisationsErrorHandler,
  [REQUEST_OBSERVATIONS_SUCCESS]: requestObservationsSuccessHandler,
  [REQUEST_OBSERVATIONS_ERROR]: requestObservationsErrorHandler,
  [DELETE_DETAILS]: deleteDetailsHandler,
  [DELETE_ALL_DETAILS]: deleteAllDetailsHandler,
};
