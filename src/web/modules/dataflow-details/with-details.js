import { connect } from 'react-redux';
import { compose, mapProps } from 'recompose';
import { createStructuredSelector } from 'reselect';
import { omit } from 'lodash';
import { deleteDetails, requestDetails } from './action-creators';
import { getCategoriesState, getObservationsState, getIsExpanded } from './selectors';

const withDataflowDetails = id => Component => {
  const mapStateToProps = createStructuredSelector({
    categories: getCategoriesState(id),
    observations: getObservationsState(id),
    isDetailsExpanded: getIsExpanded(id),
  });

  const mapDispatchToProps = {
    deleteDetails,
    requestDetails,
  };

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Component);
};

export default id =>
  compose(
    withDataflowDetails(id),
    mapProps(props => {
      const { type } = props;
      if (type !== 'dataflow') {
        return omit(props, ['deleteDetails', 'requestDetails']);
      }
      return props;
    }),
  );
