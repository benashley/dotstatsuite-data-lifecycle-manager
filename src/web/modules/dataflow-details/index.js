import reducerFactory from '../../store/reducer-factory';
import model from './model';
import handlers from './action-creators';

export const dataflowDetailsReducer = reducerFactory(model(), handlers);
export { default as dataflowDetailsSaga } from './saga';
export { default as withDataflowDetails } from './with-details';
