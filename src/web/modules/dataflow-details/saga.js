import { takeEvery, call, put, select } from 'redux-saga/effects';
import axios from 'axios';
import {
  REQUEST_DETAILS,
  requestCategorisationsError,
  requestCategorisationsSuccess,
  requestObservationsError,
  requestObservationsSuccess,
  deleteAllDetails,
} from './action-creators';
import { ARTEFACTS_CLEAR } from '../artefacts/action-creators';
import { getArtefactOptions } from '../common/selectors';
import { getDataflowDataRequestArgs } from './selectors';
import { parseDataRange } from '@sis-cc/dotstatsuite-sdmxjs';
import { getLocale } from '../i18n';
import { getDataflowCategories } from '../../sdmx-lib';

function* requestWorker(request, options, successCb, errorCb) {
  try {
    const { data } = yield call(request, options);
    yield put(successCb(data));
  } catch (error) {
    yield put(errorCb(error));
  }
}

function* getObservationsWorker(id) {
  const { url, headers, params } = yield select(getDataflowDataRequestArgs(id));
  try {
    const response = yield call(axios.get, url, { headers, params });
    const range = yield call(parseDataRange, response);
    yield put(requestObservationsSuccess(id, range.total));
  } catch (error) {
    yield put(requestObservationsError(id, error));
  }
}

export function* detailsWorker(action) {
  const id = action.payload.id;
  const dataflowOptions = yield select(getArtefactOptions(id));
  const locale = yield select(getLocale());

  const categorisationsSuccessCb = categories => requestCategorisationsSuccess(id, categories);
  const categorisationsErrorCb = error => requestCategorisationsError(id, error);

  yield call(
    requestWorker,
    getDataflowCategories,
    { ...dataflowOptions, locale },
    categorisationsSuccessCb,
    categorisationsErrorCb,
  );
  yield call(getObservationsWorker, id);
}

export function* resetDetailsWorker() {
  yield put(deleteAllDetails());
}

export default function* saga() {
  yield takeEvery(REQUEST_DETAILS, detailsWorker);
  yield takeEvery(ARTEFACTS_CLEAR, resetDetailsWorker);
}
