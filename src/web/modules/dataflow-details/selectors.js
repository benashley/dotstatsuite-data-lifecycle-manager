import { createSelector } from 'reselect';
import { getRequestArgs } from '@sis-cc/dotstatsuite-sdmxjs';
import { isNil, pipe, pick, propOr } from 'ramda';
import { getArtefactOptions } from '../common/selectors';

const getState = state => state.dataflowDetails;

export const getDetails = id =>
  createSelector(
    getState,
    state => state[id],
  );

export const getCategoriesState = id =>
  createSelector(
    getDetails(id),
    state => ({
      ...pick(['categories', 'categoriesError'], state || {}),
      isFetching: propOr(false, 'isFetchingCategories', state),
    }),
  );

export const getObservationsState = id =>
  createSelector(
    getDetails(id),
    state => ({
      ...pick(['observations', 'observationsError'], state || {}),
      isFetching: propOr(false, 'isFetchingObservations', state),
    }),
  );

export const getIsExpanded = id =>
  createSelector(
    getDetails(id),
    state => !isNil(state),
  );

export const getDataflowDataRequestArgs = id =>
  createSelector(
    getArtefactOptions(id),
    pipe(
      ({ agency, code, space, version }) => ({
        datasource: { ...space, url: space.endpoint },
        identifiers: { agencyId: agency, code, version },
        range: [0, 0],
        type: 'data',
      }),
      getRequestArgs,
    ),
  );
