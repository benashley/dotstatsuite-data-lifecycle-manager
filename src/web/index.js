import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import '@babel/polyfill';
import App from './components/common/app';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import keycloakAdapter from 'keycloak-js';
import { Router } from 'react-router';
import { createBrowserHistory } from 'history';
import { syncHistoryWithStore } from 'react-router-redux';
import { KeycloakProvider } from 'react-keycloak';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import routes from './routes';
import { RoutesContextProvider } from './components/routes-context';
import { userSignIn } from './modules/users';
import initializeI18n, { I18nProvider } from './modules/i18n';
import meta from '../../package.json';
import '@blueprintjs/core/dist/blueprint.css';

/*
 * KEYCLOAK
 * -------------------------------------------------------------------------------------------------
 */
const { keycloak: keycloakOIDC } = window.CONFIG;
const keycloak = keycloakAdapter(keycloakOIDC);
const onKeycloakEvent = (event, error) => {
  console.log('[KEYCLOAK] onKeycloakEvent', event, error); // eslint-disable-line no-console
};

const onKeycloakTokens = tokens => {
  console.log('onKeycloakTokens', tokens); // eslint-disable-line no-console
  keycloak.loadUserInfo().then(userInfo => store.dispatch(userSignIn(userInfo)));
  axios.defaults.headers.common = { Authorization: `Bearer ${tokens.idToken}` };
  window.TOKENS = tokens;
};

/*
 * STORE
 * -------------------------------------------------------------------------------------------------
 */
const initialState = { config: { ...window.SETTINGS } };
const history = createBrowserHistory();
const store = configureStore(initialState, history);
syncHistoryWithStore(history, store);

/*
 * I18N
 * -------------------------------------------------------------------------------------------------
 */
initializeI18n(window.SETTINGS.i18n);

/*
 * REACT
 * -------------------------------------------------------------------------------------------------
 */
const ROOT = (
  <KeycloakProvider
    keycloak={keycloak}
    onEvent={onKeycloakEvent}
    onTokens={onKeycloakTokens}
    initConfig={{ onLoad: 'check-sso', promiseType: 'native' }}
  >
    <Provider store={store}>
      <I18nProvider messages={window.I18N}>
        <RoutesContextProvider value={routes}>
          <Router history={history}>
            <MuiThemeProvider theme={createMuiTheme()}>
              <CssBaseline />
              <App />
            </MuiThemeProvider>
          </Router>
        </RoutesContextProvider>
      </I18nProvider>
    </Provider>
  </KeycloakProvider>
);

ReactDOM.render(ROOT, document.getElementById('root'));
console.info(`${meta.name}@${meta.version}`); // eslint-disable-line no-console
