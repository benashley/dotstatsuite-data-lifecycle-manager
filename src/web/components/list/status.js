import React from 'react';
import PropTypes from 'prop-types';
import { compose, withProps } from 'recompose';
import { Classes, Tag, Spinner } from '@blueprintjs/core';
import { size } from 'lodash';
import { FormattedMessage } from 'react-intl';
import classnames from 'classnames';

export const Status = ({ current, total, isFetching }) => {
  if (isFetching) return <Spinner className={Classes.SMALL} />;

  const label = current ? (
    <FormattedMessage id="artefacts.status" values={{ current, total }} />
  ) : (
    <FormattedMessage id="artefacts.no.artefact" />
  );

  return <Tag className={classnames(Classes.MINIMAL, Classes.LARGE)}>{label}</Tag>;
};

Status.propTypes = {
  current: PropTypes.number,
  total: PropTypes.number,
  isFetching: PropTypes.bool,
};

export const withStatus = compose(
  withProps(state => ({
    status: {
      current: size(state.list.items),
      total: size(state.items),
    },
  })),
);
