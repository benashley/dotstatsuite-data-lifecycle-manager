import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Button, Tag } from '@blueprintjs/core';
import { withState, withHandlers, compose, withProps } from 'recompose';
import { ceil, size, times, slice, min } from 'lodash';
import classnames from 'classnames';
import { FormattedMessage } from 'react-intl';

export const Pagination = ({ page, pages, pagesBuffered, pageOffset, pageChange }) => {
  const [start, end, single] = [page === 0, page === pages - 1, pages < 2];
  if (single) return null;

  return (
    <div className={classnames(Classes.BUTTON_GROUP)}>
      <Button iconName="chevron-backward" onClick={pageChange(0)} disabled={start} />
      <Button iconName="chevron-left" onClick={pageChange(page - 1)} disabled={start} />
      {times(pagesBuffered, n => {
        const pageBuffered = n + pageOffset;
        return (
          <Button
            key={pageBuffered}
            text={pageBuffered + 1}
            onClick={pageBuffered === page ? null : pageChange(pageBuffered)}
            active={pageBuffered === page}
          />
        );
      })}
      <Button iconName="chevron-right" onClick={pageChange(page + 1)} disabled={end} />
      <Button iconName="chevron-forward" onClick={pageChange(pages - 1)} disabled={end} />
    </div>
  );
};

Pagination.propTypes = {
  page: PropTypes.number,
  pages: PropTypes.number,
  pagesBuffered: PropTypes.number,
  pageOffset: PropTypes.number,
  pageChange: PropTypes.func,
};

export const PaginationStatus = ({ pages }) => {
  return (
    <Tag className={classnames(Classes.MINIMAL, Classes.LARGE)}>
      <FormattedMessage id="pagination.status" values={{ pages }} />
    </Tag>
  );
};

PaginationStatus.propTypes = {
  pages: PropTypes.number,
};

const setListHandler = pagination => list => {
  const { page, pageSize } = setPaginationHandler(pagination)(list);

  return { ...list, items: slice(list.items, page * pageSize, page * pageSize + pageSize) };
};

const setPaginationHandler = pagination => ({ items }) => {
  const { pageSize, pageBuffer } = pagination;
  const pageSymetry = pageBuffer * 2 + 1; // origin and(of) symetry, the bad name is real...
  const pages = ceil(size(items) / pageSize);
  const pagesBuffered = min([pageSymetry, pages]);
  const page = pagination.page < pages ? pagination.page : pages - 1;
  const pageOffset =
    pages <= pagesBuffered
      ? 0
      : page - pageBuffer <= 0
      ? 0
      : min([page - pageBuffer, pages - pageSymetry]);

  return { ...pagination, pages, pagesBuffered, page, pageOffset };
};

export const withPagination = ({ pageSize, pageBuffer }) =>
  compose(
    withState('pagination', 'setPagination', { page: 0, pageSize, pageBuffer }),
    withProps(state => ({
      list: setListHandler(state.pagination)(state.list), // overriden after pages computation
      pagination: setPaginationHandler(state.pagination)(state.list),
    })),
    withHandlers({
      paginationPageChange: ({ setPagination }) => page => event => {
        event.preventDefault();
        setPagination(state => ({ ...state, page }));
      },
    }),
  );
