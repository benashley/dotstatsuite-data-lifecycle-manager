import React from 'react';
import PropTypes from 'prop-types';
import { Classes, InputGroup, Popover, Position, Button } from '@blueprintjs/core';
import { injectIntl, defineMessages } from 'react-intl';
import { withState, withHandlers, compose, withProps } from 'recompose';
import {
  filter,
  includes,
  some,
  get,
  isFunction,
  toLower,
  map,
  reduce,
  size,
  gt,
  isArray,
} from 'lodash';
import glamorous from 'glamorous';
import classnames from 'classnames';

const translations = defineMessages({
  placeholder: { id: 'spotlight.placeholder' },
  label: { id: 'artefact.label' },
  code: { id: 'artefact.code' },
  description: { id: 'artefact.description' },
});

const Wrapper = glamorous.div({ padding: 10 });

export const SpotlightFields = injectIntl(({ fields, fieldChange, intl }) => {
  return (
    <Popover
      position={Position.BOTTOM}
      target={<Button className={Classes.MINIMAL} iconName="filter-list" />}
      content={
        <Wrapper
          className={classnames(
            Classes.BUTTON_GROUP,
            Classes.MINIMAL,
            Classes.VERTICAL,
            Classes.ALIGN_LEFT,
          )}
        >
          {map(fields, field => (
            <Button
              key={field.id}
              className={Classes.MINIMAL}
              text={intl.formatMessage(get(translations, field.id))}
              onClick={fieldChange(field)}
              iconName={field.isSelected ? 'tick' : 'blank'}
            />
          ))}
        </Wrapper>
      }
    />
  );
});

SpotlightFields.proptypes = {
  fieldChange: PropTypes.func,
  fields: PropTypes.objectOf(
    PropTypes.shape({
      id: PropTypes.string,
      isSelected: PropTypes.bool,
    }),
  ),
};

export const Spotlight = injectIntl(({ value, valueChange, fields, fieldChange, intl }) => {
  const clearAll = <Button className={Classes.MINIMAL} iconName="eraser" onClick={valueChange} />;
  const spotlightFields = gt(size(fields), 1) ? (
    <div>
      <SpotlightFields fields={fields} fieldChange={fieldChange} />
      {clearAll}
    </div>
  ) : (
    clearAll
  );

  return (
    <InputGroup
      leftIconName="search"
      onChange={valueChange}
      placeholder={intl.formatMessage(translations.placeholder)}
      value={value}
      rightElement={spotlightFields}
    />
  );
});

Spotlight.proptypes = {
  value: PropTypes.string,
  valueChange: PropTypes.func,
  fields: PropTypes.object,
  fieldChange: PropTypes.func,
};

const spotlightHandler = spotlight => item =>
  some(filter(spotlight.fields, 'isSelected'), field => {
    const itemValue = isFunction(field.accessor) ? field.accessor(item) : get(item, field.accessor);
    return includes(toLower(itemValue), toLower(spotlight.value));
  });

const setListHandler = spotlight => list => ({
  ...list,
  items: filter(list._items, spotlightHandler(spotlight)),
});

const setTreeHandler = spotlight => tree => {
  const recurse = nodes =>
    filter(nodes, node => {
      const nodeIsValid = spotlightHandler(spotlight)(node);
      const childrenAreValid = isArray(node.childNodes) && some(recurse(node.childNodes));
      return nodeIsValid || childrenAreValid;
    });

  return { ...tree, items: recurse(tree._items) };
};

export const withSpotlight = fields =>
  compose(
    withState('spotlight', 'setSpotlight', {
      value: '',
      fields: reduce(fields, (memo, field) => ({ ...memo, [field.id]: { ...field } }), {}),
    }),
    withProps(state => {
      const handler = state.list.isTree ? setTreeHandler : setListHandler;
      return { list: handler(state.spotlight)(state.list) };
    }),
    withHandlers({
      spotlightValueChange: ({ setSpotlight }) => event => {
        event.preventDefault();
        const value = event.target.value;
        setSpotlight(state => ({ ...state, value }));
      },
      spotlightFieldChange: ({ setSpotlight }) => field => event => {
        event.preventDefault();
        setSpotlight(state => ({
          ...state,
          fields: {
            ...state.fields,
            [field.id]: { ...field, isSelected: !field.isSelected },
          },
        }));
      },
    }),
  );
