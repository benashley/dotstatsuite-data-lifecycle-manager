import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Popover, Position, Button } from '@blueprintjs/core';
import { injectIntl, defineMessages } from 'react-intl';
import { withState, withHandlers, compose, withProps } from 'recompose';
import { filter, head, orderBy, sortBy, map, get, isFinite } from 'lodash';
import glamorous from 'glamorous';
import classnames from 'classnames';

const [ASC, DESC] = ['asc', 'desc'];

const translations = defineMessages({
  type: { id: 'artefact.type' },
  label: { id: 'artefact.label' },
  code: { id: 'artefact.code' },
  version: { id: 'artefact.version' },
  'is.final': { id: 'artefact.is.final' },
  owner: { id: 'artefact.owner' },
});

const Wrapper = glamorous.div({ padding: 10 });

export const iconFactory = (field, isField) => {
  if (!isField || !field.isSelected) return 'blank';

  switch (field.direction) {
    case ASC:
      return 'sort-alphabetical';
    case DESC:
      return 'sort-alphabetical-desc';
    default:
      return 'blank';
  }
};

export const Sort = injectIntl(({ field, fields, fieldChange, intl }) => {
  return (
    <Popover
      position={Position.BOTTOM}
      target={
        <Button
          className={Classes.MINIMAL}
          text={intl.formatMessage(get(translations, field.id))}
          iconName={iconFactory(field, true)}
        />
      }
      content={
        <Wrapper
          className={classnames(
            Classes.BUTTON_GROUP,
            Classes.MINIMAL,
            Classes.VERTICAL,
            Classes.ALIGN_LEFT,
          )}
        >
          {map(fields, _field => (
            <Button
              key={_field.id}
              className={Classes.MINIMAL}
              text={intl.formatMessage(get(translations, _field.id))}
              iconName={iconFactory(_field, field.id === _field.id)}
              onClick={fieldChange(_field)}
              active={field.id === _field.id && _field.isSelected}
            />
          ))}
        </Wrapper>
      }
    />
  );
});

Sort.proptypes = {
  field: PropTypes.objectOf(
    PropTypes.shape({
      id: PropTypes.string,
      direction: PropTypes.oneOf([ASC, DESC]),
      isSelected: PropTypes.bool,
    }),
  ),
  fields: PropTypes.object,
  fieldChange: PropTypes.func,
};

// isSelected is useful for the UI
// for the sort, we use several casdading and the index is the relavant attr to select fields
const getSortFields = fields => sortBy(filter(fields, field => isFinite(field.index)), 'index');
const getSortDirection = (field, _field) => {
  if (_field.id !== field.id) return;

  switch (
    field.direction // revert
  ) {
    case ASC:
      return DESC;
    case DESC:
      return ASC;
    default:
      return ASC;
  }
};

const setListHandler = sort => list => {
  const sortFields = getSortFields(sort.fields);

  return {
    ...list,
    items: orderBy(list.items, map(sortFields, 'accessor'), map(sortFields, 'direction')),
  };
};

export const withSort = fields =>
  compose(
    withState('sort', 'setSort', { fields: [...fields] }),
    // sorts can be combined, ie sort by type then by name, only the selected sort is displayed
    withProps(state => ({
      list: setListHandler(state.sort)(state.list),
      sort: { ...state.sort, field: head(getSortFields(state.sort.fields)) },
    })),
    withHandlers({
      sortFieldChange: ({ setSort }) => field => event => {
        event.preventDefault();
        setSort(state => ({
          ...state,
          fields: map(fields, _field => ({
            ..._field,
            direction: getSortDirection(field, _field),
            isSelected: _field.id === field.id,
            index: _field.id === field.id ? 0 : _field.index === 0 ? null : _field.index,
          })),
        }));
      },
    }),
  );
