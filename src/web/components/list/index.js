import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { withState, withProps, compose } from 'recompose';
import { map } from 'lodash';

export { Spotlight, withSpotlight } from './spotlight';
export { Pagination, withPagination, PaginationStatus } from './pagination';
export { Status, withStatus } from './status';
export { Sort, withSort } from './sort';

const ListWrapper = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
});

export const List = ({ items, itemRenderer, handlers }) => {
  return (
    <ListWrapper>
      {map(items, item => {
        const Renderer = item.renderer || itemRenderer;
        if (!Renderer) {
          console.log('no renderer found', 'item.id', item.id); // eslint-disable-line no-console
          return null;
        }
        return <Renderer key={item.id} {...item} handlers={handlers} />;
      })}
    </ListWrapper>
  );
};

List.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }),
  ),
  itemRenderer: PropTypes.func,
  handlers: PropTypes.object,
};

export const withList = ({ isTree } = {}) =>
  compose(
    withState('list', 'setList', { isTree }),
    withProps(({ items, list }) => ({ list: { ...list, items, _items: items } })),
  );
