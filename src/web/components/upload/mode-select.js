import React from 'react';
import PropTypes from 'prop-types';
import { Button, ButtonGroup } from '@blueprintjs/core';
import { map } from 'lodash';
import { FormattedMessage } from 'react-intl';

const ModeSelect = ({ modes, selected, changeMode }) => {
  const onClick = mode => () => {
    changeMode(mode);
  };

  return (
    <ButtonGroup>
      {map(modes, mode => (
        <Button active={mode === selected} key={mode} onClick={onClick(mode)}>
          <FormattedMessage id={`upload.mode.${mode}`} />
        </Button>
      ))}
    </ButtonGroup>
  );
};

ModeSelect.propTypes = {
  changeMode: PropTypes.func.isRequired,
  modes: PropTypes.arrayOf(PropTypes.string).isRequired,
  selected: PropTypes.string.isRequired,
};

export default ModeSelect;
