import React from 'react';
import PropTypes from 'prop-types';
import { compose, renameProps, withProps } from 'recompose';
import { Checkbox, Classes, Spinner } from '@blueprintjs/core';
import glamorous from 'glamorous';
import { FormattedMessage } from 'react-intl';
import { withDataflows } from '../../modules/upload-data';
import { List, Spotlight, withList, withSpotlight } from '../list';
import { isEmpty, isNull, omit } from 'lodash';

const ListWrapper = glamorous.div({
  maxHeight: 200,
  overflow: 'auto',
});

const StyledItem = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  flexWrap: 'no-wrap',
  margin: 5,
});

const StyledInfos = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
});

const StyledMetas = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'no-wrap',
});

const StyledCheckbox = glamorous(Checkbox)({
  marginRight: 5,
  '.pt-control': {
    marginBottom: 0,
  },
});

const Dataflow = props => {
  const { agencyId, code, handlers, isSelected, label, version } = props;

  return (
    <StyledItem>
      <StyledCheckbox
        checked={isSelected}
        onChange={() => {
          handlers.toggleDataflow(omit(props, ['handlers']));
        }}
      />
      <StyledInfos>
        <span>{isNull(label) ? <FormattedMessage id="artefact.no.label" /> : label}</span>
        <StyledMetas>
          <span>[{code}]</span>
          <span className={Classes.NAVBAR_DIVIDER}></span>
          <span>[{version}]</span>
          <span className={Classes.NAVBAR_DIVIDER}></span>
          <span>{agencyId}</span>
        </StyledMetas>
      </StyledInfos>
    </StyledItem>
  );
};

Dataflow.propTypes = {
  agencyId: PropTypes.string,
  code: PropTypes.string,
  handlers: PropTypes.object,
  isSelected: PropTypes.bool,
  label: PropTypes.string,
  version: PropTypes.string,
};

const Dataflows = props => {
  if (props.isFetching) {
    return <Spinner className={Classes.SMALL} />;
  }
  if (isEmpty(props.list.items)) {
    return (
      <div>
        <Spotlight
          {...props.spotlight}
          fieldChange={props.spotlightFieldChange}
          valueChange={props.spotlightValueChange}
        />
        <FormattedMessage id="upload.no.dataflows" />
      </div>
    );
  }
  return (
    <div>
      <Spotlight
        {...props.spotlight}
        fieldChange={props.spotlightFieldChange}
        valueChange={props.spotlightValueChange}
      />
      <ListWrapper>
        <List handlers={props.handlers} itemRenderer={Dataflow} items={props.list.items} />
      </ListWrapper>
    </div>
  );
};

Dataflows.propTypes = {
  list: PropTypes.object,
  isFetching: PropTypes.bool,
  error: PropTypes.object,
  handlers: PropTypes.object,
};

export default compose(
  withDataflows,
  renameProps({ dataflows: 'items' }),
  withProps(({ toggleDataflow }) => ({
    handlers: {
      toggleDataflow,
    },
  })),
  withList({ isTree: false }),
  withSpotlight([
    { id: 'label', accessor: 'label', isSelected: true },
    { id: 'code', accessor: 'code', isSelected: true },
  ]),
)(Dataflows);
