import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Tag, Colors, Button } from '@blueprintjs/core';
import cx from 'classnames';
import Dropzone from 'react-dropzone';
import { FormattedMessage } from 'react-intl';
import { has, isEmpty, map } from 'lodash';
import numeral from 'numeral';
import glamorous from 'glamorous';
import Path from './path';

const FileStyled = glamorous(Tag)({
  margin: 5,
});

const Span = glamorous.span({
  color: 'red',
  fontWeight: 'bold',
  marginLeft: 5,
});

const File = ({ file, fileRemove, intentClass, maxSize }) => {
  const onRemoveHandler = event => {
    event.preventDefault();
    fileRemove(file);
  };

  return (
    <FileStyled
      className={cx(Classes.MINIMAL, Classes.LARGE, intentClass)}
      onRemove={onRemoveHandler}
    >
      <FormattedMessage
        id="file"
        values={{ name: file.name, size: numeral(file.size).format('0.00 b') }}
      />
      {intentClass === Classes.INTENT_DANGER && file.size > maxSize ? (
        <Span>
          <FormattedMessage
            id="upload.exceed.oversize.limit"
            values={{ size: numeral(maxSize).format('0.00 b') }}
          />
        </Span>
      ) : null}
    </FileStyled>
  );
};

File.propTypes = {
  file: PropTypes.object.isRequired,
  fileRemove: PropTypes.func.isRequired,
  intentClass: PropTypes.string,
  maxSize: PropTypes.number.isRequired,
};

const IntentedFiles = ({ files, fileRemove, acceptedFiles, maxSize }) => {
  if (isEmpty(files)) return null;
  const getIntentClass = file =>
    has(acceptedFiles, file.name) ? Classes.INTENT_SUCCESS : Classes.INTENT_DANGER;

  return (
    <div>
      {map(files, (file, index) => (
        <File
          key={`${file.name}:${index}`}
          file={file}
          fileRemove={fileRemove}
          intentClass={getIntentClass(file)}
          maxSize={maxSize}
        />
      ))}
    </div>
  );
};

IntentedFiles.propTypes = {
  acceptedFiles: PropTypes.object,
  files: PropTypes.array,
  fileRemove: PropTypes.func.isRequired,
  maxSize: PropTypes.number,
};

const NoFile = () => {
  return (
    <FileStyled className={cx(Classes.MINIMAL, Classes.LARGE, Classes.INTENT_WARNING)}>
      <FormattedMessage id="files.no.file" />
    </FileStyled>
  );
};

// glamorous and dropzone+ref are not compliant
const dropzoneStyle = {
  width: '100%',
  minHeight: 100,
  border: `1px dashed ${Colors.LIGHT_GRAY1}`,
  borderRadius: 5,
};

const FilesStyled = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const Files = ({
  dropzoneConfig,
  filesChange,
  fileRemove,
  files,
  acceptedFiles,
  rejectedFiles,
  hasNoFile,
  maxSize,
  hasPathOption,
}) => {
  let dropzoneRef; // mon dieu que c'est moche.
  return (
    <FilesStyled>
      <Button
        className={Classes.MINIMAL}
        iconName="add"
        onClick={() => {
          dropzoneRef.open();
        }}
      >
        <FormattedMessage id="upload.files.add" />
      </Button>
      <p className={Classes.TEXT_MUTED}>
        <em>
          <FormattedMessage id="upload.files.add.help" />
        </em>
      </p>
      <Dropzone
        {...dropzoneConfig}
        onDrop={files => filesChange(files)}
        ref={node => {
          dropzoneRef = node;
        }}
        style={dropzoneStyle}
      >
        {hasNoFile ? <NoFile /> : null}
        <IntentedFiles
          files={files}
          acceptedFiles={acceptedFiles}
          rejectedFiles={rejectedFiles}
          fileRemove={fileRemove}
          maxSize={maxSize}
        />
      </Dropzone>
      {hasPathOption ? <Path /> : null}
    </FilesStyled>
  );
};

Files.propTypes = {
  filesChange: PropTypes.func.isRequired,
  fileRemove: PropTypes.func.isRequired,
  files: PropTypes.array,
  acceptedFiles: PropTypes.object,
  rejectedFiles: PropTypes.object,
  hasNoFile: PropTypes.bool,
  maxSize: PropTypes.number,
  dropzoneConfig: PropTypes.shape({
    accept: PropTypes.string,
    multiple: PropTypes.bool,
    minSize: PropTypes.number,
    maxSize: PropTypes.number,
  }).isRequired,
  hasPathOption: PropTypes.bool,
};

export default Files;
