import React from 'react';
import PropTypes from 'prop-types';
import { Classes, Icon } from '@blueprintjs/core';
import { compose, onlyUpdateForKeys, renameProps } from 'recompose';
import cx from 'classnames';
import glamorous from 'glamorous';
import { isEmpty, map } from 'lodash';
import { withDataflows } from '../../modules/upload-data';

const Header = glamorous.div({
  display: 'flex',
  justifyContent: 'space-between',
});

const Log = ({ close, log }) => {
  if (isEmpty(log)) return null;

  return (
    <div>
      {map(log, spaceLog => {
        return (
          <div className={cx(Classes.CALLOUT, Classes.INTENT_DANGER)} key={spaceLog.space.id}>
            <Header>
              <h5>{spaceLog.space.label}</h5>
              <Icon
                iconName="cross"
                style={{ cursor: 'pointer' }}
                onClick={() => close(spaceLog.space.id)}
              />
            </Header>
            {spaceLog.error.message}
          </div>
        );
      })}
    </div>
  );
};

Log.propTypes = {
  close: PropTypes.func.isRequired,
  log: PropTypes.object.isRequired,
};

export default compose(
  withDataflows,
  renameProps({
    dataflowsError: 'log',
    removeDataflowsErrorLog: 'close',
  }),
  onlyUpdateForKeys(['log']),
)(Log);
