import React from 'react';
import PropTypes from 'prop-types';
import { Classes } from '@blueprintjs/core';
import cx from 'classnames';
import glamorous from 'glamorous';
import { FormattedMessage } from 'react-intl';
import { SUCCESS, ERROR, WARNING } from '../../modules/upload';
import { isArray, map } from 'lodash';

const MyLog = glamorous.div({
  marginBottom: 10,
});

const Log = ({ type, message, error, scope, space }) => {
  let iconClass, intentClass, messageKey;
  switch (type) {
    case SUCCESS:
      iconClass = Classes.iconClass('tick-circle');
      intentClass = Classes.INTENT_SUCCESS;
      messageKey = `upload.${scope}.success`;
      break;
    case WARNING:
      iconClass = Classes.iconClass('warning-sign');
      intentClass = Classes.INTENT_WARNING;
      messageKey = `upload.${scope}.warning`;
      break;
    case ERROR:
      iconClass = Classes.iconClass('error');
      intentClass = Classes.INTENT_DANGER;
      messageKey = `upload.${scope}.error`;
      break;
    default:
      break;
  }

  return (
    <MyLog>
      <div className={cx(Classes.CALLOUT, intentClass, iconClass)}>
        <h5>
          <FormattedMessage id={messageKey} values={{ label: space.label }} />
        </h5>
        {isArray(message) ? map(message, (m, i) => <p key={i}>{m}</p>) : <p>{message}</p>}
        {error ? <p>{error.message}</p> : null}
      </div>
    </MyLog>
  );
};

Log.propTypes = {
  type: PropTypes.string.isRequired,
  message: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
  scope: PropTypes.string,
  space: PropTypes.shape({
    label: PropTypes.string,
  }),
  error: PropTypes.shape({ message: PropTypes.string }),
};

export default Log;
