import React from 'react';
import PropTypes from 'prop-types';
import Header from '../common/header';
import Spaces from '../common/spaces';
import Actions from '../common/actions';
import Files from './files';
import Log from './log';
import { map, pick } from 'lodash';
import glamorous from 'glamorous';
import cx from 'classnames';
import { Classes, Intent } from '@blueprintjs/core';
import { FormattedMessage } from 'react-intl';
import Dataflows from './dataflows';
import DataflowsLog from './dataflowslog';

const Container = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const Item = glamorous.div({
  width: 500,
  margin: 10,
  padding: '0 !important',
});

const ItemContent = glamorous.div({
  padding: 12,
});

const Upload = props => {
  const filesProps = pick(props, [
    'dropzoneConfig',
    'filesChange',
    'fileRemove',
    'files',
    'acceptedFiles',
    'rejectedFiles',
    'refinedRejectedFiles',
    'hasNoFile',
    'maxSize',
    'byPath',
    'filepath',
    'changeFilePath',
    'toggleByPath',
    'hasPathOption',
  ]);
  const spacesProps = pick(props, ['spaces', 'selectSpace', 'removeSpace']);
  const actions = [
    {
      iconName: 'upload',
      id: 'upload',
      disabled: !props.formIsValid,
      loading: props.isUploading,
      onClick: props.upload,
    },
    {
      iconName: 'delete',
      id: 'upload.reset',
      intent: Intent.DANGER,
      onClick: props.formReset,
    },
  ];
  return (
    <Container>
      <Item>
        <Header {...props.headerProps} />
      </Item>
      <Item className={cx(Classes.CARD, Classes.INTERACTIVE)}>
        <div className={cx(Classes.CALLOUT, Classes.iconClass('duplicate'))}>
          <FormattedMessage id="upload.files" />
        </div>
        <ItemContent>
          <Files {...filesProps} />
        </ItemContent>
      </Item>
      <Item className={cx(Classes.CARD, Classes.INTERACTIVE)}>
        <div className={cx(Classes.CALLOUT, Classes.iconClass('database'))}>
          <FormattedMessage id="upload.spaces" />
        </div>
        <ItemContent>
          <Spaces {...spacesProps} />
        </ItemContent>
      </Item>
      <Item>
        <DataflowsLog />
      </Item>
      {props.hasDataflowSelection && (
        <Item className={cx(Classes.CARD, Classes.INTERACTIVE)}>
          <div className={cx(Classes.CALLOUT, Classes.iconClass('import'))}>
            <FormattedMessage id="upload.dataflows" />
          </div>
          <ItemContent>
            <Dataflows />
          </ItemContent>
        </Item>
      )}
      <Item>
        <Actions actions={actions} />
      </Item>
      <Item>
        {map(props.log, log => (
          <Log {...log} scope={props.scope} key={log.space.id} />
        ))}
      </Item>
    </Container>
  );
};

Upload.propTypes = {
  headerProps: PropTypes.object,
  fileRemove: PropTypes.func.isRequired,
  filesChange: PropTypes.func.isRequired,
  files: PropTypes.array,
  acceptedFiles: PropTypes.object,
  rejectedFiles: PropTypes.object,
  hasNoFile: PropTypes.bool,
  selectSpace: PropTypes.func.isRequired,
  removeSpace: PropTypes.func.isRequired,
  scope: PropTypes.string,
  spaces: PropTypes.object,
  upload: PropTypes.func.isRequired,
  dropzoneConfig: PropTypes.object.isRequired,
  formIsValid: PropTypes.bool,
  formReset: PropTypes.func.isRequired,
  isUploading: PropTypes.bool,
  log: PropTypes.object,
  hasDataflowsSelection: PropTypes.bool,
  maxSize: PropTypes.number,
};

export default Upload;
