import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Intent, Icon, Tooltip, Position } from '@blueprintjs/core';
import { get, isArray, isEmpty, map } from 'lodash';
import { injectIntl } from 'react-intl';
import { onlyUpdateForKeys, compose, mapProps } from 'recompose';
import { ERROR } from '../../modules/transfer';
import { getTranslations } from '../../modules/i18n/utils';

const getTranslationsWithFallback = getTranslations([
  { id: 'transfer.data' },
  { id: 'transfer.artefact' },
]);

const IconStyled = glamorous(Icon)({ marginLeft: 4, marginRight: 4 });

const Messages = glamorous.div({ display: 'flex', flexDirection: 'column' });

const TransferLog = ({ log, t, intl }) => {
  const type = get(log, 'type');
  if (!type) return null;
  const messages = isArray(log.message) ? log.message : [log.message];

  const intent = Intent[type === ERROR ? 'DANGER' : type];

  return (
    <Tooltip
      intent={intent}
      content={
        <div>
          <span>{intl.formatMessage(...getTranslationsWithFallback(`transfer.${t}`))}</span>
          {isEmpty(messages) ? null : (
            <Messages>
              {map(messages, (message, ind) => (
                <div key={ind}>{message}</div>
              ))}
            </Messages>
          )}
        </div>
      }
      inheritDarkTheme={false}
      position={Position.LEFT}
    >
      <IconStyled iconName="swap-horizontal" intent={intent} />
    </Tooltip>
  );
};

TransferLog.propTypes = {
  log: PropTypes.object,
  t: PropTypes.string,
  intl: PropTypes.object.isRequired,
};

export default (withTransfer, options = {}) => artefactId =>
  compose(
    withTransfer(artefactId),
    onlyUpdateForKeys(['log']),
    mapProps(({ log }) => ({ log, ...options })),
    injectIntl,
  )(TransferLog);
