import React from 'react';
import PropTypes from 'prop-types';
import { withArtefacts } from '../../modules/artefacts';
import {
  onlyUpdateForKeys,
  renameProps,
  compose,
  branch,
  renderComponent,
  renderNothing,
  withProps,
  mapProps,
} from 'recompose';
import { isEmpty } from 'lodash';
import { Button, Classes, NonIdealState, Spinner } from '@blueprintjs/core';
import classnames from 'classnames';
import glamorous from 'glamorous';
import { FormattedMessage } from 'react-intl';
import {
  List,
  withList,
  Spotlight,
  withSpotlight,
  Pagination,
  PaginationStatus,
  withPagination,
  Status,
  withStatus,
  Sort,
  withSort,
} from '../list';
import TransferDialog from './transfer-dialog';
import { Item } from './item';
import { SelectionActions } from './actions';
import { withSelection } from '../../modules/selection';
import { withSelection as withFiltersSelection } from '../../modules/filters';

const Wrapper = glamorous.div({
  display: 'flex',
  justifyContent: 'center',
});

const StyledNav = glamorous.nav({
  padding: '0 !important',
  boxShadow: 'none !important',
  zIndex: '0 !important',
  backgroundColor: 'transparent  !important',
});

const StyledList = glamorous.nav({
  borderBottom: '1px solid #ccc',
  marginBottom: 10,
  paddingBottom: 10,
});

export const ArtefactsSpinner = () => {
  return (
    <Wrapper>
      <Spinner className={Classes.LARGE} />
    </Wrapper>
  );
};

export const ArtefactsBlank = ({ noSpaces, noTypes }) => {
  let messageId = 'artefacts.no.artefact';
  if (noSpaces && noTypes) {
    messageId = 'artefacts.no.selection';
  } else if (noSpaces || noTypes) {
    messageId = noSpaces ? 'artefacts.no.spaces' : 'artefacts.no.types';
  }
  return (
    <Wrapper>
      <NonIdealState visual="square" title={<FormattedMessage id={messageId} />} />
    </Wrapper>
  );
};

ArtefactsBlank.propTypes = {
  noSpaces: PropTypes.bool,
  noTypes: PropTypes.bool,
};

const withBlank = branch(({ items }) => isEmpty(items), renderComponent(ArtefactsBlank));
const ArtefactsList = compose(withBlank)(List);

const PaginationBlock = compose(branch(props => props.pagination.pages < 2, renderNothing))(
  props => {
    return (
      <StyledNav className={classnames(Classes.NAVBAR)}>
        <div className={classnames(Classes.NAVBAR_GROUP, Classes.ALIGN_LEFT)}>
          <Pagination {...props.pagination} pageChange={props.paginationPageChange} />
        </div>
        <div className={classnames(Classes.NAVBAR_GROUP, Classes.ALIGN_RIGHT)}>
          <PaginationStatus pages={props.pagination.pages} />
        </div>
      </StyledNav>
    );
  },
);

export const Artefacts = props => {
  return (
    <div>
      <StyledNav className={classnames(Classes.NAVBAR)}>
        <div className={classnames(Classes.NAVBAR_GROUP, Classes.ALIGN_LEFT)}>
          <Spotlight
            {...props.spotlight}
            fieldChange={props.spotlightFieldChange}
            valueChange={props.spotlightValueChange}
          />
          <span className={Classes.NAVBAR_DIVIDER}></span>
          <Sort {...props.sort} fieldChange={props.sortFieldChange} />
        </div>
        <div className={classnames(Classes.NAVBAR_GROUP, Classes.ALIGN_RIGHT)}>
          <Status {...props.status} isFetching={props.isFetching} />
        </div>
        <TransferDialog />
      </StyledNav>
      <div>
        <PaginationBlock {...props} />
        {props.hasSelection && (
          <div>
            <SelectionActions
              artefactId={props.selection}
              artefactDelete={props.deleteArtefacts}
              artefactExport={props.exportArtefacts}
              dataExport={props.exportDatas}
              hasData={props.isSelectionHasData}
              isDeletable={props.isSelectionDeletable}
            />
            <Button
              className={Classes.MINIMAL}
              text={<FormattedMessage id="selection.reset" />}
              iconName="delete"
              onClick={props.resetSelection}
            />
          </div>
        )}
        <StyledList>
          <ArtefactsList items={props.list.items} itemRenderer={Item} handlers={props.handlers} />
        </StyledList>
        <PaginationBlock {...props} />
      </div>
    </div>
  );
};

const spotlightFields = [
  { id: 'label', accessor: 'label', isSelected: true },
  { id: 'code', accessor: 'code', isSelected: true },
  { id: 'description', accessor: 'description', isSelected: true },
];

const sortFields = [
  // accessor is a only string for now
  { id: 'type', accessor: 'type', isSelected: true, index: 0, direction: 'asc' },
  { id: 'label', accessor: 'label', isSelected: true, index: 1, direction: 'asc' },
  { id: 'code', accessor: 'code' },
  { id: 'version', accessor: 'version' },
  { id: 'is.final', accessor: 'isFinal' },
  { id: 'owner', accessor: 'agencyId' },
];

export default compose(
  withArtefacts,
  withSelection,
  onlyUpdateForKeys([
    'isFetching',
    'artefacts',
    'hasSelection',
    'isSelectionDeletable',
    'isSelectionHasData',
  ]),
  renameProps({ artefacts: 'items' }),
  branch(
    ({ isFetching, items }) => isFetching && isEmpty(items),
    renderComponent(ArtefactsSpinner),
  ),
  withFiltersSelection,
  mapProps(({ selectedFiltersValues, ...rest }) => ({
    ...rest,
    noSpaces: isEmpty(selectedFiltersValues.spaces),
    noTypes: isEmpty(selectedFiltersValues.types),
  })),
  withBlank,
  withProps(({ artefactDelete, artefactExportStructure, fetchDataflowDetails }) => ({
    handlers: {
      artefactDelete,
      artefactExportStructure,
      fetchDataflowDetails,
    },
  })),
  withList(),
  withSpotlight(spotlightFields),
  withStatus, // a composition is ordered
  withSort(sortFields),
  withPagination({ pageSize: 12, pageBuffer: 3 }),
)(Artefacts);
