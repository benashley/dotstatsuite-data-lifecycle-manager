import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { isNull, isNil } from 'lodash';
import { Checkbox, Classes, Colors, Icon, Spinner } from '@blueprintjs/core';
import { FormattedMessage } from 'react-intl';
import Space from '../common/space';
import { ItemActions } from './actions';
import ItemLogs from './item-logs';
import MoreButton from './more-button';
import { DataflowDetails } from './dataflow-details';
import { compose, mapProps } from 'recompose';
import { withTransferArtefact } from '../../modules/transfer-artefact';
import { withTransferData } from '../../modules/transfer-data';
import { withExportData } from '../../modules/export-data';
import { withSelectedState } from '../../modules/selection';
import { withLocale } from '../../modules/i18n';
import { withDataflowDetails } from '../../modules/dataflow-details';

const StyledCheckbox = glamorous(Checkbox)({
  marginBottom: '0 !important',
});

const Container = glamorous.div({
  marginTop: 10,
  paddingTop: 10,
  borderTop: '1px solid #ccc',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
});

const ItemStyled = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  width: '100%',
});

const StyledPending = glamorous.div({
  marginTop: 10,
  paddingTop: 10,
  borderTop: '1px solid #ccc',
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'center',
});

const Resume = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
  justifyContent: 'space-between',
});

const Interface = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
});

//--------------------------------------------------------------------------------------------------
const IsFinal = ({ isFinal }) => {
  if (!isFinal) return null;

  return (
    <span>
      <span className={Classes.NAVBAR_DIVIDER}></span>
      <Icon iconName="tick" />
      <glamorous.Span marginLeft={5}>
        <FormattedMessage id="artefact.is.final" />
      </glamorous.Span>
    </span>
  );
};

IsFinal.propTypes = {
  isFinal: PropTypes.bool,
};

//--------------------------------------------------------------------------------------------------

const IconStyled = glamorous(Icon)({ marginLeft: 10, marginRight: 10 });
const SpinnerStyled = glamorous(Spinner)({ marginRight: 10 });

//--------------------------------------------------------------------------------------------------
const ItemPending = ({ iconName, t, children }) => {
  return (
    <StyledPending>
      <SpinnerStyled />
      {iconName ? <Icon iconName={iconName} style={{ color: Colors.GRAY1 }} /> : null}
      <glamorous.Span marginLeft={10}>
        <FormattedMessage id={`artefact.is.${t}`} />
      </glamorous.Span>
      {children}
    </StyledPending>
  );
};

ItemPending.propTypes = {
  iconName: PropTypes.string,
  t: PropTypes.string,
  children: PropTypes.array,
};

//--------------------------------------------------------------------------------------------------
const ItemView = props => {
  const {
    id,
    isDeletable,
    isExportingData,
    label,
    type,
    code,
    version,
    isFinal,
    agencyId,
    space,
    isNew,
    error,
    handlers,
    isSelectable,
    isSelected,
    link,
    hasData,
    dataExplorerUrl,
    requestDetails,
    categories,
    observations,
    isDetailsExpanded,
    deleteDetails,
  } = props;

  const transferArtefact = props.transferArtefact;

  if (transferArtefact.isUpdating) {
    return <ItemPending t="updating" iconName="add-to-artifact" />;
  }

  if (isExportingData) {
    return <ItemPending t="exporting.data" iconName="download" />;
  }

  const transferData = props.transferData;
  if (transferData.isTransfering) {
    return (
      <ItemPending t="transfering.data">
        <IconStyled iconName="arrow-right" style={{ color: Colors.GRAY1 }} />
        <Space {...transferData.destinationSpace} />
      </ItemPending>
    );
  }

  return (
    <Container>
      <StyledCheckbox
        checked={isSelected}
        disabled={!isSelectable}
        onChange={() => props.toggleArtefact(id)}
      />
      <ItemStyled>
        <Resume>
          <glamorous.Div>
            <glamorous.Div marginBottom={10}>
              <Space {...space} label={<FormattedMessage id={`artefact.type.${type}`} />} />
              <glamorous.Span marginLeft={10}>
                <a href={link} target="_blank" rel="noopener noreferrer">
                  {isNull(label) ? <FormattedMessage id="artefact.no.label" /> : label}
                </a>
              </glamorous.Span>
            </glamorous.Div>
            <div>
              <span>[{code}]</span>
              <span className={Classes.NAVBAR_DIVIDER}></span>
              <span>[{version}]</span>
              <IsFinal isFinal={isFinal} />
              <span className={Classes.NAVBAR_DIVIDER}></span>
              <span>{agencyId}</span>
              {hasData ? <span className={Classes.NAVBAR_DIVIDER}></span> : null}
              <MoreButton
                id={id}
                isActive={isDetailsExpanded}
                request={isDetailsExpanded ? deleteDetails : requestDetails}
              />
            </div>
          </glamorous.Div>
          <Interface>
            {hasData && !isNil(dataExplorerUrl) ? (
              <a href={dataExplorerUrl} target="_blank" rel="noopener noreferrer">
                <Icon
                  iconName="eye-open"
                  style={{
                    color: Colors.GRAY1,
                    marginLeft: 10,
                    marginRight: 10,
                  }}
                />
              </a>
            ) : null}
            <ItemLogs artefactId={id} isNew={isNew} error={error} />
            <ItemActions
              artefactId={id}
              artefactDelete={handlers.artefactDelete}
              artefactExport={handlers.artefactExportStructure}
              dataExport={props.exportData}
              hasData={hasData}
              isDeletable={isDeletable}
            />
          </Interface>
        </Resume>
        <DataflowDetails
          categories={categories}
          id={id}
          isExpanded={isDetailsExpanded}
          observations={observations}
          requestDetails={requestDetails}
        />
      </ItemStyled>
    </Container>
  );
};

ItemView.propTypes = {
  dataExplorerUrl: PropTypes.string,
  hasData: PropTypes.bool,
  locale: PropTypes.string,
  id: PropTypes.string.isRequired,
  isDeletable: PropTypes.bool,
  isExportingData: PropTypes.bool,
  exportData: PropTypes.func,
  link: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  code: PropTypes.string,
  version: PropTypes.string,
  isFinal: PropTypes.bool,
  agencyId: PropTypes.string,
  isNew: PropTypes.bool,
  error: PropTypes.object,
  handlers: PropTypes.object,
  space: PropTypes.shape({
    color: PropTypes.string,
  }).isRequired,
  isSelectable: PropTypes.bool,
  isSelected: PropTypes.bool,
  categorisations: PropTypes.object,
};

//--------------------------------------------------------------------------------------------------
const itemProxy = id =>
  compose(
    withLocale,
    withSelectedState(id),
    withTransferArtefact(id),
    mapProps(({ agencyId, code, error, space, type, version, locale, ...rest }) => ({
      ...rest,
      agencyId,
      artefactError: error,
      code,
      hasData: type === 'dataflow',
      dataExplorerUrl:
        space.dataExplorerUrl && !isNil(space.dataExplorerUrl)
          ? `${space.dataExplorerUrl}/vis?locale=${locale}&dataflow[datasourceId]=${space.id}&dataflow[agencyId]=${agencyId}&dataflow[dataflowId]=${code}&dataflow[version]=${version}&withoutContentConstraints=true`
          : null,
      isDeletable: !space.isExternal,
      link: `${space.endpoint}/${type}/${agencyId}/${code}/${version}`,
      space,
      type,
      version,
    })),
    mapProps(({ isTransfering, isUpdating, destinationSpace, ...rest }) => ({
      ...rest,
      transferArtefact: { isTransfering, isUpdating, destinationSpace },
    })),
    withTransferData(id),
    mapProps(({ isTransfering, isUpdating, destinationSpace, ...rest }) => ({
      ...rest,
      transferData: { isTransfering, isUpdating, destinationSpace },
    })),
    withExportData(id),
    mapProps(({ artefactError, error, ...rest }) => ({
      ...rest,
      error: artefactError ? artefactError : error,
    })),
    withDataflowDetails(id),
  )(ItemView);

//--------------------------------------------------------------------------------------------------
export const Item = ({ id, isDeleting, isExporting, ...rest }) => {
  if (isDeleting) return <ItemPending t="deleting" iconName="trash" />;
  if (isExporting) return <ItemPending t="exporting.structure" iconName="download" />;
  const ItemProxy = itemProxy(id);

  return <ItemProxy id={id} {...rest} />;
};

Item.propTypes = {
  id: PropTypes.string.isRequired,
  isDeleting: PropTypes.bool,
  isExporting: PropTypes.bool,
};
