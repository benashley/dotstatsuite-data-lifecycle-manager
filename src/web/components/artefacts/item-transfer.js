import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { FormattedMessage } from 'react-intl';
import { map } from 'lodash';
import { Classes, Menu, MenuItem } from '@blueprintjs/core';
import { branch, compose, mapProps, onlyUpdateForKeys, renderComponent } from 'recompose';
import MenuReferencesSelector from './refs-selector';

//--------------------------------------------------------------------------------------------------
const SpaceMenuItem = ({ label, color, backgroundColor, onTransfer, disabled }) => {
  const StyledMenuItem = glamorous(MenuItem)({
    ':hover': {
      color: `${color} !important`,
      backgroundColor: `${backgroundColor} !important`,
    },
  });

  return <StyledMenuItem disabled={disabled} text={label} onClick={onTransfer} />;
};

SpaceMenuItem.propTypes = {
  label: PropTypes.string.isRequired,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  disabled: PropTypes.bool,
  onTransfer: PropTypes.func,
};

SpaceMenuItem.defaultProps = {
  color: 'white', // '#182026'
  backgroundColor: '#EE6290', // '#0965C1' '#e8ebee'
  disabled: false,
};

//--------------------------------------------------------------------------------------------------
const MenuHeader = ({ translationKey }) => {
  return (
    <li className={Classes.MENU_HEADER}>
      <h6>
        <FormattedMessage id={translationKey} />
      </h6>
    </li>
  );
};

MenuHeader.propTypes = {
  translationKey: PropTypes.string,
};

//--------------------------------------------------------------------------------------------------
const MenuTransfer = ({ space, spaces, onTransfer, withReferences }) => {
  return (
    <Menu>
      <MenuHeader translationKey="transfer.source" />
      <SpaceMenuItem {...space} disabled />
      <MenuHeader translationKey="transfer.destination" />
      {map(spaces, space => (
        <SpaceMenuItem {...space} key={space.id} onTransfer={onTransfer(space, withReferences)} />
      ))}
    </Menu>
  );
};

MenuTransfer.propTypes = {
  space: PropTypes.object.isRequired,
  spaces: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
    }).isRequired,
  ),
  onTransfer: PropTypes.func.isRequired,
  withReferences: PropTypes.string,
};

//--------------------------------------------------------------------------------------------------
const MenuReferencesTransfer = props => {
  return (
    <MenuReferencesSelector>
      <MenuTransfer {...props} />
    </MenuReferencesSelector>
  );
};

MenuReferencesTransfer.propTypes = MenuTransfer.propTypes;

//--------------------------------------------------------------------------------------------------
export default (withTransfer, options = {}) => artefactId =>
  compose(
    withTransfer(artefactId),
    onlyUpdateForKeys(['destinationSpaces', 'sourceSpace', 'transfer']),
    mapProps(({ destinationSpaces, hasReferencesSelection, sourceSpace, transfer }) => ({
      spaces: destinationSpaces,
      space: sourceSpace,
      onTransfer: (destinationSpace, withReferences) => () =>
        transfer(artefactId, destinationSpace, withReferences),
      hasReferencesSelection,
      ...options,
    })),
    branch(
      ({ hasReferencesSelection }) => hasReferencesSelection,
      renderComponent(MenuReferencesTransfer),
    ),
  )(MenuTransfer);
