import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Intent, Icon } from '@blueprintjs/core';
import { withTransferArtefact } from '../../modules/transfer-artefact';
import { withTransferData } from '../../modules/transfer-data';
import itemTransferLogs from './item-transfer-logs';

const IconStyled = glamorous(Icon)({ marginLeft: 4, marginRight: 4 });

const itemTransferLogArtefact = itemTransferLogs(withTransferArtefact, { t: 'artefact' });
const itemTransferLogData = itemTransferLogs(withTransferData, { t: 'data' });

const ItemLogs = ({ artefactId, isNew, error }) => {
  const ItemTransferLogArtefact = itemTransferLogArtefact(artefactId);
  const ItemTransferLogData = itemTransferLogData(artefactId);

  return (
    <div>
      {isNew ? <IconStyled iconName="add-to-artifact" intent={Intent.PRIMARY} /> : null}
      {error && error.type === 'delete' ? (
        <IconStyled iconName="trash" intent={Intent.DANGER} />
      ) : null}
      {error && error.type === 'export' ? (
        <IconStyled iconName="download" intent={Intent.DANGER} />
      ) : null}
      {error && error.type === 'fetch' ? (
        <IconStyled iconName="import" intent={Intent.WARNING} />
      ) : null}
      <ItemTransferLogArtefact />
      <ItemTransferLogData />
    </div>
  );
};

ItemLogs.propTypes = {
  artefactId: PropTypes.string.isRequired,
  isNew: PropTypes.bool,
  error: PropTypes.object,
};

export default ItemLogs;
