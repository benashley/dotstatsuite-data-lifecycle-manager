import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Menu, MenuItem } from '@blueprintjs/core';

const MenuReferencesSelector = ({ children, onSelection }) => {
  if (React.Children.count(children) === 0) {
    return (
      <Menu>
        <MenuItem
          text={<FormattedMessage id="artefact.without.refs.selector" />}
          onClick={() => onSelection(null)}
        />
        <MenuItem
          text={<FormattedMessage id="artefact.withall.refs.selector" />}
          onClick={() => onSelection('all')}
        />
      </Menu>
    );
  }
  const child = React.Children.only(children);
  return (
    <Menu>
      <MenuItem text={<FormattedMessage id="artefact.without.refs.selector" />}>
        {React.cloneElement(child, { withReferences: null })}
      </MenuItem>
      <MenuItem text={<FormattedMessage id="artefact.withall.refs.selector" />}>
        {React.cloneElement(child, { withReferences: 'all' })}
      </MenuItem>
    </Menu>
  );
};

MenuReferencesSelector.propTypes = {
  children: PropTypes.node,
  onSelection: PropTypes.func,
};

export default MenuReferencesSelector;
