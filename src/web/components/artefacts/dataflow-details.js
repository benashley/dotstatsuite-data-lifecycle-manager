import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Button, Classes, Colors, Spinner } from '@blueprintjs/core';
import glamorous from 'glamorous';
import { isEmpty, isNil, join, map } from 'lodash';

const Container = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
});

const RightEntry = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
});

const withSpinner = Component => ({ isFetching, ...rest }) =>
  isFetching ? <Spinner className={Classes.SMALL} /> : <Component {...rest} />;

const Observations = ({ observations }) => (
  <div>
    {isNil(observations) ? (
      <FormattedMessage id="artefact.unknown.observations" />
    ) : (
      <FormattedMessage id="artefact.observations" values={{ observations }} />
    )}
  </div>
);

Observations.propTypes = {
  observations: PropTypes.number,
};

const toLabels = entries => map(entries, ({ id, label }) => (label ? label : id));

const Categories = ({ categories }) => {
  if (isEmpty(categories) || isNil(categories)) {
    return (
      <i>
        <FormattedMessage id="dataflow.no.categories" />
      </i>
    );
  }
  return (
    <div>
      <b>
        <i>
          <FormattedMessage id="dataflow.categories" /> :{' '}
        </i>
      </b>
      {join(map(categories, hierarchy => join(toLabels(hierarchy), ' > ')), '; ')}
    </div>
  );
};

Categories.propTypes = {
  categories: PropTypes.array,
};

export const DataflowDetails = ({ categories, id, isExpanded, observations, requestDetails }) => {
  if (!isExpanded) {
    return null;
  }
  return (
    <Container>
      {withSpinner(Categories)(categories)}
      <RightEntry>
        {withSpinner(Observations)(observations)}
        <Button
          className={Classes.MINIMAL}
          iconName="refresh"
          style={{
            color: Colors.GRAY1,
          }}
          onClick={() => requestDetails(id)}
        />
      </RightEntry>
    </Container>
  );
};

DataflowDetails.propTypes = {
  categories: PropTypes.object,
  id: PropTypes.string,
  isExpanded: PropTypes.bool,
  observations: PropTypes.object,
  requestDetails: PropTypes.func.isRequired,
};
