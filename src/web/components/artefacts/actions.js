import React from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Classes, Button, Popover, Position, Menu, MenuItem } from '@blueprintjs/core';
import { connect } from 'react-redux';
import { compose, defaultProps } from 'recompose';
import { withTransferArtefact } from '../../modules/transfer-artefact';
import { withSelectionArtefactsTransfer } from '../../modules/selection';
import itemTransfer from './item-transfer';
import MenuReferencesSelector from './refs-selector';
import { openItemTransfer, openSelectionTransfer } from '../../modules/transfer-control';

const MenuActions = props => {
  const {
    artefactId,
    artefactDelete,
    artefactExport,
    dataExport,
    dataTransfer,
    hasData,
    isDeletable,
    menuArtefactTransfer,
    translations,
  } = props;

  const {
    exportArtefactId,
    exportDataId,
    deleteArtefactId,
    transferArtefactId,
    transferDataId,
  } = translations;

  const MenuArtefactTransfer = menuArtefactTransfer(artefactId);

  return (
    <Menu>
      {menuArtefactTransfer && (
        <MenuItem text={<FormattedMessage id={transferArtefactId} />} iconName="swap-horizontal">
          <MenuArtefactTransfer hasReferencesSelection />
        </MenuItem>
      )}
      {hasData && (
        <MenuItem
          iconName="swap-horizontal"
          onClick={() => dataTransfer(artefactId)}
          text={<FormattedMessage id={transferDataId} />}
        />
      )}
      {isDeletable && (
        <MenuItem
          iconName="trash"
          onClick={() => artefactDelete(artefactId)}
          text={<FormattedMessage id={deleteArtefactId} />}
        />
      )}
      <MenuItem text={<FormattedMessage id={exportArtefactId} />} iconName="download">
        <MenuReferencesSelector
          onSelection={withReferences => artefactExport(artefactId, withReferences)}
        />
      </MenuItem>
      {hasData && (
        <MenuItem text={<FormattedMessage id={exportDataId} />} iconName="download">
          <MenuItem
            text={<FormattedMessage id="export.data.xml" />}
            onClick={() => dataExport(artefactId, 'xml')}
          />
          <MenuItem
            text={<FormattedMessage id="export.data.csv" />}
            onClick={() => dataExport(artefactId, 'csv')}
          />
        </MenuItem>
      )}
    </Menu>
  );
};

MenuActions.propTypes = {
  artefactId: PropTypes.oneOfType([PropTypes.string, PropTypes.object]).isRequired,
  artefactDelete: PropTypes.func.isRequired,
  artefactExport: PropTypes.func.isRequired,
  dataExport: PropTypes.func.isRequired,
  dataTransfer: PropTypes.func.isRequired,
  hasData: PropTypes.bool,
  isDeletable: PropTypes.bool,
  menuArtefactTransfer: PropTypes.func,
  translations: PropTypes.shape({
    exportArtefactId: PropTypes.string,
    exportDataId: PropTypes.string,
    deleteArtefactId: PropTypes.string,
    transferArtefactId: PropTypes.string,
    transferDataId: PropTypes.string,
  }),
};

//--------------------------------------------------------------------------------------------------
const Actions = props => {
  return (
    <Popover content={<MenuActions {...props} />} position={Position.LEFT} inline>
      <div>
        <Button iconName="menu" className={Classes.MINIMAL} />
      </div>
    </Popover>
  );
};

Actions.propTypes = MenuActions.propTypes;

export const SelectionActions = compose(
  connect(
    () => ({}),
    { dataTransfer: openSelectionTransfer('data') },
  ),
  defaultProps({
    menuArtefactTransfer: itemTransfer(withSelectionArtefactsTransfer, {
      translationScope: 'artefacts',
    }),
    menuUploadData: null,
    translations: {
      exportArtefactId: 'export.artefacts',
      exportDataId: 'export.datas',
      deleteArtefactId: 'artefacts.delete',
      transferArtefactId: 'transfer.artefacts',
      transferDataId: 'transfer.datas',
    },
  }),
)(Actions);

export const ItemActions = compose(
  connect(
    () => ({}),
    { dataTransfer: openItemTransfer('data') },
  ),
  defaultProps({
    menuArtefactTransfer: itemTransfer(withTransferArtefact, { translationScope: 'artefact' }),
    translations: {
      exportArtefactId: 'export.artefact',
      exportDataId: 'export.data',
      deleteArtefactId: 'artefact.delete',
      transferArtefactId: 'transfer.artefact',
      transferDataId: 'transfer.data',
    },
  }),
)(Actions);
