import React from 'react';
import PropTypes from 'prop-types';
import { Button, Checkbox, Classes, Dialog, EditableText } from '@blueprintjs/core';
import { compose, withProps } from 'recompose';
import glamorous from 'glamorous';
import { injectIntl } from 'react-intl';
import { find, get, isNil, keys, map, pick, reject, size } from 'lodash';
import Space from '../common/space';
import { withTransferControl } from '../../modules/transfer-control';
import { getTranslations } from '../../modules/i18n/utils';

const getTranslationsWithFallback = getTranslations([
  { id: 'transfer.destination' },
  { id: 'transfer.dataquery' },
  { id: 'transfer.dataquery.info' },
  { id: 'transfer.dataquery.placeholder' },
  { id: 'transfer.apply' },
  { id: 'transfer.cancel' },
  { id: 'transfer.artefact.type' },
  { id: 'transfer.artefact.id' },
  { id: 'transfer.artefact.name' },
  { id: 'transfer.artefact.source' },
  { id: 'artefact.type.agencyscheme' },
  { id: 'artefact.type.attachmentconstraint' },
  { id: 'artefact.type.categoryscheme' },
  { id: 'artefact.type.categorisation' },
  { id: 'artefact.type.codelist' },
  { id: 'artefact.type.conceptscheme' },
  { id: 'artefact.type.contentconstraint' },
  { id: 'artefact.type.dataconsumerscheme' },
  { id: 'artefact.type.dataproviderscheme' },
  { id: 'artefact.type.datastructure' },
  { id: 'artefact.type.dataflow' },
  { id: 'artefact.type.hierarchicalcodelist' },
  { id: 'artefact.type.metadatastructure' },
  { id: 'artefact.type.metadataflow' },
  { id: 'artefact.type.organisationscheme' },
  { id: 'artefact.type.organisationunitscheme' },
  { id: 'artefact.type.process' },
  { id: 'artefact.type.provisionagreement' },
  { id: 'artefact.type.reportingtaxonomy' },
  { id: 'artefact.type.structureset' },
  { id: 'transfer.artefact' },
  { id: 'transfer.artefacts' },
  { id: 'transfer.data' },
  { id: 'transfer.datas' },
]);

const GDialog = glamorous(Dialog)({ width: 'auto !important' });

const SpaceOption = glamorous.div({
  border: '1px solid black',
  boxSizing: 'border-box',
  padding: 10,
  alignItems: 'center',
  display: 'flex',
  flexDirection: 'row',
  marginRight: 15,
});

const Container = glamorous.div({
  marginBottom: 20,
});

const Spaces = glamorous.div({
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'flex-start',
});

const StyledCheckbox = glamorous(Checkbox)({
  lineHeight: '20px !important',
  marginBottom: '0px !important',
  minHeight: '20px !important',
  '& .pt-control-indicator': {
    top: '2px !important',
  },
});

const GTh = glamorous.th({ textAlign: 'center !important' });
const GTd = glamorous.td({ textAlign: 'center !important' });

const Input = glamorous(EditableText)({
  backgroundColor: 'white',
  minWidth: 100,
  height: 20,
});

const InputContainer = glamorous.div(
  {
    display: 'flex',
    flexDirection: 'column',
  },
  ({ hasDataQuery }) => {
    if (!hasDataQuery) {
      return { display: 'none' };
    }
    return {};
  },
);

const QueryInfo = glamorous.span({
  marginBottom: 10,
  marginTop: 10,
});

const Items = ({ intl, artefacts }) => (
  <table className={`${Classes.TABLE} ${Classes.TABLE_CONDENSED}`}>
    <thead>
      <tr>
        <GTh>
          <span>
            {intl.formatMessage(...getTranslationsWithFallback('transfer.artefact.type'))}
          </span>
        </GTh>
        <GTh>
          <span>{intl.formatMessage(...getTranslationsWithFallback('transfer.artefact.id'))}</span>
        </GTh>
        <GTh>
          <span>
            {intl.formatMessage(...getTranslationsWithFallback('transfer.artefact.name'))}
          </span>
        </GTh>
        <GTh>
          <span>
            {intl.formatMessage(...getTranslationsWithFallback('transfer.artefact.source'))}
          </span>
        </GTh>
      </tr>
    </thead>
    <tbody>
      {map(artefacts, artefact => {
        const space = get(artefact, 'space', {});
        const type = get(artefact, 'type');
        return (
          <tr key={get(artefact, 'id')}>
            <GTd>
              <Space
                {...space}
                label={
                  <span>
                    {intl.formatMessage(...getTranslationsWithFallback(`artefact.type.${type}`))}
                  </span>
                }
              />
            </GTd>
            <GTd>{get(artefact, 'code')}</GTd>
            <GTd>{get(artefact, 'label')}</GTd>
            <GTd>
              <Space {...space} />
            </GTd>
          </tr>
        );
      })}
    </tbody>
  </table>
);

Items.propTypes = {
  artefacts: PropTypes.object,
  intl: PropTypes.object.isRequired,
};

const DataQuery = ({
  artefactsCount,
  dataQuery,
  hasDataQuery,
  toggleHasDataQuery,
  updateDataQuery,
  intl,
}) => {
  if (artefactsCount !== 1) {
    return null;
  }
  return (
    <div>
      <StyledCheckbox
        checked={hasDataQuery}
        label={
          <span>{intl.formatMessage(...getTranslationsWithFallback('transfer.dataquery'))}</span>
        }
        onChange={toggleHasDataQuery}
      />
      <InputContainer hasDataQuery={hasDataQuery}>
        <QueryInfo>
          <span>
            {intl.formatMessage(...getTranslationsWithFallback('transfer.dataquery.info'))}
          </span>
        </QueryInfo>
        <Input
          onChange={updateDataQuery}
          placeholder={
            <span>
              {intl.formatMessage(...getTranslationsWithFallback('transfer.dataquery.placeholder'))}
            </span>
          }
          value={isNil(dataQuery) ? '' : dataQuery}
        />
      </InputContainer>
    </div>
  );
};

DataQuery.propTypes = {
  artefactsCount: PropTypes.number,
  dataQuery: PropTypes.string,
  hasDataQuery: PropTypes.bool,
  toggleHasDataQuery: PropTypes.func.isRequired,
  updateDataQuery: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
};

const TransferDialog = props => {
  const {
    artefacts,
    closeTransfer,
    dataQuery,
    destinationSpace,
    hasDataQuery,
    isOpenMenu,
    onTransfer,
    spaces,
    toggleDestinationSpace,
    toggleHasDataQuery,
    transferType,
    updateDataQuery,
    intl,
  } = props;
  if (!transferType) return null;

  const artefactId = get(find(artefacts), 'id'); // needed for item transfer

  return (
    <GDialog
      iconName="swap-horizontal"
      isOpen={isOpenMenu}
      onClose={closeTransfer}
      title={
        <span>
          {intl.formatMessage(...getTranslationsWithFallback(`transfer.${transferType}`))}
        </span>
      }
    >
      <div className={Classes.DIALOG_BODY}>
        <Container>
          <Items artefacts={artefacts} intl={intl} />
        </Container>
        <Container>
          <span>{intl.formatMessage(...getTranslationsWithFallback('transfer.destination'))}</span>
          <Spaces>
            {map(spaces, space => {
              const spaceId = get(space, 'id');
              return (
                <SpaceOption key={spaceId}>
                  <StyledCheckbox
                    checked={spaceId === get(destinationSpace, 'id')}
                    onChange={() => toggleDestinationSpace(space)}
                  />
                  <Space {...space} />
                </SpaceOption>
              );
            })}
          </Spaces>
        </Container>
        {transferType === 'data' ? (
          <DataQuery
            artefactsCount={size(artefacts)}
            dataQuery={dataQuery}
            hasDataQuery={hasDataQuery}
            toggleHasDataQuery={toggleHasDataQuery}
            updateDataQuery={updateDataQuery}
            intl={intl}
          />
        ) : null}
      </div>
      <div className={Classes.DIALOG_FOOTER}>
        <div className={Classes.DIALOG_FOOTER_ACTIONS}>
          <Button onClick={closeTransfer}>
            <span>{intl.formatMessage(...getTranslationsWithFallback('transfer.cancel'))}</span>
          </Button>
          <Button
            className={Classes.INTENT_PRIMARY}
            disabled={isNil(destinationSpace)}
            onClick={() => onTransfer(artefactId, destinationSpace, null, dataQuery)}
          >
            <span>{intl.formatMessage(...getTranslationsWithFallback('transfer.apply'))}</span>
          </Button>
        </div>
      </div>
    </GDialog>
  );
};

TransferDialog.propTypes = {
  artefacts: PropTypes.object,
  closeTransfer: PropTypes.func.isRequired,
  dataQuery: PropTypes.string,
  destinationSpace: PropTypes.object,
  hasDataQuery: PropTypes.bool,
  isOpenMenu: PropTypes.bool,
  onTransfer: PropTypes.func,
  spaces: PropTypes.array,
  toggleDestinationSpace: PropTypes.func.isRequired,
  toggleHasDataQuery: PropTypes.func.isRequired,
  transferType: PropTypes.string,
  updateDataQuery: PropTypes.func.isRequired,
  intl: PropTypes.object.isRequired,
};

export default compose(
  injectIntl,
  withTransferControl,
  withProps(
    ({
      artefactId,
      artefacts,
      isSelection,
      itemDataTransfer,
      selection,
      selectionDataTransfer,
      spaces,
      transferType,
    }) => {
      const refinedArtefacts = pick(artefacts, isSelection ? keys(selection) : [artefactId]);
      let onTransfer = null;
      if (transferType === 'data') {
        onTransfer = isSelection ? selectionDataTransfer : itemDataTransfer;
      }
      const sourceSpace = get(find(refinedArtefacts), 'space');
      return {
        artefacts: refinedArtefacts,
        onTransfer,
        spaces: reject(spaces, { id: get(sourceSpace, 'id') }),
      };
    },
  ),
)(TransferDialog);
