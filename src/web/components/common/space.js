import React from 'react';
import PropTypes from 'prop-types';
import { Tag } from '@blueprintjs/core';
import glamorous from 'glamorous';

const Space = ({ label, color, backgroundColor }) => {
  const StyledTag = glamorous(Tag)({
    color: `${color} !important`,
    backgroundColor: `${backgroundColor} !important`,
  });

  return <StyledTag>{label}</StyledTag>;
};

Space.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
};

Space.defaultProps = {
  //color: '#182026',
  //backgroundColor: '#e8ebee',
  color: 'white',
  //backgroundColor: '#0965C1',
  backgroundColor: '#EE6290',
};

export default Space;
