import React from 'react';
import PropTypes from 'prop-types';
import { map } from 'lodash';
import { NavLink } from 'react-router-dom';
import { Classes } from '@blueprintjs/core';
import classnames from 'classnames';
import { FormattedMessage } from 'react-intl';
import glamorous from 'glamorous';

const StyledNavLink = glamorous(NavLink)({
  marginLeft: 5,
  marginRigtht: 5,
  color: 'white !important',
  '&.pt-active': {
    color: 'white !important',
    background: 'rgba(255, 255, 255, 0.2) !important',
  },
  '::before': {
    color: 'white !important',
  },
});

const Menu = ({ routes }) => {
  return (
    <div className={classnames('menu')}>
      {map(routes, route => {
        return (
          <StyledNavLink
            key={route.name}
            to={route.path}
            activeClassName={Classes.ACTIVE}
            exact
            className={classnames(Classes.BUTTON, Classes.MINIMAL, Classes.iconClass(route.icon), {
              [Classes.DISABLED]: !!route.disabled,
            })}
          >
            {route.translationKey ? <FormattedMessage id={route.translationKey} /> : null}
          </StyledNavLink>
        );
      })}
    </div>
  );
};

Menu.propTypes = {
  routes: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
      icon: PropTypes.string,
      disabled: PropTypes.bool,
      translationKey: PropTypes.string,
    }),
  ),
};

export default Menu;
