import React from 'react';
import PropTypes from 'prop-types';
import { partition, has } from 'lodash';
import Menu from './menu';
import Lang from './lang';
import Me from './me';
import { Classes } from '@blueprintjs/core';
import classnames from 'classnames';
import { withRouter } from 'react-router-dom';
import glamorous from 'glamorous';

const NavStyled = glamorous.nav({
  height: '60px !important',
  backgroundColor: '#0965C1 !important',
  display: 'flex !important',
  alignItems: 'center',
  justifyContent: 'space-between',
  boxShadow: 'none !important',
  '& img': {
    maxHeight: 48,
  },
});

const Nav = ({ routes }) => {
  const [explicitRoutes, implicitRoutes] = partition(routes, route => has(route, 'translationKey'));
  return (
    <NavStyled className={classnames('nav', Classes.NAVBAR, Classes.FIXED_TOP, Classes.DARK)}>
      <div className={classnames(Classes.NAVBAR_GROUP)}>
        <div className={classnames(Classes.NAVBAR_HEADING)}>
          <img src={window.SETTINGS.assets.logo} alt="logo" />
        </div>
      </div>
      <div className={classnames(Classes.NAVBAR_GROUP)}>
        <Menu routes={explicitRoutes} />
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <Menu routes={implicitRoutes} />
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <Me />
        <span className={Classes.NAVBAR_DIVIDER}></span>
        <Lang />
      </div>
    </NavStyled>
  );
};

Nav.propTypes = {
  routes: PropTypes.array,
};

export default withRouter(Nav);
