import React from 'react';
import PropTypes from 'prop-types';
import { isNil, map } from 'lodash';
import { compose } from 'recompose';
import { withKeycloak } from 'react-keycloak';
import { withMe } from '../../modules/users';
import { injectIntl } from 'react-intl';
import { Button, Classes, IconClasses, Popover, Position, NonIdealState } from '@blueprintjs/core';
import Space from './space';
import { getTranslations } from '../../modules/i18n/utils';
import glamorous from 'glamorous';

const getTranslationsWithFallback = getTranslations([{ id: 'users.none' }, { id: 'logout' }]);

const NonIdealStateStyled = glamorous(NonIdealState)({
  padding: 15,
});

const ButtonStyled = glamorous(Button)({
  color: 'white !important',
  '&.pt-active': {
    color: 'white !important',
    background: 'rgba(255, 255, 255, 0.2) !important',
  },
  '::before': {
    color: 'white !important',
  },
});

const Me = ({ keycloak, me, agencies, intl }) => {
  if (!me) {
    return (
      <ButtonStyled
        iconName={IconClasses.PERSON}
        text={intl.formatMessage(...getTranslationsWithFallback('users.none'))}
        className={Classes.MINIMAL}
      />
    );
  }

  return (
    <Popover
      target={
        <ButtonStyled iconName={IconClasses.PERSON} text={me.name} className={Classes.MINIMAL} />
      }
      content={
        <NonIdealStateStyled
          description={
            <div>
              <p>{me.email}</p>
              <ul className="pt-list pt-list-unstyled">
                {map(agencies, agency => {
                  const label = isNil(agency.label)
                    ? agency.code
                    : `${agency.label} (${agency.code})`;
                  return (
                    <li key={agency.id}>
                      <Space {...agency.space} label={label} />
                    </li>
                  );
                })}
              </ul>
              <Button iconName="log-out" onClick={() => keycloak.logout()}>
                <span>{intl.formatMessage(...getTranslationsWithFallback('logout'))}</span>
              </Button>
            </div>
          }
        />
      }
      position={Position.BOTTOM}
      inheritDarkTheme={false}
    />
  );
};

Me.propTypes = {
  agencies: PropTypes.objectOf(
    PropTypes.shape({
      id: PropTypes.string,
      label: PropTypes.string,
      code: PropTypes.string,
      space: PropTypes.object,
    }),
  ),
  keycloak: PropTypes.shape({
    logout: PropTypes.func.isRequired,
  }),
  me: PropTypes.shape({
    name: PropTypes.string,
    email: PropTypes.string,
  }),
  intl: PropTypes.object.isRequired,
};

export default compose(
  injectIntl,
  withKeycloak,
  withMe,
)(Me);
