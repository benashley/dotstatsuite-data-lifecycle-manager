import React from 'react';
import { get, has, isEmpty, map } from 'lodash';
import { Classes, Spinner } from '@blueprintjs/core';
import { withList, List } from '../list';
import { withConfig } from '../../modules/config';
import { SpaceItem } from '../filters/spaces';
import { FormattedMessage } from 'react-intl';
import glamorous from 'glamorous';
import {
  onlyUpdateForKeys,
  renameProps,
  compose,
  withProps,
  branch,
  renderComponent,
} from 'recompose';

const SpaceStyled = glamorous.div({
  display: 'flex',
  justifyContent: 'center',
});

const SpaceSpinner = () => (
  <SpaceStyled>
    <Spinner className={Classes.SMALL} />
  </SpaceStyled>
);

const SpaceBlank = () => (
  <SpaceStyled>
    <em>
      <FormattedMessage id="spaces.no.space" />
    </em>
  </SpaceStyled>
);

export default compose(
  withConfig,
  onlyUpdateForKeys(['isFetching', 'internalSpaces', 'spaces', 'internalSpacesById']),
  renameProps({ internalSpaces: 'items' }),
  withList(),
  withProps(({ items, list, spaces, removeSpace, selectSpace, internalSpacesById }) => ({
    isBlank: isEmpty(items),
    isOpen: true,
    itemRenderer: SpaceItem,
    handlers: {
      onChangeHandler: id =>
        has(spaces, id)
          ? removeSpace(get(internalSpacesById, id))
          : selectSpace(get(internalSpacesById, id)),
    },
    items: map(list.items, item => ({ ...item, isSelected: has(spaces, item.id) })),
  })),
  branch(({ isFetching }) => isFetching, renderComponent(SpaceSpinner)),
  branch(({ isBlank }) => isBlank, renderComponent(SpaceBlank)),
)(List);
