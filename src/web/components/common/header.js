import React from 'react';
import PropTypes from 'prop-types';
import { NonIdealState } from '@blueprintjs/core';
import { FormattedMessage } from 'react-intl';

const Header = ({ id, description, iconName }) => {
  return (
    <NonIdealState
      title={<FormattedMessage id={id} />}
      description={description}
      visual={iconName}
    />
  );
};

Header.propTypes = {
  description: PropTypes.node,
  id: PropTypes.string.isRequired,
  iconName: PropTypes.string,
};

export default Header;
