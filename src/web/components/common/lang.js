import React from 'react';
import PropTypes from 'prop-types';
import { withLocale } from '../../modules/i18n';
import { map } from 'lodash';
import { Button, Classes } from '@blueprintjs/core';
import classnames from 'classnames';
import { FormattedMessage } from 'react-intl';
import glamorous from 'glamorous';

const StyledButton = glamorous(Button)({ marginLeft: 5, marginRigtht: 5 });

const Lang = ({ availableLocales, locale, changeLocale }) => {
  return (
    <div className="lang">
      {map(availableLocales, l => {
        return (
          <StyledButton
            key={l.id}
            className={classnames(Classes.MINIMAL, { [Classes.ACTIVE]: l.id === locale })}
            onClick={() => changeLocale(l.id)}
          >
            <FormattedMessage id={l.id} />
          </StyledButton>
        );
      })}
    </div>
  );
};

Lang.propTypes = {
  availableLocales: PropTypes.array,
  locale: PropTypes.string,
  changeLocale: PropTypes.func,
};

export default withLocale(Lang);
