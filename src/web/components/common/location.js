import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';

const Location = ({ location }) => {
  return (
    <span>
      <FormattedMessage id="path.current" />: {location.pathname}
    </span>
  );
};

Location.propTypes = {
  location: PropTypes.object,
};

export default withRouter(Location);
