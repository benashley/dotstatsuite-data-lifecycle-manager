import React from 'react';
import { FormattedMessage } from 'react-intl';
import glamorous from 'glamorous';

const StyledFooter = glamorous.nav({
  display: 'flex',
  justifyContent: 'space-between',
  padding: 15,
  borderTop: '1px solid #ccc',
  backgroundColor: '#f0f0f0',
  color: '#494444',
  height: 85,
  '& img': {
    maxHeight: 20,
    margin: '0 5px',
  },

  // sticky footer -> https://philipwalton.com/articles/normalizing-cross-browser-flexbox-bugs/
  flexShrink: 0,
});

const IconAndText = glamorous.div({
  display: 'flex',
  fontWeight: 'bold',
});

const Footer = () => {
  return (
    <StyledFooter>
      <IconAndText>
        <FormattedMessage id="app.built.by" />
        <img src={window.SETTINGS.assets.icon} alt="icon" />
        <a href="http://siscc.oecd.org/">
          <FormattedMessage id="app.siscc" />
        </a>
        &nbsp;
        <FormattedMessage id="app.built.by.end" />
      </IconAndText>
      <div className="pt-rtl">
        <FormattedMessage id="disclaimer.l1" />
        <br />
        <FormattedMessage id="disclaimer.l2" />
      </div>
    </StyledFooter>
  );
};

Footer.propTypes = {};

export default Footer;
