import React from 'react';
import { NonIdealState } from '@blueprintjs/core';
import { FormattedMessage } from 'react-intl';

const NotFound = () => {
  return <NonIdealState title={<FormattedMessage id="route.not.found" />} visual="send-to-map" />;
};

NotFound.propTypes = {};

export default NotFound;
