import React from 'react';
import PropTypes from 'prop-types';
import glamorous from 'glamorous';
import { Spinner, Classes, Collapse, Button } from '@blueprintjs/core';
import { FormattedMessage } from 'react-intl';
import { compose, branch, renderComponent, withState, withHandlers } from 'recompose';
import classnames from 'classnames';

export const StyledFilter = glamorous.div({
  borderTop: '2px solid #999',
  marginBottom: 20,
});

export const StyledHeaderFilter = glamorous.div({
  display: 'flex',
  justifyContent: 'space-between',
  paddingBottom: 5,
  alignItems: 'center',
  minHeight: 35,
  '& h6': {
    color: '#666',
    margin: 0,
  },
});

export const FilterSpinner = () => {
  return (
    <div>
      <Spinner className={Classes.SMALL} />
    </div>
  );
};

export const FilterBlank = () => {
  return (
    <div>
      <em>
        <FormattedMessage id="filters.no.filter" />
      </em>
    </div>
  );
};

export const FilterHeader = ({ title, isOpen, collapseChange, clearHandler }) => {
  const collapseButton = collapseChange ? (
    <Button
      className={Classes.MINIMAL}
      iconName={classnames({ 'chevron-up': isOpen, 'chevron-down': !isOpen })}
      onClick={collapseChange}
    />
  ) : null;

  const clearAllButton =
    clearHandler && isOpen ? (
      <Button className={Classes.MINIMAL} iconName="eraser" onClick={clearHandler} />
    ) : null;

  return (
    <StyledHeaderFilter>
      <div>
        <h6>
          <FormattedMessage id={title} default={title} />
        </h6>
      </div>
      <div>
        {clearAllButton}
        {collapseButton}
      </div>
    </StyledHeaderFilter>
  );
};

FilterHeader.propTypes = {
  title: PropTypes.string.isRequired,
  isOpen: PropTypes.bool,
  collapseChange: PropTypes.func,
  clearHandler: PropTypes.func,
};

const CollapseFilterBody = ({ children, isOpen, collapseChange }) => {
  return <Collapse isOpen={collapseChange ? isOpen : true}>{children()}</Collapse>;
};

CollapseFilterBody.propTypes = {
  children: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  collapseChange: PropTypes.func,
};

export const FilterBody = compose(
  branch(({ isFetching }) => isFetching, renderComponent(FilterSpinner)),
  branch(({ isBlank }) => isBlank, renderComponent(FilterBlank)),
)(CollapseFilterBody);

export const Filter = props => {
  return (
    <StyledFilter>
      <FilterHeader
        title={props.title}
        isOpen={props.isOpen}
        collapseChange={props.collapseChange}
        clearHandler={props.clearHandler}
      />
      <FilterBody
        isFetching={props.isFetching}
        isBlank={props.isBlank}
        isOpen={props.isOpen}
        collapseChange={props.collapseChange}
      >
        {props.children}
      </FilterBody>
    </StyledFilter>
  );
};

Filter.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.func.isRequired,
  isOpen: PropTypes.bool,
  isBlank: PropTypes.bool,
  isFetching: PropTypes.bool,
  collapseChange: PropTypes.func,
  clearHandler: PropTypes.func,
};

export default compose(
  withState('isOpen', 'setOpen', props => !!props.isOpen),
  withHandlers({
    collapseChange: ({ setOpen }) => event => {
      event.preventDefault();
      setOpen(isOpen => !isOpen);
    },
  }),
)(Filter);
