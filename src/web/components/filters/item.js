import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from '@blueprintjs/core';
import glamorous from 'glamorous';

const StyledCheckbox = glamorous(Checkbox)({
  minHeight: '20px !important',
  lineHeight: '20px !important',
  '& .pt-control-indicator': {
    top: '2px !important',
  },
});

const Item = ({ id, label, children, isSelected, handlers }) => {
  return (
    <StyledCheckbox onChange={() => handlers.onChangeHandler(id)} checked={!!isSelected}>
      {children ? children : label}
    </StyledCheckbox>
  );
};

Item.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  children: PropTypes.element,
  isSelected: PropTypes.bool,
  handlers: PropTypes.objectOf(PropTypes.func),
};

export default Item;
