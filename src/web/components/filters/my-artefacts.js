import React from 'react';
import PropTypes from 'prop-types';
import { Checkbox } from '@blueprintjs/core';
import { withFilters } from '../../modules/filters';
import { onlyUpdateForKeys, compose } from 'recompose';
import { FormattedMessage } from 'react-intl';

export const MyArtefacts = ({ isMyArtefacts, toggleIsMyArtefacts }) => {
  return (
    <div className="filters-my-artefacts">
      <Checkbox
        checked={isMyArtefacts}
        label={<FormattedMessage id="artefacts.filters.my.artefacts" />}
        onChange={() => toggleIsMyArtefacts()}
      />
    </div>
  );
};

MyArtefacts.propTypes = {
  isMyArtefacts: PropTypes.bool,
  toggleIsMyArtefacts: PropTypes.func.isRequired,
};

export default compose(
  withFilters,
  onlyUpdateForKeys(['isMyArtefacts', 'toggleIsMyArtefacts']),
)(MyArtefacts);
