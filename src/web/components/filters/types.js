import React from 'react';
import Filter from './filter';
import Item from './item';
import { withList, List, Spotlight, withSpotlight } from '../list';
import { withConfig } from '../../modules/config';
import { withFilters } from '../../modules/filters';
import { onlyUpdateForKeys, defaultProps, renameProps, compose, withProps } from 'recompose';
import { isEmpty, map } from 'lodash';
import glamorous from 'glamorous';
import { defineMessages, injectIntl } from 'react-intl';

const SpotlightWrapper = glamorous.div({
  paddingBottom: 10,
});

const ListWrapper = glamorous.div({
  maxHeight: 250,
  overflow: 'auto',
});

export const Types = props => {
  return (
    <Filter {...props}>
      {() => (
        <div>
          <SpotlightWrapper>
            <Spotlight
              {...props.spotlight}
              fieldChange={props.spotlightFieldChange}
              valueChange={props.spotlightValueChange}
            />
          </SpotlightWrapper>
          <ListWrapper>
            <List {...props} items={props.list.items} itemRenderer={Item} />
          </ListWrapper>
        </div>
      )}
    </Filter>
  );
};

export default compose(
  injectIntl,
  withConfig,
  withFilters,
  onlyUpdateForKeys(['isFetching', 'typeToggle', 'typeClear', 'types']),
  defaultProps({ title: 'artefacts.filters.types.title' }),
  renameProps({ typeToggle: 'onChangeHandler', typeClear: 'clearHandler', types: 'items' }),
  withProps(({ onChangeHandler, items, intl }) => ({
    items: map(items, item => ({
      ...item,
      label: intl.formatMessage(defineMessages({ label: { id: `filter.${item.id}` } }).label),
    })),
    handlers: { onChangeHandler },
    isBlank: isEmpty(items),
    isOpen: true,
  })),
  withList(),
  withSpotlight([{ id: 'label', accessor: 'label', isSelected: true }]),
)(Types);
