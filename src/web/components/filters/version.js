import React from 'react';
import PropTypes from 'prop-types';
import { Filter } from './filter';
import { RadioGroup, Radio, Checkbox } from '@blueprintjs/core';
import { withFilters } from '../../modules/filters';
import { onlyUpdateForKeys, compose, defaultProps } from 'recompose';
import { map } from 'lodash';
import { FormattedMessage } from 'react-intl';
import glamorous from 'glamorous';

const CheckboxWrapper = glamorous.div({
  paddingTop: 10,
  borderTop: '1px dashed #ccc',
});

export const Version = props => {
  return (
    <Filter {...props} isBlank={false}>
      {() => (
        <div>
          <RadioGroup
            onChange={e => props.changeVersion(e.target.value)}
            selectedValue={props.version}
          >
            {map(props.versions, v => (
              <Radio
                key={v}
                label={<FormattedMessage id={`artefacts.filters.version.${v}`} />}
                value={v}
              />
            ))}
          </RadioGroup>
          <CheckboxWrapper>
            <Checkbox
              checked={props.isFinal}
              label={<FormattedMessage id="filter.final" />}
              onChange={() => props.toggleIsFinal()}
            />
          </CheckboxWrapper>
        </div>
      )}
    </Filter>
  );
};

Version.propTypes = {
  versions: PropTypes.objectOf(PropTypes.string).isRequired,
  version: PropTypes.string.isRequired,
  changeVersion: PropTypes.func.isRequired,
  isFinal: PropTypes.bool,
  toggleIsFinal: PropTypes.func.isRequired,
};

export default compose(
  withFilters,
  onlyUpdateForKeys(['versions', 'version', 'changeVersion', 'isFinal', 'toggleIsFinal']),
  defaultProps({ title: 'artefacts.filters.version.title' }),
)(Version);
