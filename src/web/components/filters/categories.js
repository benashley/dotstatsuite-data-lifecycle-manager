import React from 'react';
import * as R from 'ramda';
import Filter from './filter';
import Space from '../common/space';
import { withCategories, withSpacedCategories } from '../../modules/categories';
import { withFilters } from '../../modules/filters';
import { Tree } from '@blueprintjs/core';
import glamorous from 'glamorous';
import { onlyUpdateForKeys, defaultProps, renameProps, compose, withProps } from 'recompose';
import { withList } from '../list';

const FilterWrapper = glamorous.div({
  maxHeight: 250,
  overflow: 'auto',
});

const CategoriesTreeWrapper = glamorous.div({
  borderTop: '1px dashed #ccc',
  paddingBottom: 10,
  paddingTop: 10,
  '& .pt-tree-node-content': {
    minHeight: 34,
  },
});

const CategoriesTree = props => (
  <CategoriesTreeWrapper>
    <Space {...R.propOr({}, 'space', props)} />
    <Tree {...props} />
  </CategoriesTreeWrapper>
);

const spacedCategoriesTree = spaceId =>
  compose(
    withSpacedCategories(spaceId),
    renameProps({ categories: 'items' }),
    withList({ isTree: true }),
    withProps(({ items }) => ({ isBlank: R.isEmpty(items) })),
    renameProps({
      categoryToggle: 'onNodeClick',
      items: 'contents',
    }),
    withProps(({ categoryExpandToggle }) => ({
      onNodeExpand: categoryExpandToggle,
      onNodeCollapse: categoryExpandToggle,
    })),
  )(CategoriesTree);

export const Categories = props => {
  return (
    <Filter {...props}>
      {() => (
        <FilterWrapper>
          {R.pipe(
            R.mapObjIndexed((space, spaceId) => {
              const SpacedCategoriesTree = spacedCategoriesTree(spaceId);
              return <SpacedCategoriesTree key={spaceId} {...props} space={space} />;
            }),
            R.values,
          )(props.selectedSpaces)}
        </FilterWrapper>
      )}
    </Filter>
  );
};

export default compose(
  withFilters,
  withCategories,
  onlyUpdateForKeys(['isFetching', 'categoryToggle', 'categoryClear']),
  defaultProps({ title: 'artefacts.filters.categories.title', isOpen: true }),
  renameProps({ categoryClear: 'clearHandler' }),
)(Categories);
