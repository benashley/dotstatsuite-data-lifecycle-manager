import React from 'react';
import Filter from './filter';
import { withAgencies } from '../../modules/agencies';
import { withFilters } from '../../modules/filters';
import { Tree } from '@blueprintjs/core';
import { onlyUpdateForKeys, defaultProps, renameProps, compose, withProps } from 'recompose';
import { isEmpty } from 'lodash';
import MyArtefacts from './my-artefacts';
import glamorous from 'glamorous';
import { Spotlight, withSpotlight, withList } from '../list';

export const AgenciesTree = compose(
  renameProps({
    agencyToggle: 'onNodeClick',
    agencyExpandNode: 'onNodeExpand',
    agencyCollapseNode: 'onNodeCollapse',
    items: 'contents',
  }),
)(Tree);

const AgenciesTreeWrapper = glamorous.div({
  borderTop: '1px dashed #ccc',
  '& .pt-tree-node-content': {
    minHeight: 34,
  },
});

const TreeWrapper = glamorous.div({
  maxHeight: 250,
  overflow: 'auto',
});

const SpotlightWrapper = glamorous.div({
  paddingTop: 10,
  paddingBottom: 10,
});

export const Agencies = props => {
  return (
    <Filter {...props}>
      {() => (
        <div>
          <MyArtefacts />
          <AgenciesTreeWrapper>
            <SpotlightWrapper>
              <Spotlight
                {...props.spotlight}
                fieldChange={props.spotlightFieldChange}
                valueChange={props.spotlightValueChange}
              />
            </SpotlightWrapper>
            <TreeWrapper>
              <AgenciesTree {...props} items={props.list.items} />
            </TreeWrapper>
          </AgenciesTreeWrapper>
        </div>
      )}
    </Filter>
  );
};

export default compose(
  withFilters,
  withAgencies,
  onlyUpdateForKeys(['isFetching', 'agencyToggle', 'agencyClear', 'agencies']),
  defaultProps({ title: 'artefacts.filters.agencies.title' }),
  renameProps({ agencies: 'items', agencyClear: 'clearHandler' }),
  withList({ isTree: true }),
  withSpotlight([{ id: 'label', accessor: 'label', isSelected: true }]),
  withProps(({ items }) => ({ isBlank: isEmpty(items), isOpen: true })),
)(Agencies);
