import React from 'react';
import glamorous from 'glamorous';
import spaces from './spaces';
import Agencies from './agencies';
import Categories from './categories';
import Types from './types';
import Version from './version';

export const StyledFilter = glamorous.div({
  borderTop: '3px solid #516371',
  paddingTop: 10,
  marginBottom: 40,
});

const Filters = () => {
  const InternalSpaces = spaces('internalSpaces');
  const ExternalSpaces = spaces('externalSpaces');
  return (
    <div className="filters">
      <InternalSpaces title="artefacts.filters.spaces.title" />
      <ExternalSpaces title="artefacts.filters.external.spaces.title" />
      <Categories />
      <Agencies />
      <Types />
      <Version />
    </div>
  );
};

Filters.propTypes = {};

export default Filters;
