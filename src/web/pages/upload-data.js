import React from 'react';
import { compose, mapProps } from 'recompose';
import { Classes } from '@blueprintjs/core';
import { FormattedMessage } from 'react-intl';
import glamorous from 'glamorous';
import numeral from 'numeral';
import { withUploadData, withUploadMode } from '../modules/upload-data';
import Upload from '../components/upload';
import ModeSelect from '../components/upload/mode-select';
import eddFile from '../mocks/EDD.xml';

const Wrapper = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

export default compose(
  withUploadData,
  withUploadMode,
  mapProps(({ changeMode, helpKey, maxSize, modes, selected, ...rest }) => {
    const selectProps = { changeMode, modes, selected };
    const edd = (
      <a href={eddFile} target="_blank" rel="noopener noreferrer">
        <FormattedMessage id="upload.data.help.edd.file" />
      </a>
    );
    const headerProps = {
      iconName: 'document-share',
      id: 'upload.data.title',
      description: (
        <Wrapper>
          <em className={Classes.TEXT_MUTED}>
            <FormattedMessage
              id={helpKey}
              values={{ size: numeral(maxSize).format('0.00 b'), edd }}
            />
          </em>
          <br />
          <ModeSelect {...selectProps} />
        </Wrapper>
      ),
    };

    return {
      ...rest,
      helpKey,
      scope: 'data',
      headerProps,
    };
  }),
)(Upload);
