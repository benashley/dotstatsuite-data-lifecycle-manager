import React from 'react';
import glamorous from 'glamorous';
import Artefacts from '../components/artefacts';
import Filters from '../components/filters';

const Wrapper = glamorous.div({
  display: 'flex',
});

const ArtefactsWrapper = glamorous.div({
  flexGrow: 1,
  borderTop: '2px solid #999',
  paddingTop: 10,
});

const FiltersWrapper = glamorous.div({
  width: 300,
  minWidth: 300,
  marginRight: 20,
});

const ManageStructure = () => {
  return (
    <Wrapper className="manage-structure">
      <FiltersWrapper>
        <Filters />
      </FiltersWrapper>
      <ArtefactsWrapper>
        <Artefacts />
      </ArtefactsWrapper>
    </Wrapper>
  );
};

ManageStructure.propTypes = {};

export default ManageStructure;
