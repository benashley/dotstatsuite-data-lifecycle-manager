import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import glamorous from 'glamorous';
import { Classes, Intent } from '@blueprintjs/core';
import { FormattedMessage } from 'react-intl';
import { isNil } from 'ramda';
import Actions from '../components/common/actions';
import Header from '../components/common/header';
import Spaces from '../components/common/spaces';
import { withDump } from '../modules/dump';

const Container = glamorous.div({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
});

const Item = glamorous.div({
  width: 500,
  margin: 10,
  padding: '0 !important',
});

const ItemContent = glamorous.div({
  padding: 12,
});

const headerProps = {
  iconName: 'cloud-download',
  id: 'dump.title',
  description: (
    <em className={Classes.TEXT_MUTED}>
      <FormattedMessage id="dump.description" />
    </em>
  ),
};

const Dump = ({
  dumpRemoveSpace,
  dumpRequest,
  dumpReset,
  dumpSelectSpace,
  isDownloading,
  spaceId,
}) => {
  const actions = [
    {
      iconName: 'download',
      id: 'dump.download',
      disabled: isNil(spaceId),
      loading: isDownloading,
      onClick: dumpRequest,
    },
    {
      iconName: 'delete',
      id: 'dump.reset',
      intent: Intent.DANGER,
      onClick: dumpReset,
    },
  ];
  return (
    <Container>
      <Item>
        <Header {...headerProps} />
      </Item>
      <Item className={cx(Classes.CARD, Classes.INTERACTIVE)}>
        <div className={cx(Classes.CALLOUT, Classes.iconClass('database'))}>
          <FormattedMessage id="dump.spaces" />
        </div>
        <ItemContent>
          <Spaces
            spaces={{ [spaceId]: spaceId }}
            selectSpace={dumpSelectSpace}
            removeSpace={dumpRemoveSpace}
          />
        </ItemContent>
      </Item>
      <Item>
        <Actions actions={actions} />
      </Item>
    </Container>
  );
};

Dump.propTypes = {
  dumpRemoveSpace: PropTypes.func.isRequired,
  dumpRequest: PropTypes.func.isRequired,
  dumpReset: PropTypes.func.isRequired,
  dumpSelectSpace: PropTypes.func.isRequired,
  isDownloading: PropTypes.bool,
  spaceId: PropTypes.string,
};

export default withDump(Dump);
