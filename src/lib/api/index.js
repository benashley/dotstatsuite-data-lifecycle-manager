import { path, split, compose, toPairs, forEach } from 'ramda';
import share from './share';
import backend from './backend';

const globalConfig = { debug: true };

const config = config => Object.keys(config).forEach(k => (globalConfig[k] = config[k]));
const bearer = () =>
  globalConfig.keycloak
    .updateToken(5)
    .then(() => ({ Authorization: `Bearer ${globalConfig.keycloak.token}` }));

const methods = { share, backend };

const error = method => () => {
  throw new Error(`Unkown method: ${method}`);
};

const getFn = (main, name) => path(split(':', name), main);
const main = ({ method, ...rest }) => (getFn(main, method) || error(method))(rest);
main.config = config;
main.globalConfig = globalConfig;

compose(
  forEach(([name, fn]) => (main[name] = fn({ bearer, globalConfig }))),
  toPairs,
)(methods);

export default main;
