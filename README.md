# Data Lifecycle Manager

The Data Lyfecycle Manager (DLM) is a set of adaptive back-office modules to efficiently and timely produce and (re-)use high quality statistical data by defining, running, automating, controlling and evaluating the underlying data processes. <br>
As a numerous expected features are still to be developed, the main areas concerned today by the DLM are:
* **Authenticate** & **Authorize**
* **Get**: SDMX structure & data import, Transfer, Export
* **Disseminate**: see [.Stat Data Explorer](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer)

## Webapp  

This project use boilerplate dotstatsuite-webapp
  - Multitenant
  - Proxy
  - Config

You can see more [here](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-webapp)


## Usage

To start Data-lifecycle-manager

First start a config server:

### Config

```
$ docker run -d --name config --restart always -p 5007:80 siscc/dotstatsuite-config-dev:latest
```

If unlikely you cannot run docker images on your development workstation or you want a custom config,
see how to [run config server](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-config) from source.

Clone the repo.
```
$ yarn
$ yarn start:srv
```

### Setup

Clone the repo.
```
$ yarn
```
You can see more details in [Local setup](#local-setup)

Now start `dlm` server:

```
$ yarn start:srv
...
server started
```

and proxy
```
$ yarn start:proxy
```

Launch your preferred browser on http://localhost:7000

If it doesn't work, check log messages

## Local setup

Config params are located in `src/params/<process.env.NODE_ENV>.js` file.

You can overwrite existing values by setting shell variables like this:

```
$ SERVER_PORT=7002 yarn start:srv
```

or by adding variables in `.env` file

```
SERVER_PORT=7002
PROXY_PORT=7000
CONFIG_URL="http://localhost:5007"
DEFAULT_TENANT=oecd
AUTH_SERVER_URL="http://keycloak-oecd.redpelicans.com"
TRANSFER_SERVER_URL="http://transfer-siscc.redpelicans.com"
```

AUTH_SERVER_URL
In localhost. Only the port 7000 redirect to http://keycloak-oecd.redpelicans.com
