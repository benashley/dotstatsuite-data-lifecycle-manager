module.exports = {
  env: {
    browser: true,
    jest: true,
    node: true,
    es6: true,
  },
  settings: {
    react: {
      version: '16.0',
    },
  },
  extends: ['eslint:recommended', 'plugin:react/recommended'],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true,
      jsx: true,
    },
    sourceType: 'module',
  },
  plugins: ['prettier', 'react', 'import'],
  rules: {
    'no-console': 'warn',
    'no-unused-vars': 'warn',
    'react/display-name': 'off',
    // AAAH
    'react/prop-types': 'warn',
    'no-unused-vars': 'warn',
    'no-undef': 'warn',
  },
};
